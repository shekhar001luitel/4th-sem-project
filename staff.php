<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class Staff extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
       
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/staff');
        view_require('_parts/footer');
    }
}
new Staff();
