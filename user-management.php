<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class UserManagement extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $myRole = $this->user_role();
        $readResult = $db->readAll('users');
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/userManagement',['data' =>$readResult, 'role' => $myRole]);
        view_require('_parts/footer');
    }
}
 new UserManagement();