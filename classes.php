<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class Classes extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $sql="select batch.*, faculty.faculty_name from faculty inner join batch on batch.faculty_id = faculty.id";
        $readResult = $db->sql($sql);
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/classes',['data' =>$readResult]);
        view_require('_parts/footer');
    }
}
 new classes();