<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class Section extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $sql=" SELECT * from section ";
        $readResult = $db->sql($sql);
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/section',['data' =>$readResult]);
        view_require('_parts/footer');
    }
}
new Section();