# 4th Semester Project - CRUD System

## Version: 1.0
Author: [Shekhar Luitel](https://shekharluitel.com.np)

Inspired by CodeIgniter 3

Tested on PHP Version 8.1.24

## Introduction

This project is a simple CRUD (Create, Read, Update, Delete) system developed in PHP using MySQLi. It provides basic functionality for user registration and login. The project is designed to be lightweight and serves as a foundation for further development.

## Requirements

- PHP: [Download PHP](https://www.php.net/downloads.php)
- MySQL Database

## Language
- HTML
- CSS
- JS(Plan)
- PHP

## Installation

1. Download and install [PHP](https://www.php.net/downloads.php).
2. Configure your MySQL database connection in `Model/Database.php`.
3. Open Terminal and run the PHP built-in server:

    ```bash
    php -S localhost:8000
    ```

## Database Migration

Execute the following SQL query to create the necessary 'users' table:

```sql
CREATE TABLE
  `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`)
  );
  ```
  ```structure
4th_semester_project/
│
├── Controller/
│   ├── Controller.php
│   └── LoginController.php
│
├── Model/
│   ├── Database.php
│   └── Migration.sql
│
├── View/
│   ├── _part/
│   │   ├── footer.php
│   │   └── header.php
│   ├── assets/
│   │   ├── css
│   │   │   └── login.css
│   │   └── js
│   ├── dashboard/
│   │   └── index.php
│   └── login/
│       ├── login.php
│       └── register.php
│
├── helper.php
│
├── index.php
│
├── login.php
│
├── README.md
│
├── register.php
│
└── register.php
```

## Technologies Used

This project leverages various web technologies to create a robust CRUD system. The key technologies include:

- **HTML:** Used for structuring web pages and creating forms for user interaction.

- **CSS:** Styling is implemented using CSS to enhance the visual appeal and ensure a responsive design.

- **JavaScript:** JS is planned for future enhancements, possibly for implementing client-side validation and interactive features.

- **PHP:** The server-side logic and interaction with the MySQL database are implemented in PHP, providing the backend functionality.

These technologies work together to create a seamless and user-friendly experience in the web application. The modular approach allows for flexibility and future enhancements as needed.


## Usage

- Access the application by visiting [http://localhost:8000](http://localhost:8000) in your web browser.
- Navigate to the login and registration pages to interact with the system.

## Features

- User Registration
- User Login
- Basic CRUD Operations
- Responsive Design (CSS Styling)

## File Descriptions

- **Controller/:** Contains PHP controllers for handling different aspects of the application.
- **Model/:** Manages the database connection and includes migration scripts.
- **View/:** Organized into subdirectories for partials, assets, and different sections of the application.
- **helper.php/:** Contains utility functions that can be used throughout the project.
- **index.php/:** Entry point for the application.
- **login.php/:** Login page.
- **register.php/:** Registration page.
- **README.md/:** Project documentation.

## Future Enhancements

- User Profile Management

## Security Considerations

- Passwords are securely hashed using PHP's password_hash() function.
- Validate and sanitize user input to prevent SQL injection.
