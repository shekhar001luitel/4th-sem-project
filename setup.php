<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class Setup extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $readResult = $db->readAll('faculty');
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/setup',['data' =>$readResult]);
        view_require('_parts/footer');
    }
}
 new Setup();