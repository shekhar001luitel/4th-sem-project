<?php
// session_start();
include "helper.php";
include 'Controller/Controller.php';
include "Model/Database.php";

class Setting extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->dashboard();
    }
    public function dashboard()
    {
        $Database = new Database();
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        $condition = "WHERE id = '" . $_SESSION['user_id'] . "'";
        $userId = $user['id'];
        if($_SESSION['role'] == 'admin'){
            $userDetails = $Database->read('users', $condition);
            view_require('dashboard/setting', ['data' => $userDetails]);
        }elseif($_SESSION['role'] == 'teacher'){
            $userDetails = $Database->read('staff_details', $condition);
            view_require('dashboard/teacher-profile', ['data' => $userDetails]);
        }elseif($_SESSION['role'] == 'student'){
            $condition = "SELECT sd.id AS student_id, sd.name AS student_name,sd.student_image_path AS student_image, sd.name_in_nepali, sd.gender, sd.dob_bs, f.faculty_name, b.batch_name, c.name AS class_name, s.section_name, sd.phone, sd.nationality, sd.permanent_province, sd.permanent_district, sd.permanent_municipality, sd.permanent_address, sd.father_name, sd.father_image_path, sd.f_occupation, sd.f_cell, sd.f_email, sd.f_office, sd.mother_name, sd.mother_image_path, sd.m_occupation, sd.m_cell, sd.m_email, sd.m_office,sd.guardian_name, sd.guardian_image_path, sd.g_relation, sd.g_cell, sd.religion, sd.cast, sd.citizenship, sd.handicapped, sd.password FROM student_details sd LEFT JOIN faculty f ON sd.faculty_id = f.id LEFT JOIN batch b ON sd.batch_id = b.batch_id LEFT JOIN classes c ON sd.class_id = c.id LEFT JOIN section s ON sd.section_id = s.section_id WHERE sd.id = '" . $_SESSION['user_id'] . "';";
            $userDetails = $Database->sql($condition , true);
            view_require('dashboard/student-profile', ['data' => $userDetails]);
        }
        view_require('_parts/footer');
    }
}
new Setting();
