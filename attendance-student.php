<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class AttendanceStudent extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $sql= "SELECT 
        sa.id AS attendance_id,
        sa.attendance_date,
        sa.created_at,
        sa.first_in,
        sa.status,
        sd.id AS staff_id,
        sd.name AS staff_name,
        c.id AS class_id,
        c.name AS class_name,
        s.section_id,
        s.section_name
        FROM 
            staff_attendance sa
        INNER JOIN 
            staff_details sd ON sa.staff_id = sd.id
        INNER JOIN 
            classes c ON sa.class_id = c.id
        INNER JOIN 
            section s ON sa.section_id = s.section_id
        WHERE sa.staff_id = " . $_SESSION['user_id'] . "";
        $readResult = $db->sql($sql);
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/attendanceStudent', ['data' => $readResult]);
        view_require('_parts/footer');
    }
}
new AttendanceStudent();
