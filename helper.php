<!-- helper.php -->
<?php

function dd($value)
{
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
    die();
}

function view($path)
{
    header('Location: ./View/' . $path . '.php');
}

function controller($path)
{
    header('Location:./Controller/' . $path . '.php');
}
function input_sanitize($data)
{
    return trim(stripslashes(htmlspecialchars($data)));
}

function css($cssFiles)
{
    if ($cssFiles) {


        if (is_array($cssFiles)) {
            foreach ($cssFiles as $css) {
                $path = '/View/assets/css/' . $css . '.css';
                echo '<link rel="stylesheet" type="text/css" href="' . $path . '">';
            }
        } else {
            $path = '/View/assets/css/' . $cssFiles . '.css';
            echo '<link rel="stylesheet" type="text/css" href="' . $path . '">';
        }
    } else {
    }
}

function js($scriptNames)
{
    if ($scriptNames) {

        if (is_array($scriptNames)) {
            foreach ($scriptNames as $scriptName) {
                $file_path = 'View/assets/js/' . $scriptName . '.js';
                echo '<script src="' . $file_path . '"></script>';
            }
        } else {
            $file_path = 'View/assets/js/' . $scriptNames . '.js';
            echo '<script src="' . $file_path . '"></script>';
        }
    } else {
    }
}
function view_require($path, $data = [])
{
    extract($data);
    include "View/" . $path . '.php';
}

function getAccessFaculty($config = [], $faculties = [], $access = [])
    {
        $auth = $access['auth'];
        $all = false;
        $target =  isset($config['target']) ? $config['target'] : '#batchSelect';
        $name =  isset($config['name']) ? $config['name'] : '';
        $selected =  isset($config['selected']) ? $config['selected'] : '';

        $id =  isset($config['id']) ? $config['id'] : 'facultySelect';
        $_class =  isset($config['class']) ? $config['class'] : '';
        $defaultOption = $all ? "<option value=''> Select Program </option> <option value='All'> All Program</option>" : "<option value=''> Select Program</option>";
        $accessFaculty = [];
        $event = isset($config['event']) ? $config['event'] : null;

        $html = "<select class='form-control $_class' $event id='$id' name='$name'>$defaultOption";
        if($access['auth'] !== 'client') {
            $accessFaculty = array_pluck($access['accessClassSectionSubject'], 'faculty');
            $faculties = array_values(array_filter($faculties, function($item) use($accessFaculty) {
                return in_array($item->id, $accessFaculty);
            }));
        }
        foreach ($faculties as $faculty) {
            $isSelected = $selected == $faculty->id ? 'selected' : '';
            $html .= "<option  value='$faculty->id' $isSelected> $faculty->name </option>";
        }
        $html .= "</select>";
        $html .= "<script>
            (function(){
                    var target = $('$target')
                    target.find('option').prop('disabled', true);
                    $('#$id').on('change', function() {
                        var el = $(this);
                        target.find('option').prop('disabled', true);
                        var selectedFaculty = el.val();
                        target.find('option').each(function(){
                            if($(this).data('faculty') == selectedFaculty || $(this).val() == '') {
                                $(this).prop('disabled', false)
                            }
                        })
                        target.val('').trigger('change');
                        target.trigger('chosen:updated');
                    })
            })()
        </script>
        <style>
            select option:disabled {
                display:none;
            }
            ul.chosen-results li.disabled-result {
                display:none !important;
            }
        </style>
    ";
    echo $html;
    }
    function array_pluck($array, $key)
    {
        return array_map(function ($v) use ($key) {
            return is_object($v) ? $v->$key : $v[$key];
        }, $array);
    }
?>