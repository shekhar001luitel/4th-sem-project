<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class AttendanceStaff extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $query = "SELECT 
        f.faculty_name,
        f.id as faculty_id,
        b.batch_id, 
        b.batch_name, 
        c.display_name AS class_name, 
        c.id AS class_id, 
        s.section_name, 
        s.section_id 
      FROM faculty f
      JOIN batch b ON f.id = b.faculty_id
      JOIN classes c ON b.batch_id = c.batch_id 
      JOIN class_section cs ON c.id = cs.class_id 
      JOIN section s ON cs.section_id = s.section_id";

        $faculty_batches = [];

        $readResult = $db->sql($query);

        // Group batches by faculty
        foreach ($readResult as $row) {
            $faculty_name = $row['faculty_name'];
            $faculty_id = $row['faculty_id'];
            $batch_id = $row['batch_id'];
            $batch_name = $row['batch_name'];
            $class_id = $row['class_id'];
            $class_name = $row['class_name'];
            $section_name = $row['section_name'];
            $section_id = $row['section_id'];
    
            // Add batch information to the faculty's array
            if (!isset($faculty_batches[$faculty_name])) {
                $faculty_batches[$faculty_name] = [
                    'faculty_id' => $faculty_id
                ];
            }
    
            // Add batch, class, and section information
            if (!isset($faculty_batches[$faculty_name][$batch_id])) {
                $faculty_batches[$faculty_name][$batch_id] = [
                    'batch_name' => $batch_name,
                    'classes' => []
                ];
            }
    
            if (!isset($faculty_batches[$faculty_name][$batch_id]['classes'][$class_id])) {
                $faculty_batches[$faculty_name][$batch_id]['classes'][$class_id] = [
                    'class_name' => $class_name,
                    'sections' => []
                ];
            }
    
            $faculty_batches[$faculty_name][$batch_id]['classes'][$class_id]['sections'][] = [
                'section_id' => $section_id,
                'section_name' => $section_name
            ];
        }

        $sql=" SELECT * from staff_details ";
        $readResult = $db->sql($sql);
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/attendanceStaff', ['data' => $faculty_batches, 'teachers' => $readResult]);
        view_require('_parts/footer');
    }
}
new AttendanceStaff();
