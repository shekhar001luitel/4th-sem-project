<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';

class ReportController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Kathmandu');

        $this->check_login();
        $this->index();
    }

    public function index()
    {
        $db = new Database();
        $role = $_SESSION['role'];
        $attendanceData = [];
        

        if ($role == 'admin') {
            $attendanceQuery = "
                SELECT 
                    COUNT(*) AS total_students,
                    SUM(CASE WHEN attendance_status = 'present' THEN 1 ELSE 0 END) AS present_students,
                    SUM(CASE WHEN attendance_status = 'absent' THEN 1 ELSE 0 END) AS absent_students
                FROM student_attendance
                WHERE attendance_date = CURDATE();
            ";
            $attendanceData['students'] = $db->sql($attendanceQuery);

            $staffAttendanceQuery = "
                SELECT 
                (SELECT COUNT(*) FROM staff_details) AS total_staff,
                (SELECT COUNT(DISTINCT staff_id) 
                FROM staff_attendance 
                WHERE attendance_date = CURDATE()) AS count_staff_present,
                ((SELECT COUNT(*) FROM staff_details) - 
                (SELECT COUNT(DISTINCT staff_id) 
                FROM staff_attendance 
                WHERE attendance_date = CURDATE())) AS count_staff_absent;
            ";
            $attendanceData['staff'] = $db->sql($staffAttendanceQuery);
        } elseif ($role == 'teacher') {
            $teacherId = $_SESSION['user_id'];
            $attendanceQuery = "
                SELECT 
                sa.class_id, 
                sa.section_id, 
                c.name as class_name, 
                sc.section_name, 
                s.subject_name,
                COUNT(*) AS total_students,
                SUM(CASE WHEN sa.attendance_status = 'present' THEN 1 ELSE 0 END) AS present_students,
                SUM(CASE WHEN sa.attendance_status = 'absent' THEN 1 ELSE 0 END) AS absent_students
            FROM student_attendance sa
            INNER JOIN subject s ON sa.subject_id = s.subject_id
            INNER JOIN classes c ON sa.class_id = c.id
            INNER JOIN section sc ON sa.section_id = sc.section_id
            WHERE sa.staff_id = ".$teacherId." AND DATE(sa.attendance_date) = '".date('Y-m-d')."'
            GROUP BY sa.class_id, sa.section_id, s.subject_name;
            ";
            $attendanceData = $db->sql($attendanceQuery, [$teacherId]);
        } elseif ($role == 'student') {
            $studentId = $_SESSION['user_id'];
            $attendanceQuery = "
                SELECT 
                    sa.attendance_date, 
                    s.subject_name,
                    sa.attendance_status 
                FROM student_attendance sa
                INNER JOIN subject s ON sa.subject_id = s.subject_id
                WHERE sa.student_id = ".$_SESSION['user_id']."
                ORDER BY sa.attendance_date DESC, s.subject_name;
            ";
            $attendanceData = $db->sql($attendanceQuery, [$studentId]);
        }

        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/report_attendance', ['data' => $attendanceData]);
        view_require('_parts/footer');
    }
}

new ReportController();
