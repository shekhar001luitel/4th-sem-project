<?php
// session_start();
include "helper.php";
include 'Controller/Controller.php';
class Register extends Controller
{
    public function __construct()
    {
        $this->return_dashboard();
        $this->index();
    }
    public function index()
    {

        $cssFiles = ['login'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('login/register');
        view_require('_parts/footer');
    }
}

new Register();
