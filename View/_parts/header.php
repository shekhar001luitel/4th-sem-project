<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $title = isset($title) ? $title : null;
    $dynamicTitle = ucfirst(explode('.',explode('/' , $_SERVER['PHP_SELF'])[1])[0]) ;    
    ?>
    <title><?= $title ?: ($dynamicTitle)?></title>
    <?php
    $css = isset($css) ? $css : null;
    $css ? css($css) : print('');
    ?>
    <style>

    </style>
</head> 

<body>
    <div>
        