<?php 
function is_page_active($page_url) {
    $current_url = $_SERVER['PHP_SELF'];
    return strpos($current_url, $page_url) !== false;
}

?>
<div class="side-menu">
    <div class="section-title">Nihareeka</div>
    <ul class="menu">
        <li class="menu-item">
            <a href="dashboard.php" class="<?= is_page_active('/dashboard.php') ? 'active' : '' ?> ">Dashboard</a>
        </li>
        <?php if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'teacher') { ?>
        <?php if ($_SESSION['role'] == 'admin') { ?>
        <li class="menu-item">
            <div class="menu-item-header">Setup</div>
            <ul class="submenu">
                <li><a href="setup.php" class="<?= is_page_active('/setup.php') ? 'active' : '' ?>">Program</a></li>
                <li><a href="batch.php" class="<?= is_page_active('/batch.php') ? 'active' : '' ?>">Batch</a></li>
                <li><a href="classes.php" class="<?= is_page_active('/classes.php') ? 'active' : '' ?>">Class</a></li>
                <li><a href="section.php" class="<?= is_page_active('/section.php') ? 'active' : '' ?>">Section</a></li>
                <li><a href="subject.php" class="<?= is_page_active('/subject.php') ? 'active' : '' ?>">Subject</a></li>
                <li><a href="student.php" class="<?= is_page_active('/student.php') ? 'active' : '' ?>">Student</a></li>
                <li><a href="staff.php" class="<?= is_page_active('/staff.php') ? 'active' : '' ?>">Staff/Teacher</a></li>
            </ul>
        </li>
        <?php } ?>
        <li class="menu-item">
            <div class="menu-item-header">Attendance</div>
            <ul class="submenu">
        <?php if ($_SESSION['role'] == 'admin') { ?>
                <li><a href="attendance-staff.php" class="<?= is_page_active('/attendance-staff.php') ? 'active' : '' ?>">Staff/Teacher</a></li>
        <?php } ?>

        <?php if ($_SESSION['role'] == 'teacher') { ?>
                <li><a href="attendance-student.php" class="<?= is_page_active('/attendance-student.php') ? 'active' : '' ?>">Student</a></li>
        <?php } ?>

            </ul>
        </li>
        <?php } ?>
        <li class="menu-item">
            <div class="menu-item-header">Report</div>
            <ul class="submenu">
                <li><a href="report.php" class="<?= is_page_active('/report.php') ? 'active' : '' ?>">Report</a></li>
                <?php if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'teacher') { ?>
                <li><a href="attendance-list.php" class="<?= is_page_active('/attendance-list.php') ? 'active' : '' ?>">Attendance List</a></li>
                <?php } ?>
            </ul>
        </li>
        <!-- Student -->
        <!-- <li class="menu-item">
            <a href="stu-subject.php" class="<?= is_page_active('/setting.php') ? 'active' : '' ?> ">Subject</a>
        </li>
        <li class="menu-item">
            <a href="stu-teacher.php" class="<?= is_page_active('/setting.php') ? 'active' : '' ?> ">Teacher</a>
        </li> -->

        <!-- Teacher -->
        <!-- <li class="menu-item">
            <a href="stu-subject.php" class="<?= is_page_active('/setting.php') ? 'active' : '' ?> ">Subject</a>
        </li>
        <li class="menu-item">
            <a href="stu-teacher.php" class="<?= is_page_active('/setting.php') ? 'active' : '' ?> ">Teacher</a>
        </li> -->

        
        <li class="menu-item">
            <div class="menu-item-header">Setting</div>
            <ul class="submenu">
                <li><a href="setting.php" class="<?= is_page_active('/setting.php') ? 'active' : '' ?>">Setting</a></li>
            <?php if ($_SESSION['role'] == 'admin') { ?>
                <li><a href="user-management.php" class="<?= is_page_active('/user-management.php') ? 'active' : '' ?>">User Management</a></li>
                <li><a href="student-management.php" class="<?= is_page_active('/student-management.php') ? 'active' : '' ?>">Student Management</a></li>
                <li><a href="teacher-management.php" class="<?= is_page_active('/teacher-management.php') ? 'active' : '' ?>">Teacher Management</a></li>
            <?php } ?>
            </ul>
        </li>
        <li class="menu-item">
            <form id="logoutForm" method="post" action="Controller/LoginController.php">
                <input type="hidden" name="page" value="logout">
                <a onclick="document.getElementById('logoutForm').submit();" class="menu-item-logout ">Logout</a>
            </form>
        </li>
    </ul>
</div>
<style>
    .side-menu {
    font-family: Arial, sans-serif;
    padding: 20px;
    background-color: #b5b5b5;
}

.section-title {
    font-weight: bold;
    text-transform: uppercase;
    color: #333;
    margin-bottom: 20px;
}

.menu {
    list-style-type: none;
    padding: 0;
}

.menu-item {
    margin-bottom: 10px;
}

.heading{
    font-weight: bold;
    color: #666;
}

.menu-item a {
    text-decoration: none;
    /* color: #333; */
    display: block;
    padding: 8px;
    transition: background-color 0.3s ease;
}

.menu-item a:hover {
    background-color: #ddd;
}

.menu-item-header {
    font-weight: bold;
    color: #666;
    cursor: pointer;
}

.submenu {
    display: none;
    padding-left: 15px;
}

.menu-item:hover .submenu {
    display: block;
}

.active {
    font-weight: bold;
    color: #007bff;
}

.menu-item-logout {
    color: #dc3545;
    cursor: pointer;
}

</style>
<script>
    document.addEventListener('DOMContentLoaded', function() {
    const menuHeaders = document.querySelectorAll('.menu-item-header');

    menuHeaders.forEach(header => {
        // Add click event listener to each menu header
        header.addEventListener('click', () => {
            const submenu = header.nextElementSibling;
            const isActive = submenu.style.display === 'block';

            // Close all other open submenus
            document.querySelectorAll('.submenu').forEach(sub => {
                if (sub !== submenu) {
                    sub.style.display = 'none';
                }
            });

            // Toggle the display of the clicked submenu
            submenu.style.display = isActive ? 'none' : 'block';

            // Add/remove 'active' class to the clicked menu header
            header.classList.toggle('active', !isActive);
        });

        // Check if the submenu contains an active link initially
        const submenu = header.nextElementSibling;
        const isActive = submenu.querySelector('a.active') !== null;
        header.classList.toggle('active', isActive);
        submenu.style.display = isActive ? 'block' : 'none';
    });
});

</script>