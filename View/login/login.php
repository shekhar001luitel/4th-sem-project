<style>
    .login-container {
        text-align: center;
    }

    .user-type-selection {
        display: flex;
        justify-content: center;
        margin-bottom: 20px;
    }

    .user-type-btn {
        width: 120px;
        height: 120px;
        border-radius: 50%;
        background-color: #007bff; /* Blue background color */
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
        font-size: 20px;
        cursor: pointer;
        margin: 0 20px;
        transition: background-color 0.3s ease;
        position: relative;
    }

    .user-type-btn.selected {
        background-color: #0056b3; /* Darker shade of blue for selected button */
    }

    .user-type-btn::before {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        border-style: solid;
        z-index: -1;
    }

    .user-type-btn.student::before {
        border-width: 0 22px 38px 22px; /* This creates an equilateral triangle */
        border-color: transparent transparent #ffffff transparent;
        top: -38px; /* Adjust position of triangle */
        left: 50%;
        transform: translateX(-50%);
    }

    .user-type-btn.teacher::before {
        border-width: 0 22px 38px 22px;
        border-color: transparent transparent #ffffff transparent;
        top: 80px;
        left: calc(50% - 90px);
    }

    .user-type-btn.admin::before {
        border-width: 0 22px 38px 22px;
        border-color: transparent transparent #ffffff transparent;
        top: 80px;
        left: calc(50% + 90px);
    }

    .user-type-btn:hover {
        background-color: #0056b3; /* Darker shade of blue on hover */
    }

    .user-type-btn span {
        position: relative;
        z-index: 1;
    }
</style>

<div class="login-container">
    <h2>Login</h2>

    <!-- User type selection buttons -->
    <div class="user-type-selection">
        <div class="user-type-btn student" onclick="showLoginForm('student')">
            <span>Student</span>
        </div>
        <div class="user-type-btn teacher" onclick="showLoginForm('teacher')">
            <span>Teacher</span>
        </div>
        <div class="user-type-btn admin" onclick="showLoginForm('admin')">
            <span>Admin</span>
        </div>
    </div>

    <!-- Error or success messages -->
    <?php
    if (isset($_SESSION["errorMessage"])) {
    ?>
        <div class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
    <?php
        unset($_SESSION["errorMessage"]);
    } elseif (isset($_SESSION["successMessage"])) {
    ?>
        <div class="error-info" style="color: green;"><?php echo $_SESSION["successMessage"]; ?></div>
    <?php
        unset($_SESSION["successMessage"]);
    }
    ?>

    <!-- Login form -->
    <form id="loginForm" class="login-form" method="post" action="Controller/LoginController.php?page=login" onSubmit="return validate();" style="display: none;">
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="email">ID/EMAIL</label>
            </div>
            <input id="email" name="email" type="text" placeholder="ID">
            <span id="email_info" class="error-info"></span>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="password">Password</label>
            </div>
            <input id="password" name="password" type="password" placeholder="Password">
            <span id="password_info" class="error-info"></span>
        </div>
        <button type="submit">Login</button>
    </form>
    <!-- <a href="./register.php">
        <span class="button">Create New Account</span>
    </a> -->
</div>

<script>
    var selectedUserType = ""; // To keep track of the selected user type

    function showLoginForm(userType) {
        // Hide all error messages
        document.getElementById("email_info").innerHTML = "";
        document.getElementById("password_info").innerHTML = "";

        // Remove 'selected' class from previously selected button
        if (selectedUserType) {
            document.querySelector(".user-type-btn." + selectedUserType).classList.remove("selected");
        }

        // Display the login form
        document.getElementById("loginForm").style.display = "block";

        // Update selectedUserType and add 'selected' class to current button
        selectedUserType = userType;
        document.querySelector(".user-type-btn." + userType).classList.add("selected");

        // Depending on userType, you may want to set different actions or styles
        switch (userType) {
            case 'student':
                // Example: Update form action for student login
                document.getElementById("loginForm").action = "Controller/LoginController.php?page=login&type=student";
                break;
            case 'teacher':
                // Example: Update form action for teacher login
                document.getElementById("loginForm").action = "Controller/LoginController.php?page=login&type=teacher";
                break;
            case 'admin':
                // Example: Update form action for admin login
                document.getElementById("loginForm").action = "Controller/LoginController.php?page=login&type=admin";
                break;
            default:
                // Handle unexpected cases
                break;
        }
    }

    function validate() {
        var isValid = true;
        document.getElementById("email_info").innerHTML = "";
        document.getElementById("password_info").innerHTML = "";

        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;

        if (email.trim() === "") {
            document.getElementById("email_info").innerHTML = "<span style='color: red;'> * Email is required</span>";
            isValid = false;
        }
        if (password.trim() === "") {
            document.getElementById("password_info").innerHTML = "<span style='color: red;'> * Password is required</span>";
            isValid = false;
        }

        return isValid;
    }
</script>
