<div class="login-container">
    <h2>Register</h2>
    <?php
    // session_start();
    if (isset($_SESSION["errorMessage"])) {
    ?>
        <div class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
    <?php
        unset($_SESSION["errorMessage"]);
    }
    ?>
    <form class="login-form" method="post" action="Controller/LoginController.php?page=register" id="registerForm">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="email">Email</label>
            </div>
            <input id="email" name="email" type="email" placeholder="Email">
            <span id="email_info" class="error-info"></span>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="password">Password</label>
            </div>
            <input id="password" name="password" type="password" placeholder="Password">
            <span id="password_info" class="error-info"></span>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="confirm_password">Confirm Password</label>
            </div>
            <input id="confirm_password" name="confirm_password" type="password" placeholder="Confirm Password">
            <span id="confirm_password_info" class="error-info"></span>
        </div>
        <button type="submit">Register</button>
    </form>
    <a href="./login.php">
        <span class="button">Already have an account?</span>
    </a>
</div>

<script>
    document.getElementById("registerForm").addEventListener("submit", function(event) {
        if (!validate()) {
            event.preventDefault();
        }
    });

    function validate() {
        var isValid = true;
        document.getElementById("email_info").textContent = "";
        document.getElementById("password_info").textContent = "";
        document.getElementById("confirm_password_info").textContent = "";

        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("confirm_password").value;

        if (email.trim() === "") {
            document.getElementById("email_info").textContent = " * Email is required";
            isValid = false;
        }
        if (password.trim() === "") {
            document.getElementById("password_info").textContent = " * Password is required";
            isValid = false;
        }
        if (confirmPassword.trim() === "") {
            document.getElementById("confirm_password_info").textContent = " * Confirm Password is required";
            isValid = false;
        }
        if (password !== confirmPassword) {
            document.getElementById("confirm_password_info").textContent = " * Confirm Password should be the same";
            isValid = false;
        }

        return isValid;
    }
</script>