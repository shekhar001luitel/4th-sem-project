<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="#program">Program</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
            </div>
            </form>
        </div>
    </div>
    <div class="content">
        <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Program</h2>
                </div>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Details</th>
                        <th>Batch</th>
                    </tr>
                    <?php
                    foreach ($data as $key) :
                        echo '<tr>';
                        echo '<td><a href="#program_edit#' . $key['id'] . '" class="btn">' . ($key['faculty_name']) . '</a></td>';
                        echo '<td><a href="#program_view#' . $key['id'] . '" class="btn-red">View</a></td>';
                        echo '<td><a href="#' . $key['id'] . '" class="btn-green">View</a></td>';
                        echo '</tr>';
                    endforeach;
                    ?>
                </table>
            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h1>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener(
        "hashchange",
        () => {
            let hashValue = window.location.hash.split('#');
            console.log(hashValue);
            // Create Program
            if (hashValue[1] == "program") {
                fetch('Controller/SettingsController.php?page=inputProgram', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
            // View program
            else if (hashValue[1] === "program_view") {
                fetch('Controller/SettingsController.php?page=viewProgram', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
            // Edit Program
            else if (hashValue[1] === "program_edit") {
                fetch('Controller/SettingsController.php?page=editProgram', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
            // View Batch 
            ///
            else {
                fetch('Controller/SettingsController.php?page=programId', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[1]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                        // document.querySelector("div .create-batch").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
        },
        false,
    );
</script>