<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="#createSection">Section</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Section</h2>
                </div>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Add Class</th>
                    </tr>
                    <?php
                    // dd($data);
                    foreach ($data as $key) :
                        echo '<tr>';
                        echo '<td>' . ($key['section_name']). '</td>';
                        echo '<td><a href="#editSection#' . $key['section_id'] .'" class="btn-green">View</a></td>';
                        echo '</tr>';
                    endforeach;
                    ?>
                </table>
            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2>Class</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Section</h1>
                </table>
            </div>
        </div>
        <div class="content-2 class-table-list">
            <div class="recent-payments" id="sec-div">
                <div class="title">
                    <h2>Section</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Section Selected</h1>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener(
        "hashchange",
        () => {
            let hashValue = window.location.hash.split('#');
                // var data = {
                //     'hash': encodeURIComponent(hashValue[1]),
                // };
                if(hashValue[1] == 'createSection'){
                    var formData = new URLSearchParams(hashValue[1]);
                    console.log(formData);
                    fetch('Controller/SectionController.php?page=createSection', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: formData,
                            // body: 'hash=' + encodeURIComponent(hashValue[2]),
                        })
                        .then(response => response.text())
                        .then(data => {
                            document.querySelector("div .recent-payments").innerHTML = data
                            // document.querySelector("div .create-Classess").innerHTML = data
                        })
                        console.log(data)
                        .catch(error => console.error('Error:', error));
                        
                }else if (hashValue[1] === "editSection") {
                fetch('Controller/SectionController.php?page=editSection', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data

                        return fetch('Controller/SectionController.php?page=sectionClass', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: 'hash=' + encodeURIComponent(hashValue[2]),
                        });
                    })
                    .then(response => response.text())
                    .then(tableData => {
                        document.querySelector("#sec-div").innerHTML = tableData;
                    })
                    .catch(error => console.error('Error:', error));
                }
                else if (hashValue[1] === "updateSection") {
                fetch('Controller/SectionController.php?page=editSection', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
                }
            },
        false
    );
</script>
