<div class="container">
    <div class="header">
        <div class="nav">
            <div style="width: 85%;">
                <h1>Attendance Report</h1>
            </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red"><li>Logout</li></button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="cards">
            <div class="card">
                <div class="box">
                <h1>Attendance Report</h1>
                <?php if ($_SESSION['role'] == 'admin') { ?>
                    <h2>Student Attendance</h2>
                    <p>Total Students: <?php echo htmlspecialchars($data['students'][0]['total_students']); ?></p>
                    <p>Present Students: <?php echo htmlspecialchars($data['students'][0]['present_students']); ?></p>
                    <p>Absent Students: <?php echo htmlspecialchars($data['students'][0]['absent_students']); ?></p>

                    <h2>Staff Attendance</h2>
                    <p>Total Staff: <?php echo htmlspecialchars($data['staff'][0]['total_staff']); ?></p>
                    <p>Present Staff: <?php echo htmlspecialchars($data['staff'][0]['count_staff_present']); ?></p>
                    <p>Absent Staff: <?php echo htmlspecialchars($data['staff'][0]['count_staff_absent']); ?></p>
                <?php } elseif ($_SESSION['role'] == 'teacher') { ?>
                    <h2>Class Attendance</h2>
                    <?php foreach ($data as $class) {?>
                        <b><p>Class Names: <?php echo htmlspecialchars($class['class_name']); ?></p></b>
                        <p>Section Name: <?php echo htmlspecialchars($class['section_name']); ?></p>
                        <p>Subject Name: <?php echo htmlspecialchars($class['subject_name']); ?></p>
                        <p>Total Students: <?php echo htmlspecialchars($class['total_students']); ?></p>
                        <p>Present Students: <?php echo htmlspecialchars($class['present_students']); ?></p>
                        <p>Absent Students: <?php echo htmlspecialchars($class['absent_students']); ?></p>
                        <hr>
                    <?php } ?>
                <?php } elseif ($_SESSION['role'] == 'student') { ?>
                    <h2>Your Attendance</h2>
                    <table>
                        <tr>
                            <th>Date</th>
                            <th>Subject</th>
                            <th>Status</th>
                        </tr>
                        <?php foreach ($data as $attendance) { ?>
                            <tr>
                                <td><?php echo htmlspecialchars($attendance['attendance_date']); ?></td>
                                <td><?php echo htmlspecialchars($attendance['subject_name']); ?></td>
                                <td><?php echo htmlspecialchars($attendance['attendance_status']); ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .card{
        width: auto !important;
        height: auto !important;
    }
</style>