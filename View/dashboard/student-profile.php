<style>
    .form-container {
    background-color: #fff;
    border-radius: 8px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    padding: 20px;
    width: 100%;
    text-align: center;
    }
    .profile-image {
        width: 150px;
        height: 150px;
        object-fit: scale-down;
        border: 5px solid white;
        background-color: #fff;
        position: relative;
    }
</style>
<div class="container">
    <div class="header">
        <div class="nav">
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="form-container">
            <div class="profile-header">
                <img src="<?= $data['student_image'] ?>" alt="Profile Image" class="profile-image">
            </div>
            <div class="profile-details">
                <p><strong>Name:</strong> <span><?= $data['student_name'] ?></span></p>
                <p><strong>Name in Nepali:</strong> <span><?= $data['name_in_nepali'] ?></span></p>
                <p><strong>Gender:</strong> <span><?= $data['gender'] ?></span></p>
                <p><strong>Date of Birth (BS):</strong> <span><?= $data['dob_bs'] ?></span></p>
                <p><strong>Faculty:</strong> <span><?= $data['faculty_name'] ?></span></p>
                <p><strong>Batch:</strong> <span><?= $data['batch_name'] ?></span></p>
                <p><strong>Class:</strong> <span><?= $data['class_name'] ?></span></p>
                <p><strong>Section:</strong> <span><?= $data['section_name'] ?></span></p>
                <p><strong>Date of Birth (BS):</strong> <span><?= $data['dob_bs'] ?></span></p>
                <p><strong>Phone:</strong> <span><?= $data['phone'] ?></span></p>
                <p><strong>Nationality:</strong> <span><?= $data['nationality'] ?></span></p>
            </div>
        </div>
        <div class="form-container">
            <p class="section-title">Permanent Address</p>
            <p><strong>Province:</strong> <span><?= $data['permanent_province'] ?></span></p>
            <p><strong>District:</strong> <span><?= $data['permanent_district'] ?></span></p>
            <p><strong>Municipality:</strong> <span><?= $data['permanent_municipality'] ?></span></p>
            <p><strong>Address:</strong> <span><?= $data['permanent_address'] ?></span></p>
        </div>
        <div class="form-container">
            <p class="section-title">Family Details</p>
            <div class="profile-header">
                <?php if($data['father_image_path']){?>
                <img src="<?= $data['father_image_path'] ?>" alt="Profile Image" class="profile-image">
                <?php }?>
            </div>
            <p><strong>Father's Name:</strong> <span><?= $data['father_name'] ?></span></p>
            <p><strong>Father's Occupation:</strong> <span><?= $data['f_occupation'] ?></span></p>
            <p><strong>Father's Phone:</strong> <span><?= $data['f_cell'] ?></span></p>
            <p><strong>Father's Mail:</strong> <span><?= $data['f_mail'] ?></span></p>
            <p><strong>Father's Office:</strong> <span><?= $data['f_office'] ?></span></p>
            <div class="profile-header">
            <?php if($data['mother_image_path']){?>
                <img src="<?= $data['mother_image_path'] ?>" alt="Profile Image" class="profile-image">
                <?php }?>
            </div>
            <p><strong>Mother's Name:</strong> <span><?= $data['mother_name'] ?></span></p>
            <p><strong>Mother's Occupation:</strong> <span><?= $data['m_occupation'] ?></span></p>
            <p><strong>Mother's Phone:</strong> <span><?= $data['m_cell'] ?></span></p>
            <p><strong>Mother's Mail:</strong> <span><?= $data['m_mail'] ?></span></p>
            
            <?php if($data['guardian_name']){?>
            <div class="profile-header">

            <?php if($data['guardian_image_path']){?>
                <img src="<?= $data['guardian_image_path'] ?>" alt="Profile Image" class="profile-image">
                <?php }?>
            </div>
            <p><strong>Guardain's Name:</strong> <span><?= $data['guardian_name'] ?></span></p>
            <p><strong>Guardain's Relation:</strong> <span><?= $data['g_relation'] ?></span></p>
            <p><strong>Guardain's Phone:</strong> <span><?= $data['g_cell'] ?></span></p>
            <p><strong>Mother's Mail:</strong> <span><?= $data['m_mail'] ?></span></p>
            <?php }?>
        </div>
        <div class="form-container">
            <p class="section-title">Other Details</p>
            <p><strong>Religion:</strong> <span><?= $data['religion'] ?></span></p>
            <p><strong>Caste:</strong> <span><?= $data['cast'] ?></span></p>
            <p><strong>Citizenship:</strong> <span><?= $data['citizenship'] ?></span></p>
            <p><strong>Handicapped:</strong> <span><?= $data['handicapped'] == '1' ?'Yes':'No' ?></span></p>
        </div>
        <div class="form-container">
            <?php
            // session_start();
            if (isset($_SESSION["errorMessage"])) {
            ?>
                <div class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
            <?php
                unset($_SESSION["errorMessage"]);
            } elseif (isset($_SESSION["successMessage"])) {
            ?>
                <div class="error-info" style="color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
            <?php
                unset($_SESSION["successMessage"]);
            }
            ?>
            <form class="login-form" method="post" action="Controller/StaffController.php?page=update">
                <div>
                    <div style="display: flex; justify-content: space-between;">
                        <label for="password"><strong>Password</strong></label>
                    </div>
                    <input id="password" name="password" type="password" placeholder="Password">
                    <span id="password_info" class="error-info"></span>
                </div>
                <button class="btn-green" type="submit">Update</button>
            </form>

        </div>
    </div>
</div>