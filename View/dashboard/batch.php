<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="/setup.php">Program</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Program</h2>
                </div>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Batch</th>
                    </tr>
                    <?php
                    foreach ($data as $key) :
                        echo '<tr>';
                        echo '<td>' . ($key['faculty_name']) . '</td>';
                        echo '<td><a href="#createBatch#' . $key['id'] . '#' . '" class="btn-green">Add</a></td>';
                        echo '</tr>';
                    endforeach;
                    ?>
                </table>
            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h1>
                </table>
            </div>
        </div>
        <div class="content-2 create-batch">
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h1>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener(
        "hashchange",
        () => {
            let hashValue = window.location.hash.split('#');
            console.log(hashValue);

            if (hashValue[1] == "createBatch") {
                var data = {
                    'hash': encodeURIComponent(hashValue[2]),
                };

                var formData = new URLSearchParams(data);

                // First fetch request
                fetch('Controller/BatchController.php?page=inputBatch', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: formData,
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector(".recent-payments").innerHTML = data;

                        // Second fetch request
                        return fetch('Controller/BatchController.php?page=tableBatch', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: formData,
                        });
                    })
                    .then(response => response.text())
                    .then(tableData => {
                        document.querySelector(".content-2.create-batch .recent-payments").innerHTML = tableData;
                    })
                    .catch(error => console.error('Error:', error));
            } else {
                var data = {
                    'hash': encodeURIComponent(hashValue[2]),
                    'program': hashValue[3],
                };

                var formData = new URLSearchParams(data);
                fetch('Controller/BatchController.php?page=batchEdit', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: formData,
                        // body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                        // document.querySelector("div .create-batch").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
        },
        false
    );
</script>
