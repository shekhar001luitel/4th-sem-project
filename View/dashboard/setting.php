<div class="container">
    <div class="header">
        <div class="nav">
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="login-container">
            <h2>Update</h2>
            <?php
            // session_start();
            if (isset($_SESSION["errorMessage"])) {
            ?>
                <div class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
            <?php
                unset($_SESSION["errorMessage"]);
            } elseif (isset($_SESSION["successMessage"])) {
            ?>
                <div class="error-info" style="color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
            <?php
                unset($_SESSION["successMessage"]);
            }
            ?>
            <form class="login-form" method="post" action="Controller/SettingsController.php?page=update" onSubmit="return validate();">
                <div>
                    <div style="display: flex; justify-content: space-between; ">
                        <label for="email">Email</label>
                    </div>
                    <input id="email" name="email" type="email" value="<?= $data['email'] ?>" placeholder="Email">
                    <span id="email_info" class="error-info"></span>
                </div>
                <div>
                    <div style="display: flex; justify-content: space-between;">
                        <label for="password">Password</label>
                    </div>
                    <input id="password" name="password" type="password" placeholder="Password">
                    <span id="password_info" class="error-info"></span>
                </div>
                <button class="btn-green" type="submit">Update</button>
            </form>
            <form class="login-form" method="post" action="Controller/SettingsController.php?page=delete" onSubmit="return confirmMessage(event);">
                <button class="btn-red" type="submit">Delete</button>
            </form>

        </div>

        <script>
            function validate() {
                var isValid = true;
                document.getElementById("email_info").innerHTML = "";

                var email = document.getElementById("email").value;

                if (email.trim() === "") {
                    document.getElementById("email_info").innerHTML = "<span style='color: red ; display: flex; justify-content: space-between;'> * Email is required</span>";
                    isValid = false;
                }
                return isValid;
            }

            function confirmMessage(event) {
                var userConfirmed = confirm("Are you sure you want to delete the account?");

                if (!userConfirmed) {
                    event.preventDefault();
                }
            }
        </script>
    </div>
</div>