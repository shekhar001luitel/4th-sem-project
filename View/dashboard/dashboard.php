<div class="container">
    <div class="header">
        <div class="nav">
            <div class="user">
                <?php if ($_SESSION['role'] == 'student') { ?>
            <h1>STUDENT</h1>
        <?php } elseif ($_SESSION['role'] == 'admin') { ?>
            <h1>ADMIN</h1>
        <?php } elseif ($_SESSION['role'] == 'teacher') { ?>
            <h1>TEACHER</h1>
        <?php } ?>
    </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red"><li>Logout</li></button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="cards">
            <div class="card">
                <div class="box">
                    <h1>Absent Today</h1>
                    <h3>Students</h3>
                    <h1><?php echo htmlspecialchars($data['today_student_absent'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Absent Today</h1>
                    <h3>Teachers</h3>
                    <h1><?php echo htmlspecialchars(($data['total_staff_members'] ?? 0) - ($data['today_staff_present'] ?? 0)); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Students</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_students'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Faculty</h1>
                    <h3>Members</h3>
                    <h1><?php echo htmlspecialchars($data['total_faculty_members'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Batches</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_batches'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Classes</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_classes'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Sections</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_sections'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Subjects</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_subjects'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Staff</h1>
                    <h3>Members</h3>
                    <h1><?php echo htmlspecialchars($data['total_staff_members'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Male Students</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_male_students'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Female Students</h1>
                    <h3>College</h3>
                    <h1><?php echo htmlspecialchars($data['total_female_students'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Male Staff</h1>
                    <h3>Members</h3>
                    <h1><?php echo htmlspecialchars($data['total_male_staff'] ?? 0); ?></h1>
                </div>
            </div>
            <div class="card">
                <div class="box">
                    <h1>Total Female Staff</h1>
                    <h3>Members</h3>
                    <h1><?php echo htmlspecialchars($data['total_female_staff'] ?? 0); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .box {
        margin : 5px;
    }
</style>
