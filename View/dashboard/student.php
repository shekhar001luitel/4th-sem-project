<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="#createSection">Section</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content" style="padding:15px;">
    <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <form method="POST" action="Controller/StudentController.php?page=inputStudent" enctype="multipart/form-data">
            <h3>
                Basic Info
            </h3>
            <div>
                <div>
                    <div>
                        <label>Student's Image</label>
                        <input type="file"  data-resize="600" name="student_image" accept=".jpg,.png,.jpeg">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Full Name *</label>
                        <input type="text" name="name"  style="text-transform: capitalize;" required="">
                    </div>
                </div>
                <div>
                    <div>
                        <label>नाम (देवनागरीमा) </label>
                        <input type="text" name="name_in_nepali">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Gender *</label>
                        <select name="gender"  required="">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>Date of Birth</label>
                        <input type="date" name="dob_bs" placeholder="YYYY-MM-DD" class="" autocomplete="off" style="width:100%" oninput="this.value = this.value.replace(/[^0-9 -]/g, '').replace(/(\..*?)\..*/g, '$1').replace(/^0[^.]/, '0');">
                    </div>
                </div>
            </div>
            <hr>
            <br>
            <!-- start academic info -->
            <hr>
            <h3>
                Academic Info
            </h3>
            <div>
                <div>
                    <label>Program</label>
                    <select  id="program" name="faculty">
                        <option value="">Select Program</option>
                        <?php foreach ($data as $faculty => $batches) { ?>
                            <option value="<?= $batches['faculty_id'] ?>"><?= $faculty ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <label>Batch</label>
                    <select  id="batchSelect" name="batch">
                        <option value="" selected>Select Batch</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <label>Class *</label>
                    <select  id="mark" name="class">
                        <option value="" selected>Select Class</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <label>Section *</label>
                    <select  id="series" name="section">
                        <option value="" selected>Select Section</option>
                    </select>
                </div>
            </div>
            <script>
                (function() {
                    var programSelect = document.getElementById('program');
                    var batchSelect = document.getElementById('batchSelect');
                    var classSelect = document.getElementById('mark');
                    var sectionSelect = document.getElementById('series');

                    programSelect.addEventListener('change', function() {
                        var selectedFacultyId = this.value;
                        var selectedProgram = programSelect.options[programSelect.selectedIndex].text;
                        batchSelect.innerHTML = '<option value="" selected>Select Batch</option>'; // Reset batch options
                        classSelect.innerHTML = '<option value="" selected>Select Class</option>'; // Reset class options
                        sectionSelect.innerHTML = '<option value="" selected>Select Section</option>'; // Reset section options

                        // Populate batch options based on the selected faculty
                        if (selectedProgram !== '') {
                            var facultyData = <?= json_encode($data) ?>;
                            var batches = facultyData[selectedProgram];
                            if (batches) {
                                for (var batchId in batches) {
                                    if (batchId !== 'faculty_id') {
                                        var batchName = batches[batchId]['batch_name'];
                                        batchSelect.innerHTML += '<option value="' + batchId + '">' + batchName + '</option>';
                                    }
                                }
                            }
                        }
                    });

                    batchSelect.addEventListener('change', function() {
                        var selectedBatchId = this.value;
                        var selectedBatch = batchSelect.options[batchSelect.selectedIndex].text;
                        classSelect.innerHTML = '<option value="" selected>Select Class</option>'; // Reset class options
                        sectionSelect.innerHTML = '<option value="" selected>Select Section</option>'; // Reset section options

                        // Populate class options based on the selected batch
                        if (selectedBatch !== '') {
                            var facultyData = <?= json_encode($data) ?>;
                            var batches = facultyData[programSelect.options[programSelect.selectedIndex].text];
                            if (batches && batches[selectedBatchId]) {
                                var classes = batches[selectedBatchId]['classes'];
                                for (var classId in classes) {
                                    var className = classes[classId]['class_name'];
                                    classSelect.innerHTML += '<option value="' + classId + '">' + className + '</option>';
                                }
                            }
                        }
                    });

                    classSelect.addEventListener('change', function() {
                        var selectedClassId = this.value;
                        sectionSelect.innerHTML = '<option value="" selected>Select Section</option>'; // Reset section options
                        // Populate section options based on the selected class
                        if (selectedClassId !== '') {
                            var facultyData = <?= json_encode($data) ?>;
                            var batches = facultyData[programSelect.options[programSelect.selectedIndex].text];
                            if (batches) {
                                for (var batchId in batches) {
                                    if (batchId !== 'faculty_id') {
                                        var classes = batches[batchId]['classes'];
                                        for (var classId in classes) {
                                            if (classId === selectedClassId) {
                                                var sections = classes[classId]['sections'];
                                                for (var i = 0; i < sections.length; i++) {
                                                    var sectionId = sections[i]['section_id'];
                                                    var sectionName = sections[i]['section_name'];
                                                    sectionSelect.innerHTML += '<option value="' + sectionId + '">' + sectionName + '</option>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                })();
            </script>

            <!-- end academic info -->
            <!-- start personal info -->
            <hr>
            <h3>
                Personal Info
            </h3>
            <div>
                <div>
                    <div>
                        <label>Phone</label>
                        <input type="number" min="0" name="phone">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Nationality</label>
                        <select name="nationality" id="nationality">
                            <option value="">Select Nationality</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Brazil">Brazil</option>
                            <option value="Brunei">Brunei</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                            <option value="Cabo Verde">Cabo Verde</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo (Congo-Brazzaville)">Congo (Congo-Brazzaville)</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czechia (Czech Republic)">Czechia (Czech Republic)</option>
                            <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Eswatini (fmr. Swaziland)">Eswatini (fmr. Swaziland)</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Greece">Greece</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Holy See">Holy See</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Iran">Iran</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Laos">Laos</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libya">Libya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Micronesia">Micronesia</option>
                            <option value="Moldova">Moldova</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montenegro">Montenegro</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar (formerly Burma)">Myanmar (formerly Burma)</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="North Korea">North Korea</option>
                            <option value="North Macedonia (formerly Macedonia)">North Macedonia (formerly Macedonia)</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Palestine State">Palestine State</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Romania">Romania</option>
                            <option value="Russia">Russia</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                            <option value="Saint Lucia">Saint Lucia</option>
                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                            <option value="Samoa">Samoa</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia">Serbia</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="South Korea">South Korea</option>
                            <option value="South Sudan">South Sudan</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="State of Palestine">State of Palestine</option>
                            <option value="Sudan">Sudan</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Syria">Syria</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania">Tanzania</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Timor-Leste">Timor-Leste</option>
                            <option value="Togo">Togo</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States of America">United States of America</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Vietnam">Vietnam</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div>
                        <label>Province</label>
                        <select name="permanent_province" id="p-province" class="p-province form-control">
                            <option value="">Select Province</option>
                            <option value="Koshi">Koshi</option>
                            <option value="Madhesh">Madhesh</option>
                            <option value="Bagmati">Bagmati</option>
                            <option value="Gandaki">Gandaki</option>
                            <option value="Lumbini">Lumbini</option>
                            <option value="Karnali">Karnali</option>
                            <option value="Sudurpashchim">Sudurpashchim</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>District</label>
                        <select name="permanent_district" id="p-district" class="p-district form-control">
                            <option value="">Select District</option>
                            <option value="Panchthar" data-id="2">Panchthar</option>
                            <option value="Ilam" data-id="3">Ilam</option>
                            <option value="Jhapa" data-id="4">Jhapa</option>
                            <option value="Morang" data-id="5">Morang</option>
                            <option value="Sunsari" data-id="6">Sunsari</option>
                            <option value="Terhathum" data-id="8">Terhathum</option>
                            <option value="Bhojpur" data-id="9">Bhojpur</option>
                            <option value="Sankhuwasabha" data-id="10">Sankhuwasabha</option>
                            <option value="Solukhumbu" data-id="11">Solukhumbu</option>
                            <option value="Khotang" data-id="12">Khotang</option>
                            <option value="Okhaldhunga" data-id="13">Okhaldhunga</option>
                            <option value="Udayapur" data-id="14">Udayapur</option>
                            <option value="Saptari" data-id="16">Saptari</option>
                            <option value="Sindhuli" data-id="20">Sindhuli</option>
                            <option value="Dolakha" data-id="22">Dolakha</option>
                            <option value="Rasuwa" data-id="23">Rasuwa</option>
                            <option value="Nuwakot" data-id="25">Nuwakot</option>
                            <option value="Lalitpur" data-id="28">Lalitpur</option>
                            <option value="Makwanpur" data-id="31">Makwanpur</option>
                            <option value="Bara" data-id="33">Bara</option>
                            <option value="Parsa" data-id="34">Parsa</option>
                            <option value="Kapilbastu" data-id="38">Kapilbastu</option>
                            <option value="Arghakhanchi" data-id="40">Arghakhanchi</option>
                            <option value="Syangja" data-id="42">Syangja</option>
                            <option value="Gorkha" data-id="44">Gorkha</option>
                            <option value="Kaski" data-id="46">Kaski</option>
                            <option value="Manang" data-id="47">Manang</option>
                            <option value="Baglung" data-id="50">Baglung</option>
                            <option value="Dang" data-id="52">Dang</option>
                            <option value="Rolpa" data-id="54">Rolpa</option>
                            <option value="Mugu" data-id="58">Mugu</option>
                            <option value="Jumla" data-id="60">Jumla</option>
                            <option value="Dailekh" data-id="63">Dailekh</option>
                            <option value="Surkhet" data-id="64">Surkhet</option>
                            <option value="Banke" data-id="66">Banke</option>
                            <option value="Doti" data-id="68">Doti</option>
                            <option value="Bajura" data-id="70">Bajura</option>
                            <option value="Baitadi" data-id="73">Baitadi</option>
                            <option value="Kanchanpur" data-id="75">Kanchanpur</option>
                            <option value="Rukum East" data-id="541">Rukum East</option>
                            <option value="Taplejung" data-id="1">Taplejung</option>
                            <option value="Dhankuta" data-id="7">Dhankuta</option>
                            <option value="Siraha" data-id="15">Siraha</option>
                            <option value="Dhanusa" data-id="17">Dhanusa</option>
                            <option value="Mahottari" data-id="18">Mahottari</option>
                            <option value="Sarlahi" data-id="19">Sarlahi</option>
                            <option value="Ramechhap" data-id="21">Ramechhap</option>
                            <option value="Sindhupalchok" data-id="24">Sindhupalchok</option>
                            <option value="Dhading" data-id="26">Dhading</option>
                            <option value="Kathmandu" data-id="27">Kathmandu</option>
                            <option value="Bhaktapur" data-id="29">Bhaktapur</option>
                            <option value="Kavrepalanchok" data-id="30">Kavrepalanchok</option>
                            <option value="Rautahat" data-id="32">Rautahat</option>
                            <option value="Chitwan" data-id="35">Chitwan</option>
                            <option value="Rupandehi" data-id="37">Rupandehi</option>
                            <option value="Palpa" data-id="39">Palpa</option>
                            <option value="Gulmi" data-id="41">Gulmi</option>
                            <option value="Tanahu" data-id="43">Tanahu</option>
                            <option value="Lamjung" data-id="45">Lamjung</option>
                            <option value="Mustang" data-id="48">Mustang</option>
                            <option value="Myagdi" data-id="49">Myagdi</option>
                            <option value="Parbat" data-id="51">Parbat</option>
                            <option value="Pyuthan" data-id="53">Pyuthan</option>
                            <option value="Salyan" data-id="55">Salyan</option>
                            <option value="Dolpa" data-id="57">Dolpa</option>
                            <option value="Humla" data-id="59">Humla</option>
                            <option value="Kalikot" data-id="61">Kalikot</option>
                            <option value="Jajarkot" data-id="62">Jajarkot</option>
                            <option value="Bardiya" data-id="65">Bardiya</option>
                            <option value="Kailali" data-id="67">Kailali</option>
                            <option value="Achham" data-id="69">Achham</option>
                            <option value="Bajhang" data-id="71">Bajhang</option>
                            <option value="Darchula" data-id="72">Darchula</option>
                            <option value="Dadeldhura" data-id="74">Dadeldhura</option>
                            <option value="Nawalparasi East" data-id="481">Nawalparasi East</option>
                            <option value="Nawalparasi West" data-id="482">Nawalparasi West</option>
                            <option value="Rukum West" data-id="542">Rukum West</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>Municipality</label>
                        <select name="permanent_municipality" id="p-municipality" class="p-municipality form-control">
                            <option value="">Select Municipality</option>
                            <option value="Phaktanglung" data-id="">Phaktanglung</option>
                            <option value="Sirijangha" data-id="">Sirijangha</option>
                            <option value="Illam" data-id="">Illam</option>
                            <option value="Mai" data-id="">Mai</option>
                            <option value="Rong" data-id="">Rong</option>
                            <option value="Suryodaya" data-id="">Suryodaya</option>
                            <option value="Sunwarshi" data-id="">Sunwarshi</option>
                            <option value="Bhokraha" data-id="">Bhokraha</option>
                            <option value="Gadhi" data-id="">Gadhi</option>
                            <option value="Aathrai" data-id="">Aathrai</option>
                            <option value="Laligurans" data-id="">Laligurans</option>
                            <option value="Phedap" data-id="">Phedap</option>
                            <option value="Tyamkemaiyung" data-id="">Tyamkemaiyung</option>
                            <option value="Chainpur" data-id="">Chainpur</option>
                            <option value="Khandbari" data-id="">Khandbari</option>
                            <option value="Makalu" data-id="">Makalu</option>
                            <option value="Sabhapokhari" data-id="">Sabhapokhari</option>
                            <option value="Chhatreshwori" data-id="">Chhatreshwori</option>
                            <option value="Darma" data-id="">Darma</option>
                            <option value="Kapurkot" data-id="">Kapurkot</option>
                            <option value="Tribeni" data-id="">Tribeni</option>
                            <option value="Chhayanath Rara" data-id="">Chhayanath Rara</option>
                            <option value="Soru" data-id="">Soru</option>
                            <option value="Bhagawatimai" data-id="">Bhagawatimai</option>
                            <option value="Dullu" data-id="">Dullu</option>
                            <option value="Dungeshwor" data-id="">Dungeshwor</option>
                            <option value="Narayan" data-id="">Narayan</option>
                            <option value="Duduwa" data-id="">Duduwa</option>
                            <option value="Kohalpur" data-id="">Kohalpur</option>
                            <option value="Badikedar" data-id="">Badikedar</option>
                            <option value="Jorayal" data-id="">Jorayal</option>
                            <option value="Sayal" data-id="">Sayal</option>
                            <option value="Shikhar" data-id="">Shikhar</option>
                            <option value="Bhume" data-id="">Bhume</option>
                            <option value="Sisne" data-id="">Sisne</option>
                            <option value="Tribeni" data-id="">Tribeni</option>
                            <option value="Itahari" data-id="">Itahari</option>
                            <option value="Dharmadevi" data-id="">Dharmadevi</option>
                            <option value="Silichong" data-id="">Silichong</option>
                            <option value="Mapya Dudhkoshi" data-id="">Mapya Dudhkoshi</option>
                            <option value="Khumbupasanglahmu" data-id="">Khumbupasanglahmu</option>
                            <option value="Likhupike" data-id="">Likhupike</option>
                            <option value="Solududhakunda" data-id="">Solududhakunda</option>
                            <option value="Belaka" data-id="">Belaka</option>
                            <option value="Katari" data-id="">Katari</option>
                            <option value="Rautamai" data-id="">Rautamai</option>
                            <option value="Bagmati" data-id="">Bagmati</option>
                            <option value="Godawari_Lalitpur" data-id="">Godawari_Lalitpur</option>
                            <option value="Konjyosom" data-id="">Konjyosom</option>
                            <option value="Changunarayan" data-id="">Changunarayan</option>
                            <option value="Suryabinayak" data-id="">Suryabinayak</option>
                            <option value="Meringden" data-id="">Meringden</option>
                            <option value="Tumbewa" data-id="">Tumbewa</option>
                            <option value="Fakphokthum" data-id="">Fakphokthum</option>
                            <option value="Mangsebung" data-id="">Mangsebung</option>
                            <option value="Birtamod" data-id="">Birtamod</option>
                            <option value="Kamal" data-id="">Kamal</option>
                            <option value="Belbari" data-id="">Belbari</option>
                            <option value="Katahari" data-id="">Katahari</option>
                            <option value="Bhojpur" data-id="">Bhojpur</option>
                            <option value="Triyuga" data-id="">Triyuga</option>
                            <option value="Bakaiya" data-id="">Bakaiya</option>
                            <option value="Indrasarowar" data-id="">Indrasarowar</option>
                            <option value="Manahari" data-id="">Manahari</option>
                            <option value="Thaha" data-id="">Thaha</option>
                            <option value="Bahudaramai" data-id="">Bahudaramai</option>
                            <option value="Kalika" data-id="">Kalika</option>
                            <option value="Rapti" data-id="">Rapti</option>
                            <option value="Banganga" data-id="">Banganga</option>
                            <option value="Kapilbastu" data-id="">Kapilbastu</option>
                            <option value="Anbukhaireni" data-id="">Anbukhaireni</option>
                            <option value="Bandipur" data-id="">Bandipur</option>
                            <option value="Myagde" data-id="">Myagde</option>
                            <option value="Loghekar Damodarkunda" data-id="">Loghekar Damodarkunda</option>
                            <option value="Thasang" data-id="">Thasang</option>
                            <option value="Badigad" data-id="">Badigad</option>
                            <option value="Dhorpatan" data-id="">Dhorpatan</option>
                            <option value="Galkot" data-id="">Galkot</option>
                            <option value="Babai" data-id="">Babai</option>
                            <option value="Ghorahi" data-id="">Ghorahi</option>
                            <option value="Lamahi" data-id="">Lamahi</option>
                            <option value="Shantinagar" data-id="">Shantinagar</option>
                            <option value="Dolpo Buddha" data-id="">Dolpo Buddha</option>
                            <option value="Kaike" data-id="">Kaike</option>
                            <option value="Barbardiya" data-id="">Barbardiya</option>
                            <option value="Madhuwan" data-id="">Madhuwan</option>
                            <option value="Chitwan National Park" data-id="">Chitwan National Park</option>
                            <option value="Aathrai Tribeni" data-id="">Aathrai Tribeni</option>
                            <option value="Haripurwa" data-id="">Haripurwa</option>
                            <option value="Gokulganga" data-id="">Gokulganga</option>
                            <option value="Likhu Tamakoshi" data-id="">Likhu Tamakoshi</option>
                            <option value="Dhobini" data-id="">Dhobini</option>
                            <option value="Jirabhawani" data-id="">Jirabhawani</option>
                            <option value="Kalikamai" data-id="">Kalikamai</option>
                            <option value="Parsagadhi" data-id="">Parsagadhi</option>
                            <option value="Paterwasugauli" data-id="">Paterwasugauli</option>
                            <option value="Pokhariya" data-id="">Pokhariya</option>
                            <option value="SakhuwaPrasauni" data-id="">SakhuwaPrasauni</option>
                            <option value="Basbariya" data-id="">Basbariya</option>
                            <option value="Manthali" data-id="">Manthali</option>
                            <option value="Ramechhap" data-id="">Ramechhap</option>
                            <option value="Sunapati" data-id="">Sunapati</option>
                            <option value="Umakunda" data-id="">Umakunda</option>
                            <option value="Paribartan" data-id="">Paribartan</option>
                            <option value="Lungri" data-id="">Lungri</option>
                            <option value="Sunchhahari" data-id="">Sunchhahari</option>
                            <option value="Duhabi" data-id="">Duhabi</option>
                            <option value="Bhaktapur" data-id="">Bhaktapur</option>
                            <option value="Rohini" data-id="">Rohini</option>
                            <option value="Sainamaina" data-id="">Sainamaina</option>
                            <option value="Siddharthanagar" data-id="">Siddharthanagar</option>
                            <option value="Siyari" data-id="">Siyari</option>
                            <option value="Sudhdhodhan" data-id="">Sudhdhodhan</option>
                            <option value="Tillotama" data-id="">Tillotama</option>
                            <option value="Rolpa" data-id="">Rolpa</option>
                            <option value="Runtigadi" data-id="">Runtigadi</option>
                            <option value="Sunil Smriti" data-id="">Sunil Smriti</option>
                            <option value="Thawang" data-id="">Thawang</option>
                            <option value="Dewanganj" data-id="">Dewanganj</option>
                            <option value="Karjanha" data-id="">Karjanha</option>
                            <option value="Bagmati" data-id="">Bagmati</option>
                            <option value="Barahathawa" data-id="">Barahathawa</option>
                            <option value="Bishnu" data-id="">Bishnu</option>
                            <option value="Bramhapuri" data-id="">Bramhapuri</option>
                            <option value="Chandranagar" data-id="">Chandranagar</option>
                            <option value="Godaita" data-id="">Godaita</option>
                            <option value="Hariwan" data-id="">Hariwan</option>
                            <option value="Ishworpur" data-id="">Ishworpur</option>
                            <option value="Kabilasi" data-id="">Kabilasi</option>
                            <option value="Kaudena" data-id="">Kaudena</option>
                            <option value="Lalbandi" data-id="">Lalbandi</option>
                            <option value="Ghanglekh" data-id="">Ghanglekh</option>
                            <option value="Maiwakhola" data-id="">Maiwakhola</option>
                            <option value="Inaruwa" data-id="">Inaruwa</option>
                            <option value="Golanjor" data-id="">Golanjor</option>
                            <option value="Mayadevi" data-id="">Mayadevi</option>
                            <option value="Shivaraj" data-id="">Shivaraj</option>
                            <option value="Koshi" data-id="">Koshi</option>
                            <option value="Ramdhuni" data-id="">Ramdhuni</option>
                            <option value="Marin" data-id="">Marin</option>
                            <option value="Phikkal" data-id="">Phikkal</option>
                            <option value="Sunkoshi" data-id="">Sunkoshi</option>
                            <option value="Bhotekoshi" data-id="">Bhotekoshi</option>
                            <option value="Benighat Rorang" data-id="">Benighat Rorang</option>
                            <option value="Dhunibesi" data-id="">Dhunibesi</option>
                            <option value="Gajuri" data-id="">Gajuri</option>
                            <option value="Gangajamuna" data-id="">Gangajamuna</option>
                            <option value="Jwalamukhi" data-id="">Jwalamukhi</option>
                            <option value="Netrawati Dabjong" data-id="">Netrawati Dabjong</option>
                            <option value="Neelakantha" data-id="">Neelakantha</option>
                            <option value="Siddhalek" data-id="">Siddhalek</option>
                            <option value="Panchkhal" data-id="">Panchkhal</option>
                            <option value="Makawanpurgadhi" data-id="">Makawanpurgadhi</option>
                            <option value="Dewahhi Gonahi" data-id="">Dewahhi Gonahi</option>
                            <option value="Yashodhara" data-id="">Yashodhara</option>
                            <option value="Banglachuli" data-id="">Banglachuli</option>
                            <option value="Aathbiskot" data-id="">Aathbiskot</option>
                            <option value="Banfikot" data-id="">Banfikot</option>
                            <option value="Musikot" data-id="">Musikot</option>
                            <option value="Sani Bheri" data-id="">Sani Bheri</option>
                            <option value="Koshi Tappu Wildlife Reserve" data-id="">Koshi Tappu Wildlife Reserve</option>
                            <option value="Dhorpatan Hunting Reserve" data-id="">Dhorpatan Hunting Reserve</option>
                            <option value="Bardiya National Park" data-id="">Bardiya National Park</option>
                            <option value="Chitwan National Park" data-id="">Chitwan National Park</option>
                            <option value="Aaurahi" data-id="">Aaurahi</option>
                            <option value="Bideha" data-id="">Bideha</option>
                            <option value="Chhireshwornath" data-id="">Chhireshwornath</option>
                            <option value="Chautara SangachokGadhi" data-id="">Chautara SangachokGadhi</option>
                            <option value="Helambu" data-id="">Helambu</option>
                            <option value="Indrawati" data-id="">Indrawati</option>
                            <option value="Jugal" data-id="">Jugal</option>
                            <option value="Rubi Valley" data-id="">Rubi Valley</option>
                            <option value="Thakre" data-id="">Thakre</option>
                            <option value="Tripura Sundari" data-id="">Tripura Sundari</option>
                            <option value="Ajirkot" data-id="">Ajirkot</option>
                            <option value="Mudkechula" data-id="">Mudkechula</option>
                            <option value="Thuli Bheri" data-id="">Thuli Bheri</option>
                            <option value="Tripurasundari" data-id="">Tripurasundari</option>
                            <option value="Parsa Wildlife Reserve" data-id="">Parsa Wildlife Reserve</option>
                            <option value="Pathibhara Yangwarak" data-id="">Pathibhara Yangwarak</option>
                            <option value="Arun" data-id="">Arun</option>
                            <option value="Hatuwagadhi" data-id="">Hatuwagadhi</option>
                            <option value="Ramprasad Rai" data-id="">Ramprasad Rai</option>
                            <option value="Gharapjhong" data-id="">Gharapjhong</option>
                            <option value="Tatopani" data-id="">Tatopani</option>
                            <option value="Mahakali_Darchula" data-id="">Mahakali_Darchula</option>
                            <option value="Kakani" data-id="">Kakani</option>
                            <option value="Kispang" data-id="">Kispang</option>
                            <option value="Kotahimai" data-id="">Kotahimai</option>
                            <option value="Nisdi" data-id="">Nisdi</option>
                            <option value="Tinau" data-id="">Tinau</option>
                            <option value="Phalebas" data-id="">Phalebas</option>
                            <option value="Sarawal" data-id="">Sarawal</option>
                            <option value="Aurahi" data-id="">Aurahi</option>
                            <option value="Balwa" data-id="">Balwa</option>
                            <option value="Gaushala" data-id="">Gaushala</option>
                            <option value="Loharpatti" data-id="">Loharpatti</option>
                            <option value="Koshi Tappu Wildlife Reserve" data-id="">Koshi Tappu Wildlife Reserve</option>
                            <option value="Dhorpatan Hunting Reserve" data-id="">Dhorpatan Hunting Reserve</option>
                            <option value="Jhimruk" data-id="">Jhimruk</option>
                            <option value="Naubahini" data-id="">Naubahini</option>
                            <option value="Parsa Wildlife Reserve" data-id="">Parsa Wildlife Reserve</option>
                            <option value="Khaptad National Park" data-id="">Khaptad National Park</option>
                            <option value="Malangawa" data-id="">Malangawa</option>
                            <option value="Ramnagar" data-id="">Ramnagar</option>
                            <option value="Hariharpurgadhi" data-id="">Hariharpurgadhi</option>
                            <option value="Khadadevi" data-id="">Khadadevi</option>
                            <option value="Baiteshwor" data-id="">Baiteshwor</option>
                            <option value="Balefi" data-id="">Balefi</option>
                            <option value="Barhabise" data-id="">Barhabise</option>
                            <option value="Budhanilakantha" data-id="">Budhanilakantha</option>
                            <option value="Shankharapur" data-id="">Shankharapur</option>
                            <option value="Tarakeshwor" data-id="">Tarakeshwor</option>
                            <option value="Tokha" data-id="">Tokha</option>
                            <option value="Mahalaxmi" data-id="">Mahalaxmi</option>
                            <option value="Madhyapur Thimi" data-id="">Madhyapur Thimi</option>
                            <option value="Bagmati" data-id="">Bagmati</option>
                            <option value="Gadhimai" data-id="">Gadhimai</option>
                            <option value="Rajdevi" data-id="">Rajdevi</option>
                            <option value="Pakahamainpur" data-id="">Pakahamainpur</option>
                            <option value="Ichchhyakamana" data-id="">Ichchhyakamana</option>
                            <option value="Sammarimai" data-id="">Sammarimai</option>
                            <option value="Bhumekasthan" data-id="">Bhumekasthan</option>
                            <option value="Malarani" data-id="">Malarani</option>
                            <option value="Chandrakot" data-id="">Chandrakot</option>
                            <option value="Chatrakot" data-id="">Chatrakot</option>
                            <option value="Nisikhola" data-id="">Nisikhola</option>
                            <option value="Subha Kalika" data-id="">Subha Kalika</option>
                            <option value="Raskot" data-id="">Raskot</option>
                            <option value="Panchpuri" data-id="">Panchpuri</option>
                            <option value="Rapti Sonari" data-id="">Rapti Sonari</option>
                            <option value="Chaurpati" data-id="">Chaurpati</option>
                            <option value="Masta" data-id="">Masta</option>
                            <option value="Bhageshwar" data-id="">Bhageshwar</option>
                            <option value="Mahakali_Kanchanpur" data-id="">Mahakali_Kanchanpur</option>
                            <option value="Chichila" data-id="">Chichila</option>
                            <option value="Khatyad" data-id="">Khatyad</option>
                            <option value="Mugum Karmarong" data-id="">Mugum Karmarong</option>
                            <option value="Kharpunath" data-id="">Kharpunath</option>
                            <option value="Simkot" data-id="">Simkot</option>
                            <option value="Tilagufa" data-id="">Tilagufa</option>
                            <option value="Dipayal Silgadi" data-id="">Dipayal Silgadi</option>
                            <option value="Purbichauki" data-id="">Purbichauki</option>
                            <option value="Putha Uttarganga" data-id="">Putha Uttarganga</option>
                            <option value="Parsa" data-id="">Parsa</option>
                            <option value="Malikaarjun" data-id="">Malikaarjun</option>
                            <option value="Alital" data-id="">Alital</option>
                            <option value="Ganayapdhura" data-id="">Ganayapdhura</option>
                            <option value="Bulingtar" data-id="">Bulingtar</option>
                            <option value="Hupsekot" data-id="">Hupsekot</option>
                            <option value="Dhorpatan Hunting Reserve" data-id="">Dhorpatan Hunting Reserve</option>
                            <option value="Mikwakhola" data-id="">Mikwakhola</option>
                            <option value="Phungling" data-id="">Phungling</option>
                            <option value="Chhathar" data-id="">Chhathar</option>
                            <option value="Myanglung" data-id="">Myanglung</option>
                            <option value="Panchakhapan" data-id="">Panchakhapan</option>
                            <option value="Sailung" data-id="">Sailung</option>
                            <option value="Thulung Dudhkoshi" data-id="">Thulung Dudhkoshi</option>
                            <option value="Melung" data-id="">Melung</option>
                            <option value="Miklajung" data-id="">Miklajung</option>
                            <option value="Barah" data-id="">Barah</option>
                            <option value="Chhathar Jorpati" data-id="">Chhathar Jorpati</option>
                            <option value="Nechasalyan" data-id="">Nechasalyan</option>
                            <option value="Lalitpur" data-id="">Lalitpur</option>
                            <option value="Mahankal" data-id="">Mahankal</option>
                            <option value="Falgunanda" data-id="">Falgunanda</option>
                            <option value="Dhanpalthan" data-id="">Dhanpalthan</option>
                            <option value="Bhimphedi" data-id="">Bhimphedi</option>
                            <option value="Kailash" data-id="">Kailash</option>
                            <option value="Raksirang" data-id="">Raksirang</option>
                            <option value="Bharatpur" data-id="">Bharatpur</option>
                            <option value="Madi" data-id="">Madi</option>
                            <option value="Sotang" data-id="">Sotang</option>
                            <option value="Ratnanagar" data-id="">Ratnanagar</option>
                            <option value="Buddhabhumi" data-id="">Buddhabhumi</option>
                            <option value="Krishnanagar" data-id="">Krishnanagar</option>
                            <option value="Deumai" data-id="">Deumai</option>
                            <option value="Chakraghatta" data-id="">Chakraghatta</option>
                            <option value="Vyas" data-id="">Vyas</option>
                            <option value="Rhishing" data-id="">Rhishing</option>
                            <option value="Dhankaul" data-id="">Dhankaul</option>
                            <option value="Chhipaharmai" data-id="">Chhipaharmai</option>
                            <option value="Bijayanagar" data-id="">Bijayanagar</option>
                            <option value="Maharajgunj" data-id="">Maharajgunj</option>
                            <option value="Suddhodhan" data-id="">Suddhodhan</option>
                            <option value="Ainselukhark" data-id="">Ainselukhark</option>
                            <option value="Balan Bihul" data-id="">Balan Bihul</option>
                            <option value="Birgunj" data-id="">Birgunj</option>
                            <option value="Baragung Muktikhsetra" data-id="">Baragung Muktikhsetra</option>
                            <option value="Lomanthang" data-id="">Lomanthang</option>
                            <option value="Baglung" data-id="">Baglung</option>
                            <option value="Jaimini" data-id="">Jaimini</option>
                            <option value="Dangisharan" data-id="">Dangisharan</option>
                            <option value="Rajpur" data-id="">Rajpur</option>
                            <option value="Khaptad National Park" data-id="">Khaptad National Park</option>
                            <option value="Tulsipur" data-id="">Tulsipur</option>
                            <option value="Bansagadhi" data-id="">Bansagadhi</option>
                            <option value="Gulariya" data-id="">Gulariya</option>
                            <option value="Rajapur" data-id="">Rajapur</option>
                            <option value="Chure" data-id="">Chure</option>
                            <option value="Bogtan" data-id="">Bogtan</option>
                            <option value="K I Singh" data-id="">K I Singh</option>
                            <option value="Bannigadhi Jayagadh" data-id="">Bannigadhi Jayagadh</option>
                            <option value="Mangalsen" data-id="">Mangalsen</option>
                            <option value="Sanphebagar" data-id="">Sanphebagar</option>
                            <option value="Rawa Besi" data-id="">Rawa Besi</option>
                            <option value="Chisankhugadhi" data-id="">Chisankhugadhi</option>
                            <option value="Khijidemba" data-id="">Khijidemba</option>
                            <option value="Likhu" data-id="">Likhu</option>
                            <option value="Arnama" data-id="">Arnama</option>
                            <option value="Bhagawanpur" data-id="">Bhagawanpur</option>
                            <option value="Barhadashi" data-id="">Barhadashi</option>
                            <option value="Buddhashanti" data-id="">Buddhashanti</option>
                            <option value="Haldibari" data-id="">Haldibari</option>
                            <option value="Mirchaiya" data-id="">Mirchaiya</option>
                            <option value="Siraha" data-id="">Siraha</option>
                            <option value="Mahadeva" data-id="">Mahadeva</option>
                            <option value="Surunga" data-id="">Surunga</option>
                            <option value="Tilathi Koiladi" data-id="">Tilathi Koiladi</option>
                            <option value="Tirahut" data-id="">Tirahut</option>
                            <option value="Dhanauji" data-id="">Dhanauji</option>
                            <option value="Kamala" data-id="">Kamala</option>
                            <option value="Mithila Bihari" data-id="">Mithila Bihari</option>
                            <option value="Mukhiyapatti Musaharmiya" data-id="">Mukhiyapatti Musaharmiya</option>
                            <option value="Baijanath" data-id="">Baijanath</option>
                            <option value="Khajura" data-id="">Khajura</option>
                            <option value="Chulachuli" data-id="">Chulachuli</option>
                            <option value="Arjundhara" data-id="">Arjundhara</option>
                            <option value="Gauriganj" data-id="">Gauriganj</option>
                            <option value="Tapli" data-id="">Tapli</option>
                            <option value="Udayapurgadhi" data-id="">Udayapurgadhi</option>
                            <option value="Dudhouli" data-id="">Dudhouli</option>
                            <option value="Shivasataxi" data-id="">Shivasataxi</option>
                            <option value="Bhajani" data-id="">Bhajani</option>
                            <option value="Gauriganga" data-id="">Gauriganga</option>
                            <option value="Joshipur" data-id="">Joshipur</option>
                            <option value="Mohanyal" data-id="">Mohanyal</option>
                            <option value="Gramthan" data-id="">Gramthan</option>
                            <option value="Kerabari" data-id="">Kerabari</option>
                            <option value="Lisangkhu Pakhar" data-id="">Lisangkhu Pakhar</option>
                            <option value="Tripurasundari" data-id="">Tripurasundari</option>
                            <option value="Bishrampur" data-id="">Bishrampur</option>
                            <option value="Karaiyamai" data-id="">Karaiyamai</option>
                            <option value="Janaknandani" data-id="">Janaknandani</option>
                            <option value="Mithila" data-id="">Mithila</option>
                            <option value="Sabaila" data-id="">Sabaila</option>
                            <option value="Nijgadh" data-id="">Nijgadh</option>
                            <option value="Pheta" data-id="">Pheta</option>
                            <option value="Siranchok" data-id="">Siranchok</option>
                            <option value="Gulmidarbar" data-id="">Gulmidarbar</option>
                            <option value="Belauri" data-id="">Belauri</option>
                            <option value="Bhangaha" data-id="">Bhangaha</option>
                            <option value="Matihani" data-id="">Matihani</option>
                            <option value="Ramgopalpur" data-id="">Ramgopalpur</option>
                            <option value="Bhumlu" data-id="">Bhumlu</option>
                            <option value="Dhulikhel" data-id="">Dhulikhel</option>
                            <option value="Mandandeupur" data-id="">Mandandeupur</option>
                            <option value="Roshi" data-id="">Roshi</option>
                            <option value="Bagnaskali" data-id="">Bagnaskali</option>
                            <option value="Rainadevi Chhahara" data-id="">Rainadevi Chhahara</option>
                            <option value="Ribdikot" data-id="">Ribdikot</option>
                            <option value="Bhirkot" data-id="">Bhirkot</option>
                            <option value="Chapakot" data-id="">Chapakot</option>
                            <option value="Kaligandagi" data-id="">Kaligandagi</option>
                            <option value="Krishnapur" data-id="">Krishnapur</option>
                            <option value="Bhanu" data-id="">Bhanu</option>
                            <option value="Shuklagandaki" data-id="">Shuklagandaki</option>
                            <option value="Gadhawa" data-id="">Gadhawa</option>
                            <option value="Chhedagad" data-id="">Chhedagad</option>
                            <option value="Gurans" data-id="">Gurans</option>
                            <option value="Amargadhi" data-id="">Amargadhi</option>
                            <option value="Nawadurga" data-id="">Nawadurga</option>
                            <option value="Laljhadi" data-id="">Laljhadi</option>
                            <option value="BhimsenThapa" data-id="">BhimsenThapa</option>
                            <option value="Chum Nubri" data-id="">Chum Nubri</option>
                            <option value="Gorkha" data-id="">Gorkha</option>
                            <option value="Adanchuli" data-id="">Adanchuli</option>
                            <option value="Chitwan National Park" data-id="">Chitwan National Park</option>
                            <option value="Molung" data-id="">Molung</option>
                            <option value="Sunkoshi" data-id="">Sunkoshi</option>
                            <option value="Aurahi" data-id="">Aurahi</option>
                            <option value="Dhangadhimai" data-id="">Dhangadhimai</option>
                            <option value="Lahan" data-id="">Lahan</option>
                            <option value="Naraha" data-id="">Naraha</option>
                            <option value="Bishnupur" data-id="">Bishnupur</option>
                            <option value="Bode Barsain" data-id="">Bode Barsain</option>
                            <option value="Kanchanrup" data-id="">Kanchanrup</option>
                            <option value="Rajbiraj" data-id="">Rajbiraj</option>
                            <option value="Saptakoshi" data-id="">Saptakoshi</option>
                            <option value="Lakshminiya" data-id="">Lakshminiya</option>
                            <option value="Jaleswor" data-id="">Jaleswor</option>
                            <option value="Sonama" data-id="">Sonama</option>
                            <option value="Jiri" data-id="">Jiri</option>
                            <option value="Gosaikunda" data-id="">Gosaikunda</option>
                            <option value="Belkotgadhi" data-id="">Belkotgadhi</option>
                            <option value="Dupcheshwar" data-id="">Dupcheshwar</option>
                            <option value="Suryagadhi" data-id="">Suryagadhi</option>
                            <option value="Brindaban" data-id="">Brindaban</option>
                            <option value="Durga Bhagwati" data-id="">Durga Bhagwati</option>
                            <option value="Gujara" data-id="">Gujara</option>
                            <option value="Katahariya" data-id="">Katahariya</option>
                            <option value="Paroha" data-id="">Paroha</option>
                            <option value="Phatuwa Bijayapur" data-id="">Phatuwa Bijayapur</option>
                            <option value="Mahagadhimai" data-id="">Mahagadhimai</option>
                            <option value="Jagarnathpur" data-id="">Jagarnathpur</option>
                            <option value="Arjunchaupari" data-id="">Arjunchaupari</option>
                            <option value="Waling" data-id="">Waling</option>
                            <option value="Ghiring" data-id="">Ghiring</option>
                            <option value="Gandaki" data-id="">Gandaki</option>
                            <option value="Dordi" data-id="">Dordi</option>
                            <option value="Rupa" data-id="">Rupa</option>
                            <option value="Talkot" data-id="">Talkot</option>
                            <option value="Palhi Nandan" data-id="">Palhi Nandan</option>
                            <option value="Pratappur" data-id="">Pratappur</option>
                            <option value="Ramgram" data-id="">Ramgram</option>
                            <option value="Sunwal" data-id="">Sunwal</option>
                            <option value="Susta" data-id="">Susta</option>
                            <option value="Menchayam" data-id="">Menchayam</option>
                            <option value="Aamchowk" data-id="">Aamchowk</option>
                            <option value="Pauwadungma" data-id="">Pauwadungma</option>
                            <option value="Harinagara" data-id="">Harinagara</option>
                            <option value="Jantedhunga" data-id="">Jantedhunga</option>
                            <option value="Chhatradev" data-id="">Chhatradev</option>
                            <option value="Panini" data-id="">Panini</option>
                            <option value="Sandhikharka" data-id="">Sandhikharka</option>
                            <option value="Sitganga" data-id="">Sitganga</option>
                            <option value="Modi" data-id="">Modi</option>
                            <option value="Painyu" data-id="">Painyu</option>
                            <option value="Airawati" data-id="">Airawati</option>
                            <option value="Gaumukhi" data-id="">Gaumukhi</option>
                            <option value="Mandavi" data-id="">Mandavi</option>
                            <option value="Pyuthan" data-id="">Pyuthan</option>
                            <option value="Sworgadwary" data-id="">Sworgadwary</option>
                            <option value="Sukidaha" data-id="">Sukidaha</option>
                            <option value="Tribeni" data-id="">Tribeni</option>
                            <option value="Bagchaur" data-id="">Bagchaur</option>
                            <option value="Kumakh" data-id="">Kumakh</option>
                            <option value="Mellekh" data-id="">Mellekh</option>
                            <option value="Panchadewal Binayak" data-id="">Panchadewal Binayak</option>
                            <option value="Ramaroshan" data-id="">Ramaroshan</option>
                            <option value="Badimalika" data-id="">Badimalika</option>
                            <option value="Budhiganga" data-id="">Budhiganga</option>
                            <option value="Budhinanda" data-id="">Budhinanda</option>
                            <option value="Khaptad Chhededaha" data-id="">Khaptad Chhededaha</option>
                            <option value="Gaumul" data-id="">Gaumul</option>
                            <option value="Himali" data-id="">Himali</option>
                            <option value="Jagannath" data-id="">Jagannath</option>
                            <option value="Tribeni" data-id="">Tribeni</option>
                            <option value="Thalara" data-id="">Thalara</option>
                            <option value="Dasharathchanda" data-id="">Dasharathchanda</option>
                            <option value="Dilasaini" data-id="">Dilasaini</option>
                            <option value="Dogadakedar" data-id="">Dogadakedar</option>
                            <option value="Melauli" data-id="">Melauli</option>
                            <option value="Pancheshwar" data-id="">Pancheshwar</option>
                            <option value="Patan" data-id="">Patan</option>
                            <option value="Chaurjahari" data-id="">Chaurjahari</option>
                            <option value="Gaur" data-id="">Gaur</option>
                            <option value="Bithadchir" data-id="">Bithadchir</option>
                            <option value="Bungal" data-id="">Bungal</option>
                            <option value="Chabispathivera" data-id="">Chabispathivera</option>
                            <option value="Durgathali" data-id="">Durgathali</option>
                            <option value="JayaPrithivi" data-id="">JayaPrithivi</option>
                            <option value="Saipal" data-id="">Saipal</option>
                            <option value="Kedarseu" data-id="">Kedarseu</option>
                            <option value="Khaptadchhanna" data-id="">Khaptadchhanna</option>
                            <option value="Surma" data-id="">Surma</option>
                            <option value="Chaubise" data-id="">Chaubise</option>
                            <option value="Dhankuta" data-id="">Dhankuta</option>
                            <option value="Shahidbhumi" data-id="">Shahidbhumi</option>
                            <option value="Mahalaxmi" data-id="">Mahalaxmi</option>
                            <option value="Pakhribas" data-id="">Pakhribas</option>
                            <option value="Barahapokhari" data-id="">Barahapokhari</option>
                            <option value="Diprung Chuichumma" data-id="">Diprung Chuichumma</option>
                            <option value="Halesi Tuwachung" data-id="">Halesi Tuwachung</option>
                            <option value="Kepilasagadhi" data-id="">Kepilasagadhi</option>
                            <option value="Khotehang" data-id="">Khotehang</option>
                            <option value="Diktel Rupakot Majhuwagadhi" data-id="">Diktel Rupakot Majhuwagadhi</option>
                            <option value="Sakela" data-id="">Sakela</option>
                            <option value="Bhimeshwor" data-id="">Bhimeshwor</option>
                            <option value="Taman Khola" data-id="">Taman Khola</option>
                            <option value="Rapti" data-id="">Rapti</option>
                            <option value="Siddha Kumakh" data-id="">Siddha Kumakh</option>
                            <option value="Sarkegad" data-id="">Sarkegad</option>
                            <option value="Tanjakot" data-id="">Tanjakot</option>
                            <option value="Janki" data-id="">Janki</option>
                            <option value="Narainapur" data-id="">Narainapur</option>
                            <option value="Marma" data-id="">Marma</option>
                            <option value="Naugad" data-id="">Naugad</option>
                            <option value="Khaptad National Park" data-id="">Khaptad National Park</option>
                            <option value="Shailyashikhar" data-id="">Shailyashikhar</option>
                            <option value="Purchaudi" data-id="">Purchaudi</option>
                            <option value="Chaudandigadhi" data-id="">Chaudandigadhi</option>
                            <option value="Limchunbung" data-id="">Limchunbung</option>
                            <option value="Geruwa" data-id="">Geruwa</option>
                            <option value="Thakurbaba" data-id="">Thakurbaba</option>
                            <option value="Swami Kartik" data-id="">Swami Kartik</option>
                            <option value="Apihimal" data-id="">Apihimal</option>
                            <option value="Byas" data-id="">Byas</option>
                            <option value="Dunhu" data-id="">Dunhu</option>
                            <option value="Lekam" data-id="">Lekam</option>
                            <option value="Shivanath" data-id="">Shivanath</option>
                            <option value="Sigas" data-id="">Sigas</option>
                            <option value="Surnaya" data-id="">Surnaya</option>
                            <option value="Bateshwor" data-id="">Bateshwor</option>
                            <option value="Tamakoshi" data-id="">Tamakoshi</option>
                            <option value="Kalika" data-id="">Kalika</option>
                            <option value="Aamachhodingmo" data-id="">Aamachhodingmo</option>
                            <option value="Thori" data-id="">Thori</option>
                            <option value="Chandannath" data-id="">Chandannath</option>
                            <option value="Guthichaur" data-id="">Guthichaur</option>
                            <option value="Kanakasundari" data-id="">Kanakasundari</option>
                            <option value="Patrasi" data-id="">Patrasi</option>
                            <option value="Sinja" data-id="">Sinja</option>
                            <option value="Tila" data-id="">Tila</option>
                            <option value="Khandachakra" data-id="">Khandachakra</option>
                            <option value="Barju" data-id="">Barju</option>
                            <option value="Sangurigadhi" data-id="">Sangurigadhi</option>
                            <option value="Laxmipur Patari" data-id="">Laxmipur Patari</option>
                            <option value="Sakhuwanankarkatti" data-id="">Sakhuwanankarkatti</option>
                            <option value="Uttargaya" data-id="">Uttargaya</option>
                            <option value="Hetauda" data-id="">Hetauda</option>
                            <option value="Champadevi" data-id="">Champadevi</option>
                            <option value="Ganeshman Charnath" data-id="">Ganeshman Charnath</option>
                            <option value="Kamalamai" data-id="">Kamalamai</option>
                            <option value="Tinpatan" data-id="">Tinpatan</option>
                            <option value="Doramba" data-id="">Doramba</option>
                            <option value="Panchpokhari Thangpal" data-id="">Panchpokhari Thangpal</option>
                            <option value="Nagarjun" data-id="">Nagarjun</option>
                            <option value="Bethanchowk" data-id="">Bethanchowk</option>
                            <option value="Mahabharat" data-id="">Mahabharat</option>
                            <option value="Tansen" data-id="">Tansen</option>
                            <option value="Malika" data-id="">Malika</option>
                            <option value="Resunga" data-id="">Resunga</option>
                            <option value="Besishahar" data-id="">Besishahar</option>
                            <option value="Dudhpokhari" data-id="">Dudhpokhari</option>
                            <option value="Kwholasothar" data-id="">Kwholasothar</option>
                            <option value="MadhyaNepal" data-id="">MadhyaNepal</option>
                            <option value="Rainas" data-id="">Rainas</option>
                            <option value="Madi" data-id="">Madi</option>
                            <option value="Barahtal" data-id="">Barahtal</option>
                            <option value="Godawari_Kailali" data-id="">Godawari_Kailali</option>
                            <option value="Lamkichuha" data-id="">Lamkichuha</option>
                            <option value="Bhimdatta" data-id="">Bhimdatta</option>
                            <option value="Binayee Tribeni" data-id="">Binayee Tribeni</option>
                            <option value="Sundarbazar" data-id="">Sundarbazar</option>
                            <option value="Chame" data-id="">Chame</option>
                            <option value="Nashong" data-id="">Nashong</option>
                            <option value="Manang Ngisyang" data-id="">Manang Ngisyang</option>
                            <option value="Beni" data-id="">Beni</option>
                            <option value="Dhaulagiri" data-id="">Dhaulagiri</option>
                            <option value="Malika" data-id="">Malika</option>
                            <option value="Mangala" data-id="">Mangala</option>
                            <option value="Bihadi" data-id="">Bihadi</option>
                            <option value="Jaljala" data-id="">Jaljala</option>
                            <option value="Kushma" data-id="">Kushma</option>
                            <option value="Barekot" data-id="">Barekot</option>
                            <option value="Bheri" data-id="">Bheri</option>
                            <option value="Junichande" data-id="">Junichande</option>
                            <option value="Kuse" data-id="">Kuse</option>
                            <option value="Madi" data-id="">Madi</option>
                            <option value="Naukunda" data-id="">Naukunda</option>
                            <option value="Panchakanya" data-id="">Panchakanya</option>
                            <option value="Tarkeshwar" data-id="">Tarkeshwar</option>
                            <option value="Galchi" data-id="">Galchi</option>
                            <option value="Khaniyabash" data-id="">Khaniyabash</option>
                            <option value="Baragadhi" data-id="">Baragadhi</option>
                            <option value="Jitpur Simara" data-id="">Jitpur Simara</option>
                            <option value="Pacharauta" data-id="">Pacharauta</option>
                            <option value="Suwarna" data-id="">Suwarna</option>
                            <option value="Mallarani" data-id="">Mallarani</option>
                            <option value="Sarumarani" data-id="">Sarumarani</option>
                            <option value="Mahawai" data-id="">Mahawai</option>
                            <option value="Naraharinath" data-id="">Naraharinath</option>
                            <option value="Pachaljharana" data-id="">Pachaljharana</option>
                            <option value="Palata" data-id="">Palata</option>
                            <option value="Nalgaad" data-id="">Nalgaad</option>
                            <option value="Chaukune" data-id="">Chaukune</option>
                            <option value="Lekbeshi" data-id="">Lekbeshi</option>
                            <option value="Simta" data-id="">Simta</option>
                            <option value="Badhaiyatal" data-id="">Badhaiyatal</option>
                            <option value="Chitwan National Park" data-id="">Chitwan National Park</option>
                            <option value="Lumbini Sanskritik Development Area" data-id="">Lumbini Sanskritik Development Area</option>
                            <option value="Bindabasini" data-id="">Bindabasini</option>
                            <option value="Bangad Kupinde" data-id="">Bangad Kupinde</option>
                            <option value="Kalimati" data-id="">Kalimati</option>
                            <option value="Sharada" data-id="">Sharada</option>
                            <option value="Sanni Tribeni" data-id="">Sanni Tribeni</option>
                            <option value="Aathabis" data-id="">Aathabis</option>
                            <option value="Chamunda Bindrasaini" data-id="">Chamunda Bindrasaini</option>
                            <option value="Mahabu" data-id="">Mahabu</option>
                            <option value="Naumule" data-id="">Naumule</option>
                            <option value="Nepalgunj" data-id="">Nepalgunj</option>
                            <option value="Aadarsha" data-id="">Aadarsha</option>
                            <option value="Kawasoti" data-id="">Kawasoti</option>
                            <option value="Sidingba" data-id="">Sidingba</option>
                            <option value="Maijogmai" data-id="">Maijogmai</option>
                            <option value="Bhotkhola" data-id="">Bhotkhola</option>
                            <option value="Salpasilichho" data-id="">Salpasilichho</option>
                            <option value="Shadananda" data-id="">Shadananda</option>
                            <option value="Bhadrapur" data-id="">Bhadrapur</option>
                            <option value="Damak" data-id="">Damak</option>
                            <option value="Jhapa" data-id="">Jhapa</option>
                            <option value="Kankai" data-id="">Kankai</option>
                            <option value="Mechinagar" data-id="">Mechinagar</option>
                            <option value="Budhiganga" data-id="">Budhiganga</option>
                            <option value="Jahada" data-id="">Jahada</option>
                            <option value="Kanepokhari" data-id="">Kanepokhari</option>
                            <option value="Letang" data-id="">Letang</option>
                            <option value="Rangeli" data-id="">Rangeli</option>
                            <option value="Ratuwamai" data-id="">Ratuwamai</option>
                            <option value="Uralabari" data-id="">Uralabari</option>
                            <option value="Dhanusadham" data-id="">Dhanusadham</option>
                            <option value="Hansapur" data-id="">Hansapur</option>
                            <option value="Janakpur" data-id="">Janakpur</option>
                            <option value="Nagarain" data-id="">Nagarain</option>
                            <option value="Sahidnagar" data-id="">Sahidnagar</option>
                            <option value="Melamchi" data-id="">Melamchi</option>
                            <option value="Sunkoshi" data-id="">Sunkoshi</option>
                            <option value="Adarshkotwal" data-id="">Adarshkotwal</option>
                            <option value="Devtal" data-id="">Devtal</option>
                            <option value="Kalaiya" data-id="">Kalaiya</option>
                            <option value="Kolhabi" data-id="">Kolhabi</option>
                            <option value="Parwanipur" data-id="">Parwanipur</option>
                            <option value="Simraungadh" data-id="">Simraungadh</option>
                            <option value="Dharche" data-id="">Dharche</option>
                            <option value="Palungtar" data-id="">Palungtar</option>
                            <option value="Gangadev" data-id="">Gangadev</option>
                            <option value="Chankheli" data-id="">Chankheli</option>
                            <option value="Namkha" data-id="">Namkha</option>
                            <option value="Bardagoriya" data-id="">Bardagoriya</option>
                            <option value="Dhangadhi" data-id="">Dhangadhi</option>
                            <option value="Ghodaghodi" data-id="">Ghodaghodi</option>
                            <option value="Janaki" data-id="">Janaki</option>
                            <option value="Kailari" data-id="">Kailari</option>
                            <option value="Tikapur" data-id="">Tikapur</option>
                            <option value="Falelung" data-id="">Falelung</option>
                            <option value="Ekdara" data-id="">Ekdara</option>
                            <option value="Mahottari" data-id="">Mahottari</option>
                            <option value="Pipra" data-id="">Pipra</option>
                            <option value="Samsi" data-id="">Samsi</option>
                            <option value="Haripur" data-id="">Haripur</option>
                            <option value="Banepa" data-id="">Banepa</option>
                            <option value="Chaurideurali" data-id="">Chaurideurali</option>
                            <option value="Khanikhola" data-id="">Khanikhola</option>
                            <option value="Namobuddha" data-id="">Namobuddha</option>
                            <option value="Panauti" data-id="">Panauti</option>
                            <option value="Temal" data-id="">Temal</option>
                            <option value="Mathagadhi" data-id="">Mathagadhi</option>
                            <option value="Purbakhola" data-id="">Purbakhola</option>
                            <option value="Rambha" data-id="">Rambha</option>
                            <option value="Isma" data-id="">Isma</option>
                            <option value="Aandhikhola" data-id="">Aandhikhola</option>
                            <option value="Biruwa" data-id="">Biruwa</option>
                            <option value="Galyang" data-id="">Galyang</option>
                            <option value="Phedikhola" data-id="">Phedikhola</option>
                            <option value="Putalibazar" data-id="">Putalibazar</option>
                            <option value="Bhimad" data-id="">Bhimad</option>
                            <option value="Jagadulla" data-id="">Jagadulla</option>
                            <option value="Shiwalaya" data-id="">Shiwalaya</option>
                            <option value="Bhairabi" data-id="">Bhairabi</option>
                            <option value="Thantikandh" data-id="">Thantikandh</option>
                            <option value="Bheriganga" data-id="">Bheriganga</option>
                            <option value="Dhakari" data-id="">Dhakari</option>
                            <option value="Kamalbazar" data-id="">Kamalbazar</option>
                            <option value="Ajaymeru" data-id="">Ajaymeru</option>
                            <option value="Parashuram" data-id="">Parashuram</option>
                            <option value="Bedkot" data-id="">Bedkot</option>
                            <option value="Beldandi" data-id="">Beldandi</option>
                            <option value="Punarbas" data-id="">Punarbas</option>
                            <option value="Shuklaphanta" data-id="">Shuklaphanta</option>
                            <option value="Shuklaphanta National Park" data-id="">Shuklaphanta National Park</option>
                            <option value="Hilihang" data-id="">Hilihang</option>
                            <option value="Bariyarpatti" data-id="">Bariyarpatti</option>
                            <option value="Bishnupur" data-id="">Bishnupur</option>
                            <option value="Golbazar" data-id="">Golbazar</option>
                            <option value="Kalyanpur" data-id="">Kalyanpur</option>
                            <option value="Rajgadh" data-id="">Rajgadh</option>
                            <option value="Chhinnamasta" data-id="">Chhinnamasta</option>
                            <option value="Dakneshwori" data-id="">Dakneshwori</option>
                            <option value="Hanumannagar Kankalini" data-id="">Hanumannagar Kankalini</option>
                            <option value="Khadak" data-id="">Khadak</option>
                            <option value="Rupani" data-id="">Rupani</option>
                            <option value="Shambhunath" data-id="">Shambhunath</option>
                            <option value="Bidur" data-id="">Bidur</option>
                            <option value="Prasauni" data-id="">Prasauni</option>
                            <option value="Khairahani" data-id="">Khairahani</option>
                            <option value="Madane" data-id="">Madane</option>
                            <option value="Musikot" data-id="">Musikot</option>
                            <option value="Satyawati" data-id="">Satyawati</option>
                            <option value="Devghat" data-id="">Devghat</option>
                            <option value="Aarughat" data-id="">Aarughat</option>
                            <option value="Sahid Lakhan" data-id="">Sahid Lakhan</option>
                            <option value="Marsyangdi" data-id="">Marsyangdi</option>
                            <option value="Narpa Bhoomi" data-id="">Narpa Bhoomi</option>
                            <option value="Annapurna" data-id="">Annapurna</option>
                            <option value="Raghuganga" data-id="">Raghuganga</option>
                            <option value="Shey Phoksundo" data-id="">Shey Phoksundo</option>
                            <option value="Birendranagar" data-id="">Birendranagar</option>
                            <option value="Chingad" data-id="">Chingad</option>
                            <option value="Turmakhad" data-id="">Turmakhad</option>
                            <option value="Baudikali" data-id="">Baudikali</option>
                            <option value="Devchuli" data-id="">Devchuli</option>
                            <option value="Gaidakot" data-id="">Gaidakot</option>
                            <option value="Madhyabindu" data-id="">Madhyabindu</option>
                            <option value="Khaptad National Park" data-id="">Khaptad National Park</option>
                            <option value="Koshi Tappu Wildlife Reserve" data-id="">Koshi Tappu Wildlife Reserve</option>
                            <option value="Phidim" data-id="">Phidim</option>
                            <option value="Sandakpur" data-id="">Sandakpur</option>
                            <option value="Gauradhaha" data-id="">Gauradhaha</option>
                            <option value="Kachankawal" data-id="">Kachankawal</option>
                            <option value="Biratnagar" data-id="">Biratnagar</option>
                            <option value="Patahrishanishchare" data-id="">Patahrishanishchare</option>
                            <option value="Sundarharaicha" data-id="">Sundarharaicha</option>
                            <option value="Dharan" data-id="">Dharan</option>
                            <option value="Mahakulung" data-id="">Mahakulung</option>
                            <option value="Chandragiri" data-id="">Chandragiri</option>
                            <option value="Dakshinkali" data-id="">Dakshinkali</option>
                            <option value="Gokarneshwor" data-id="">Gokarneshwor</option>
                            <option value="Kageshwori Manahora" data-id="">Kageshwori Manahora</option>
                            <option value="Kathmandu" data-id="">Kathmandu</option>
                            <option value="Kirtipur" data-id="">Kirtipur</option>
                            <option value="Dhurkot" data-id="">Dhurkot</option>
                            <option value="Kaligandaki" data-id="">Kaligandaki</option>
                            <option value="Ruru" data-id="">Ruru</option>
                            <option value="Annapurna" data-id="">Annapurna</option>
                            <option value="Bareng" data-id="">Bareng</option>
                            <option value="Kanthekhola" data-id="">Kanthekhola</option>
                            <option value="Tara Khola" data-id="">Tara Khola</option>
                            <option value="Kummayak" data-id="">Kummayak</option>
                            <option value="Miklajung" data-id="">Miklajung</option>
                            <option value="Yangwarak" data-id="">Yangwarak</option>
                            <option value="Manebhanjyang" data-id="">Manebhanjyang</option>
                            <option value="Siddhicharan" data-id="">Siddhicharan</option>
                            <option value="Nawarajpur" data-id="">Nawarajpur</option>
                            <option value="Sukhipur" data-id="">Sukhipur</option>
                            <option value="Agnisair Krishna Savaran" data-id="">Agnisair Krishna Savaran</option>
                            <option value="Bardibas" data-id="">Bardibas</option>
                            <option value="Manra Siswa" data-id="">Manra Siswa</option>
                            <option value="Balara" data-id="">Balara</option>
                            <option value="Bigu" data-id="">Bigu</option>
                            <option value="Gaurishankar" data-id="">Gaurishankar</option>
                            <option value="Kalinchok" data-id="">Kalinchok</option>
                            <option value="Likhu" data-id="">Likhu</option>
                            <option value="Meghang" data-id="">Meghang</option>
                            <option value="Shivapuri" data-id="">Shivapuri</option>
                            <option value="Tadi" data-id="">Tadi</option>
                            <option value="Baudhimai" data-id="">Baudhimai</option>
                            <option value="Chandrapur" data-id="">Chandrapur</option>
                            <option value="Garuda" data-id="">Garuda</option>
                            <option value="Ishanath" data-id="">Ishanath</option>
                            <option value="Madhav Narayan" data-id="">Madhav Narayan</option>
                            <option value="Maulapur" data-id="">Maulapur</option>
                            <option value="Devdaha" data-id="">Devdaha</option>
                            <option value="Omsatiya" data-id="">Omsatiya</option>
                            <option value="Rampur" data-id="">Rampur</option>
                            <option value="Harinas" data-id="">Harinas</option>
                            <option value="Mahashila" data-id="">Mahashila</option>
                            <option value="Chharka Tangsong" data-id="">Chharka Tangsong</option>
                            <option value="Hima" data-id="">Hima</option>
                            <option value="Gurbhakot" data-id="">Gurbhakot</option>
                            <option value="Bardaghat" data-id="">Bardaghat</option>
                            <option value="Shivapuri Watershed and Wildlife Reserve" data-id="">Shivapuri Watershed and Wildlife Reserve</option>
                            <option value="Langtang National Park" data-id="">Langtang National Park</option>
                            <option value="Rajpur" data-id="">Rajpur</option>
                            <option value="Yemunamai" data-id="">Yemunamai</option>
                            <option value="Butwal" data-id="">Butwal</option>
                            <option value="Gaidahawa" data-id="">Gaidahawa</option>
                            <option value="Kanchan" data-id="">Kanchan</option>
                            <option value="Lumbini Sanskritik" data-id="">Lumbini Sanskritik</option>
                            <option value="Marchawari" data-id="">Marchawari</option>
                            <option value="Mayadevi" data-id="">Mayadevi</option>
                            <option value="Machhapuchchhre" data-id="">Machhapuchchhre</option>
                            <option value="Madi" data-id="">Madi</option>
                            <option value="Pokhara" data-id="">Pokhara</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>Permanent Address</label>
                        <input type="text" name="permanent_address">
                    </div>
                </div>
            </div>

            <div style="clear:both"></div>


            <!-- end personal info -->

            <hr>
            <h3>
                Father's Detail
            </h3>
            <div>
                <div>
                    <div>
                        <label>Father's Name</label>
                        <input type="text" name="father_name">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Father's Image</label>
                        <input type="file"  name="father_image" data-resize="300" accept=".jpg,.png,.jpeg">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Father's Occupation</label>
                        <input type="text" name="f_occupation">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Father's Cell No.</label>
                        <input type="text" name="f_cell">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Father's Email</label>
                        <input type="text" name="f_email">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Father's Office</label>
                        <input type="text" name="f_office">
                    </div>
                </div>
            </div>
            <hr>
            <h3>
                Mother's Detail
            </h3>
            <div>
                <div>
                    <div>
                        <label>Mother's Name</label>
                        <input type="text" name="mother_name">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Mother's Image</label>
                        <input type="file"  data-resize="300" name="mother_image" accept=".jpg,.png,.jpeg">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Mother's Occupation</label>
                        <input type="text" name="m_occupation">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Mother's Cell No.</label>
                        <input type="text" name="m_cell">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Mother's Email</label>
                        <input type="text" name="m_email">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Mother's Office</label>
                        <input type="text" name="m_office">
                    </div>
                </div>
            </div>
            <hr>
            <h3>
                Local Guardian's Detail
            </h3>
            <div>
                <div>
                    <div>
                        <label>Local Guardian's Name</label>
                        <input type="text" name="guardian_name">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Relation</label>
                        <input type="text" name="g_relation">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Local Guardian's Image</label>
                        <input type="file"  data-resize="300" name="guardian_image" accept=".jpg,.png,.jpeg">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Local Guardian's Cell No.</label>
                        <input type="text" name="g_cell">
                    </div>
                </div>
            </div>
            <hr>
            <h3>
                Other Detail
            </h3>
            <div>
                <div>
                    <div>
                        <label>Religion</label>
                        <select name="religion" id="religion">
                            <option value="">Select Religion</option>
                            <option value="Buddhism">Buddhism</option>
                            <option value="Christianity">Christianity</option>
                            <option value="Hinduism">Hinduism</option>
                            <option value="Islam">Islam</option>
                            <option value="Kirat">Kirat</option>
                            <option value="Christianity">Christianity</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>Ethnicity</label>
                        <select name="cast" id="cast">
                            <option value="">Select Cast</option>
                            <option value="Brahmin">Brahmin</option>
                            <option value="Chhetri">Chhetri</option>
                            <option value="Newar">Newar</option>
                            <option value="Tamang">Tamang</option>
                            <option value="Magar">Magar</option>
                            <option value="Sherpa">Sherpa</option>
                            <option value="Rai">Rai</option>
                            <option value="Gurung">Gurung</option>
                            <option value="Limbu">Limbu</option>
                            <option value="Tharu">Tharu</option>
                            <option value="Chepang">Chepang</option>
                            <option value="Bhote">Bhote</option>
                            <option value="Sunuwar">Sunuwar</option>
                            <option value="Kami">Kami</option>
                            <option value="Damai">Damai</option>
                            <option value="Sarki">Sarki</option>
                            <option value="Thakuri">Thakuri</option>
                            <option value="Bhujel">Bhujel</option>
                            <option value="Danuwar">Danuwar</option>
                            <option value="Dhanuk">Dhanuk</option>
                            <option value="Gaine (Gandarbha)">Gaine (Gandarbha)</option>
                            <option value="Ghale">Ghale</option>
                            <option value="Hayu">Hayu</option>
                            <option value="Jhangad">Jhangad</option>
                            <option value="Kumal">Kumal</option>
                            <option value="Lepcha">Lepcha</option>
                            <option value="Majhi">Majhi</option>
                            <option value="Meche">Meche</option>
                            <option value="Pahari">Pahari</option>
                            <option value="Rajbanshi">Rajbanshi</option>
                            <option value="Satar (Santhal)">Satar (Santhal)</option>
                            <option value="Thakali">Thakali</option>
                            <option value="Yakkha">Yakkha</option>
                        </select>
                    </div>
                </div>
                <div>
                    <div>
                        <label>Citizenship</label>
                        <input type="text" name="citizenship"  value="Nepali">
                    </div>
                </div>
                <div>
                    <div>
                        <label>Differently Abled</label>
                        <select name="handicapped" id="">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div  style="clear: both">
                <button type="submit" class="btn btn-green">Submit</button>
            </div>
        </form>
    </div>
</div>