<div class="container">
    <?php
        session_start();
        if (isset($_SESSION["errorMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%;" class="error-info"><?= $_SESSION["errorMessage"] ?></div>
    <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%; color: green;" class="success-info"><?= $_SESSION["successMessage"] ?></div>
    <?php
            unset($_SESSION["successMessage"]);
        }
    ?>
    <h1>Student Management</h1>
    <div class="table-responsive">
        <table class="student-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Profile</th>
                    <th>Name</th>
                    <th>Name in Nepali</th>
                    <th>Faculty</th>
                    <th>Batch</th>
                    <th>Class</th>
                    <th>Section</th>
                    <th>Gender</th>
                    <th>Date of Birth (BS)</th>
                    <th>Phone</th>
                    <th>Nationality</th>
                    <th>Permanent Province</th>
                    <th>Permanent District</th>
                    <th>Permanent Municipality</th>
                    <th>Permanent Address</th>
                    <th>Father's Name</th>
                    <th>Mother's Name</th>
                    <th>Religion</th>
                    <th>Cast</th>
                    <th>Citizenship</th>
                    <th>Handicapped</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($data && !empty($data)): ?>
                    <?php foreach ($data as $student):?>
                        <tr>
                            <td><?= $student['student_id'] ?></td>
                            <td>
                                <div class="profile-image-container">
                                    <?php if ($student['student_image']): ?>
                                        <img src="<?= $student['student_image'] ?>" alt="<?= $student['name'] ?>" class="profile-image">
                                    <?php else: ?>
                                        <div class="default-profile-image"></div>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td><?= $student['student_name'] ?></td>
                            <td><?= $student['name_in_nepali'] ?></td>
                            <td><?= $student['faculty_name'] ?></td>
                            <td><?= $student['batch_name'] ?></td>
                            <td><?= $student['class_name'] ?></td>
                            <td><?= $student['section_name'] ?></td>
                            <td><?= $student['gender'] ?></td>
                            <td><?= $student['dob_bs'] ?></td>
                            <td><?= $student['phone'] ?></td>
                            <td><?= $student['nationality'] ?></td>
                            <td><?= $student['permanent_province'] ?></td>
                            <td><?= $student['permanent_district'] ?></td>
                            <td><?= $student['permanent_municipality'] ?></td>
                            <td><?= $student['permanent_address'] ?></td>
                            <td><?= $student['father_name'] ?></td>
                            <td><?= $student['mother_name'] ?></td>
                            <td><?= $student['religion'] ?></td>
                            <td><?= $student['cast'] ?></td>
                            <td><?= $student['citizenship'] ?></td>
                            <td><?= $student['handicapped'] == '1' ? 'Yes' : 'No' ?></td>
                            <td>
                                <form action="Controller/StudentController.php?page=deleteStudent" method="POST">
                                    <input type="hidden" name="id" value="<?= $student['id'] ?>">
                                    <button type="submit" class="delete-btn">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="18" style="text-align: center; padding: 20px; color: #aaa;">No data found.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<style>
<style>
.container {
    max-width: 100%;
    overflow-x: auto;
}

h1 {
    padding: 20px;
    text-align: center;
}

.table-responsive {
    overflow-x: auto;
}

.student-table {
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
}

.student-table th, .student-table td {
    border: 1px solid #ddd;
    padding: 12px;
    text-align: left;
}

.student-table th {
    background-color: #f2f2f2;
    font-weight: bold;
}

.student-table td {
    vertical-align: middle;
}

.student-table td .profile-image-container {
    width: 40px;
    height: 40px;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
}

.student-table td .profile-image-container img {
    max-width: 100%;
    max-height: 100%;
    object-fit: fill;
}

.student-table td .default-profile-image {
    width: 100%;
    height: 100%;
    background-color: #ccc;
}

.student-table .delete-btn {
    padding: 8px 16px;
    background-color: #dc3545;
    color: white;
    border: none;
    cursor: pointer;
    border-radius: 4px;
    font-size: 14px;
    transition: background-color 0.3s;
}

.student-table .delete-btn:hover {
    background-color: #c82333;
}
</style>
