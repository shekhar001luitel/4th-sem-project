<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav">
                <button class="btn-green">
                    <li><a href="/setup.php">Program</a></li>
                </button>
            </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <?php
        session_start();
        if (isset($_SESSION["errorMessage"])) {
            echo '<div style="font-size: 30px; text-align: center; padding:1%" class="error-info">' . $_SESSION["errorMessage"] . '</div>';
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
            echo '<div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green;">' . $_SESSION["successMessage"] . '</div>';
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Teacher</h2>
                </div>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Assign Class</th>
                    </tr>
                    <?php
                    foreach ($teachers as $key) {
                        echo '<tr>';
                        echo '<td>' . $key['name'] . '</td>';
                        echo '<td><a href="#" class="btn-green assign-class-btn" data-teacher-id="' . $key['id'] . '" data-teacher-name="' . $key['name'] . '">Add</a></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2 id="teacherNameSpan">Teacher's Assign Class</h2>
                </div>
                <h3 style="text-align: center; margin-top: 15%;" id="noneAssignClassH3">Select the Teacher </h3>

                <div id="viewAssignClass" style="display:none; padding: 15px">
                    <form method="post" action="Controller/StaffAttendanceController.php?page=assignClass">
                        <input type="hidden" id="staff_id" name="staff_id" value="">
                        <div>
                            <label>Program</label>
                            <select id="program" name="faculty">
                                <option value="">Select Program</option>
                                <?php foreach ($data as $faculty => $batches) { ?>
                                    <option value="<?= $batches['faculty_id'] ?>"><?= $faculty ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div>
                            <label>Batch</label>
                            <select id="batchSelect" name="batch">
                                <option value="" selected>Select Batch</option>
                            </select>
                        </div>
                        <div>
                            <label>Class *</label>
                            <select id="mark" name="class">
                                <option value="" selected>Select Class</option>
                            </select>
                        </div>
                        <div>
                            <label>Section *</label>
                            <select id="series" name="section">
                                <option value="" selected>Select Section</option>
                            </select>
                        </div>
                        <div style="padding-top: 15px">
                            <button type="submit" class="btn-green">Assign</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="content-2 create-batch">
            <div class="recent-payments">
                <div class="title">
                    <h2>Assigned Class</h2>
                </div>
                <div id="assignedClassList">
                    <h3 style="text-align: center; margin-top: 15%;">No Class Selected</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var programSelect = document.getElementById('program');
        var batchSelect = document.getElementById('batchSelect');
        var classSelect = document.getElementById('mark');
        var sectionSelect = document.getElementById('series');
        var viewAssignClass = document.getElementById('viewAssignClass');
        var noneAssignClassH3 = document.getElementById('noneAssignClassH3');
        var staffIdInput = document.getElementById('staff_id');
        var teacherNameSpan = document.getElementById('teacherNameSpan');
        var assignClassBtns = document.querySelectorAll('.assign-class-btn');
        var assignedClassList = document.getElementById('assignedClassList');
        var facultyData = <?= json_encode($data) ?>;

        assignClassBtns.forEach(function(btn) {
            btn.addEventListener('click', function(event) {
                event.preventDefault();
                var teacherId = btn.getAttribute('data-teacher-id');
                var teacherName = btn.getAttribute('data-teacher-name');
                staffIdInput.value = teacherId;
                teacherNameSpan.textContent = teacherName;
                viewAssignClass.style.display = 'block';
                noneAssignClassH3.style.display = 'none';

                // Fetch assigned classes

                    fetch('Controller/StaffAttendanceController.php?page=assignedClassList', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'staff_id=' + encodeURIComponent(teacherId),
                        // body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("#assignedClassList").innerHTML = data
                        // document.querySelector("div .create-batch").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));


            });
        });

        programSelect.addEventListener('change', function() {
            var selectedFacultyId = this.value;
            var selectedProgram = programSelect.options[programSelect.selectedIndex].text;
            batchSelect.innerHTML = '<option value="" selected>Select Batch</option>';
            classSelect.innerHTML = '<option value="" selected>Select Class</option>';
            sectionSelect.innerHTML = '<option value="" selected>Select Section</option>';

            if (selectedProgram !== '') {
                var batches = facultyData[selectedProgram];
                if (batches) {
                    for (var batchId in batches) {
                        if (batchId !== 'faculty_id') {
                            var batchName = batches[batchId]['batch_name'];
                            batchSelect.innerHTML += '<option value="' + batchId + '">' + batchName + '</option>';
                        }
                    }
                }
            }
        });

        batchSelect.addEventListener('change', function() {
            var selectedBatchId = this.value;
            var selectedBatch = batchSelect.options[batchSelect.selectedIndex].text;
            classSelect.innerHTML = '<option value="" selected>Select Class</option>';
            sectionSelect.innerHTML = '<option value="" selected>Select Section</option>';

            if (selectedBatch !== '') {
                var batches = facultyData[programSelect.options[programSelect.selectedIndex].text];
                if (batches && batches[selectedBatchId]) {
                    var classes = batches[selectedBatchId]['classes'];
                    for (var classId in classes) {
                        var className = classes[classId]['class_name'];
                        classSelect.innerHTML += '<option value="' + classId + '">' + className + '</option>';
                    }
                }
            }
        });

        classSelect.addEventListener('change', function() {
            var selectedClassId = this.value;
            sectionSelect.innerHTML = '<option value="" selected>Select Section</option>';

            if (selectedClassId !== '') {
                var batches = facultyData[programSelect.options[programSelect.selectedIndex].text];
                if (batches) {
                    for (var batchId in batches) {
                        if (batchId !== 'faculty_id') {
                            var classes = batches[batchId]['classes'];
                            for (var classId in classes) {
                                if (classId === selectedClassId) {
                                    var sections = classes[classId]['sections'];
                                    for (var i = 0; i < sections.length; i++) {
                                        var sectionId = sections[i]['section_id'];
                                        var sectionName = sections[i]['section_name'];
                                        sectionSelect.innerHTML += '<option value="' + sectionId + '">' + sectionName + '</option>';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    });

</script>
