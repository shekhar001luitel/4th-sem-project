<div class="container">
<?php
        session_start();
        if (isset($_SESSION["errorMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%;" class="error-info"><?= $_SESSION["errorMessage"] ?></div>
    <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%; color: green;" class="success-info"><?= $_SESSION["successMessage"] ?></div>
    <?php
            unset($_SESSION["successMessage"]);
        }
    ?>
    <h1>Teacher Management</h1>
    <div class="table-responsive">
        <table class="staff-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Profile</th>
                    <th>Name</th>
                    <th>Name in Nepali</th>
                    <th>Gender</th>
                    <th>Date of Birth (BS)</th>
                    <th>Phone</th>
                    <th>Nationality</th>
                    <th>Address</th>
                    <th>Father's Name</th>
                    <th>Mother's Name</th>
                    <th>Religion</th>
                    <th>Cast</th>
                    <th>Citizenship</th>
                    <th>Handicapped</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if($data): ?>
                <?php foreach ($data as $staff): ?>
                    <tr>
                        <td><?= $staff['id'] ?></td>
                        <td>
                            <div class="profile-image-container">
                                <?php if ($staff['staff_image_path']): ?>
                                    <img src="<?= $staff['staff_image_path'] ?>" alt="<?= $staff['name'] ?>" class="profile-image">
                                <?php else: ?>
                                    <div class="default-profile-image"></div>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td><?= $staff['name'] ?></td>
                        <td><?= $staff['name_in_nepali'] ?></td>
                        <td><?= $staff['gender'] ?></td>
                        <td><?= $staff['dob_bs'] ?></td>
                        <td><?= $staff['phone'] ?></td>
                        <td><?= $staff['nationality'] ?></td>
                        <td><?= $staff['permanent_address'] ?></td>
                        <td><?= $staff['father_name'] ?></td>
                        <td><?= $staff['mother_name'] ?></td>
                        <td><?= $staff['religion'] ?></td>
                        <td><?= $staff['cast'] ?></td>
                        <td><?= $staff['citizenship'] ?></td>
                        <td><?= $staff['handicapped'] == '1' ? 'Yes' : 'No' ?></td>
                        <form action="Controller/StaffController.php?page=deleteStaff" method="POST">
                            <input type="hidden" name="id" value="<?= $staff['id'] ?>">
                            <td><button type="submit" class="delete-btn">Delete</button></td>
                        </form>
                    </tr>
                <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="15" style="text-align: center; padding: 20px; color: #aaa;">No data found.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<style>
.container {
    max-width: 100%;
    overflow-x: auto;
}

h1 {
    padding: 20px;
    text-align: center;
}

.table-responsive {
    overflow-x: auto;
}

.staff-table {
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
}

.staff-table th, .staff-table td {
    border: 1px solid #ddd;
    padding: 12px;
    text-align: left;
}

.staff-table th {
    background-color: #f2f2f2;
    font-weight: bold;
}

.staff-table td {
    vertical-align: middle;
}

.staff-table td .profile-image-container {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
}

.staff-table td .profile-image-container img {
    max-width: 100%;
    max-height: 100%;
    object-fit: cover;
}

.staff-table td .default-profile-image {
    width: 100%;
    height: 100%;
    background-color: #ccc;
}

.staff-table .delete-btn {
    padding: 8px 16px;
    background-color: #dc3545;
    color: white;
    border: none;
    cursor: pointer;
    border-radius: 4px;
    font-size: 14px;
    transition: background-color 0.3s;
}

.staff-table .delete-btn:hover {
    background-color: #c82333;
}
</style>
