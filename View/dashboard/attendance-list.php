<!-- report_attendance.php -->
<div class="container">
    <h1>Attendance Report</h1>

    <!-- Date selection form -->
    <form action="Controller/AttendanceListController.php?page=listStudent" method="POST" id="attendanceForm">
        <label for="date">Select Date:</label>
        <input type="date" id="date" name="date" value="<?php echo isset($selectedDate) ? $selectedDate : ''; ?>">
        <input type="submit" value="View Report">
    </form>

    <div id="attendanceData">
        <?php if (!empty($data)) : ?>
            <h2>Attendance Data</h2>
            <table border="1">
                <tr>
                    <th>Class Name</th>
                    <th>Section Name</th>
                    <th>Subject Name</th>
                    <th>Total Students</th>
                    <th>Present Students</th>
                    <th>Absent Students</th>
                </tr>
                <?php foreach ($data as $attendance) : ?>
                    <tr>
                        <td><?php echo $attendance['class_name']; ?></td>
                        <td><?php echo $attendance['section_name']; ?></td>
                        <td><?php echo $attendance['subject_name']; ?></td>
                        <td><?php echo $attendance['total_students']; ?></td>
                        <td><?php echo $attendance['present_students']; ?></td>
                        <td><?php echo $attendance['absent_students']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <p>No attendance data available.</p>
        <?php endif; ?>
    </div>
</div>

<script>
    document.getElementById('attendanceForm').addEventListener('submit', function(event) {
        event.preventDefault();

        const formData = new FormData(this);
        const searchParams = new URLSearchParams();

        for (const pair of formData) {
            searchParams.append(pair[0], pair[1]);
        }

        fetch('Controller/AttendanceListController.php?page=listStudent', {
            method: 'POST',
            body: searchParams,
        })
        .then(response => response.text())
        .then(data => {
            document.getElementById('attendanceData').innerHTML = data;
        })
        .catch(error => console.error('Error:', error));
    });
</script>
