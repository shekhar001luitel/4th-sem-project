<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="#createSection">Section</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Today Classes</h2>
                </div>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Section</th>
                        <input id="attendance_id" type="hidden" value="">

                    </tr>
                    <?php
                    foreach ($data as $key => $value) :
                        if ($value['status'] == '0') {
                            echo '<tr>';
                            echo '<td>' . $value['class_name'] . '</td>';
                            echo '<td>' . $value['section_name'] . '</td>';
                            echo '<td>
                                    <input id="attendance_id_' . $value['class_id'] . '_' . $value['section_id'] . '" type="hidden" value="' . $value['attendance_id'] . '">
                                    <a href="#assignedStudent#' . $value['class_id'] . '#' . $value['section_id'] . '" class="btn-green" onclick="getAttendance(' . $value['class_id'] . ', ' . $value['section_id'] . ')">View</a>
                                </td>';
                            echo '</tr>';
                        }
                    endforeach;
                    ?>
                </table>

            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2>Subject</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Class Selected</h1>
                </table>
            </div>
        </div>
        <div class="content-2 class-table-list">
            <div class="recent-payments" id="sec-div" style="position: relative">
                <div class="title">
                    <h2>Student</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Subject</h1>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener(
        "hashchange",
        () => {
            let hashValue = window.location.hash.split('#');
                // var data = {
                //     'hash': encodeURIComponent(hashValue[1]),
                // };
                if(hashValue[1] == 'createSection'){
                    var formData = new URLSearchParams(hashValue[1]);
                    console.log(formData);
                    fetch('Controller/StudentAttendanceController.php?page=createSection', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: formData,
                            // body: 'hash=' + encodeURIComponent(hashValue[2]),
                        })
                        .then(response => response.text())
                        .then(data => {
                            document.querySelector("div .recent-payments").innerHTML = data
                            // document.querySelector("div .create-Classess").innerHTML = data
                        })
                        console.log(data)
                        .catch(error => console.error('Error:', error));
                        
                }else if (hashValue[1] === "assignedStudent") {
                fetch('Controller/StudentAttendanceController.php?page=assignedStudent', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data

                        return fetch('Controller/StudentAttendanceController.php?page=selectedClassStudent', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: 'class=' + encodeURIComponent(hashValue[2]) + '&section=' + encodeURIComponent(hashValue[3]),
                        });
                    })
                    .then(response => response.text())
                    .then(tableData => {
                        document.querySelector("#sec-div").innerHTML = tableData;
                    })
                    .catch(error => console.error('Error:', error));
                }
                else if (hashValue[1] === "updateSection") {
                fetch('Controller/StudentAttendanceController.php?page=assignedStudent', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
                }
            },
        false
    );
</script>
<script>
    function getAttendance(classId, sectionId) {
        var selectedAttendanceIdInput = document.getElementById("attendance_id");
        selectedAttendanceIdInput.value = document.getElementById("attendance_id_" + classId + "_" + sectionId).value; //

    }

function showSection(value) {
    var selectedSubjectIdInput = document.getElementById("selectedSubjectId");
    var selectedAttendanceIdInput = document.getElementById("selectedStaffAttendanceId");
    selectedSubjectIdInput.value = value;
    selectedAttendanceIdInput.value = document.getElementById("attendance_id").value;
}
function validateForm(){
    var selectedSubjectIdInput = document.getElementById("selectedSubjectId");
    if (!selectedSubjectIdInput.value) {
        alert("Subject should be selected first");
        event.preventDefault();
    }
}
</script>
