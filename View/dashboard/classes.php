<div class="container">
    <div class="header">
        <div class="nav">
            <div class="nav"> <button class="btn-green">
                    <li><a href="/setup.php">Program</a></li>
                </button> </div>
            <div>
                <form method="post" action="Controller/LoginController.php">
                    <input type="hidden" name="page" value="logout">
                    <button type="submit" class="btn-red">
                        <li>Logout</li>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="content">
        <?php
        // session_start();
        if (isset($_SESSION["errorMessage"])) {
        ?>
            <div style="font-size: 30px; text-align: center; padding:1%" class="error-info"><?php echo $_SESSION["errorMessage"]; ?></div>
        <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
        ?>
            <div class="error-info" style="font-size: 30px; text-align: center; padding:1%; color:green; !important"><?php echo $_SESSION["successMessage"]; ?></div>
        <?php
            unset($_SESSION["successMessage"]);
        }
        ?>
        <div class="content-2">
            <div class="new-students">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <tr>
                        <th>Batch  Name</th>
                        <th>View Class</th>
                    </tr>
                    <?php
                    // dd($data);
                    foreach ($data as $key) :
                        echo '<tr>';
                        echo '<td>' . ($key['faculty_name']) .'-'.($key['batch_name']) . '</td>';
                        echo '<td><a href="#viewClass#' . $key['batch_id'] . '#' . '" class="btn-green">View</a></td>';
                        echo '</tr>';
                    endforeach;
                    ?>
                </table>
            </div>
            <div class="recent-payments">
                <div class="title">
                    <h2>Class</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Class Selected</h1>
                </table>
            </div>
        </div>
        <div class="content-2 class-table-list">
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Batch Selected</h1>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener(
        "hashchange",
        () => {
            let hashValue = window.location.hash.split('#');
            console.log(hashValue);

            if (hashValue[1] == "viewClass") {
                var data = {
                    'hash': encodeURIComponent(hashValue[2]),
                };

                var formData = new URLSearchParams(data);

                // First fetch request
                fetch('Controller/ClassessController.php?page=viewClass', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            body: formData,
                        })
                    .then(response => response.text())
                    .then(tableData => {
                        document.querySelector(".class-table-list .recent-payments").innerHTML = tableData;
                    })
                    .catch(error => console.error('Error:', error));
            } else {
                var data = {
                    'hash': encodeURIComponent(hashValue[2]),
                };

                var formData = new URLSearchParams(data);
                fetch('Controller/ClassessController.php?page=ClassessEdit', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: formData,
                        // body: 'hash=' + encodeURIComponent(hashValue[2]),
                    })
                    .then(response => response.text())
                    .then(data => {
                        document.querySelector("div .recent-payments").innerHTML = data
                        // document.querySelector("div .create-Classess").innerHTML = data
                    })
                    .catch(error => console.error('Error:', error));
            }
        },
        false
    );
</script>
