<div class="container">
    <?php
        session_start();
        if (isset($_SESSION["errorMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%;" class="error-info"><?= $_SESSION["errorMessage"] ?></div>
    <?php
            unset($_SESSION["errorMessage"]);
        } elseif (isset($_SESSION["successMessage"])) {
    ?>
            <div style="font-size: 20px; text-align: center; padding: 1%; color: green;" class="success-info"><?= $_SESSION["successMessage"] ?></div>
    <?php
            unset($_SESSION["successMessage"]);
        }
    ?>
    <h1>Admin Management</h1>
    <div class="table-responsive">
        <table class="admin-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($data && !empty($data)): ?>
                    <?php foreach ($data as $admin): ?>
                        <tr>
                            <td><?= $admin['id'] ?></td>
                            <td><?= $admin['email'] ?></td>
                            <td><?= $admin['role'] ?></td>
                            <td>
                                <?php if (in_array($role, ['super-admin', 'admin']) && $admin['role'] === 'non-admin'): ?>
                                    <form action="Controller/SettingsController.php?page=acceptAdmin" method="POST">
                                        <input type="hidden" name="admin_id" value="<?= $admin['id'] ?>">
                                        <button type="submit" class="accept-btn">Accept</button>
                                    </form>
                                    <form action="Controller/SettingsController.php?page=deleteAdmin" method="POST">
                                        <input type="hidden" name="admin_id" value="<?= $admin['id'] ?>">
                                        <button type="submit" class="delete-btn">Delete</button>
                                    </form>
                                <?php endif; ?>
                                <?php if (in_array($role, ['super-admin']) && $admin['role'] === 'admin'): ?>
                                    <form action="Controller/SettingsController.php?page=deleteAdmin" method="POST">
                                        <input type="hidden" name="admin_id" value="<?= $admin['id'] ?>">
                                        <button type="submit" class="delete-btn">Delete</button>
                                    </form>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="5" style="text-align: center; padding: 20px; color: #aaa;">No data found.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<style>
.container {
    max-width: 100%;
    overflow-x: auto;
    padding: 20px;
}

h1 {
    text-align: center;
    margin-bottom: 20px;
}

.table-responsive {
    overflow-x: auto;
}

.admin-table {
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
}

.admin-table th,
.admin-table td {
    border: 1px solid #ddd;
    padding: 12px;
    text-align: left;
}

.admin-table th {
    background-color: #f2f2f2;
    font-weight: bold;
}

.admin-table td {
    vertical-align: middle;
}

.admin-table td .profile-image-container {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
}

.admin-table td .profile-image-container img {
    max-width: 100%;
    max-height: 100%;
    object-fit: cover;
}

.admin-table td .default-profile-image {
    width: 100%;
    height: 100%;
    background-color: #ccc;
}

.admin-table .accept-btn,
.admin-table .delete-btn {
    padding: 8px 16px;
    border: none;
    cursor: pointer;
    border-radius: 4px;
    font-size: 14px;
    transition: background-color 0.3s, color 0.3s, border-color 0.3s;
}

.admin-table .accept-btn {
    background-color: #28a745;
    color: white;
}

.admin-table .delete-btn {
    background-color: #dc3545;
    color: white;
}

.admin-table .accept-btn:hover,
.admin-table .delete-btn:hover {
    filter: brightness(90%);
}

.admin-table .delete-btn[disabled] {
    background-color: #ccc;
    cursor: not-allowed;
    color: #555;
}

.admin-table .delete-btn[disabled]:hover {
    filter: none;
}

.admin-table .edit-btn,
.admin-table .delete-btn {
    margin-right: 5px;
}

</style>
