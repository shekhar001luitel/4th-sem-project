<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->check_login();
        $this->dashboard();
    }

    public function dashboard()
    {

        $cssFiles = ['dashboard'];
        $db = new Database();

        // Count the distinct staff members present today
        $today_staff_present_query = "
            SELECT COUNT(DISTINCT staff_id) AS count_staff_present 
            FROM staff_attendance 
            WHERE attendance_date = CURDATE()
        ";
        $today_staff_present_result = $db->sql($today_staff_present_query);
        $today_staff_present_count = $today_staff_present_result[0]['count_staff_present'];

        // Fetch other required data in a single query
        $query = "
            SELECT 
                (SELECT COUNT(*) FROM student_details) AS total_students,
                (SELECT COUNT(*) FROM faculty) AS total_faculty_members,
                (SELECT COUNT(*) FROM batch) AS total_batches,
                (SELECT COUNT(*) FROM classes) AS total_classes,
                (SELECT COUNT(*) FROM section) AS total_sections,
                (SELECT COUNT(*) FROM subject) AS total_subjects,
                (SELECT COUNT(*) FROM staff_details) AS total_staff_members,
                (SELECT COUNT(*) FROM student_attendance WHERE attendance_date = CURDATE() AND attendance_status = 'absent') AS today_student_absent,
                (SELECT COUNT(*) FROM student_details WHERE gender = 'Male') AS total_male_students,
                (SELECT COUNT(*) FROM student_details WHERE gender = 'Female') AS total_female_students,
                (SELECT COUNT(*) FROM staff_details WHERE gender = 'Male') AS total_male_staff,
                (SELECT COUNT(*) FROM staff_details WHERE gender = 'Female') AS total_female_staff
        ";

        $result = $db->sql($query);
        $data = $result[0];
        $data['today_staff_present'] = $today_staff_present_count; // Add today's staff present count to the data array
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar', ['sidebar' => $sidebar]);
        view_require('dashboard/dashboard', ['data' => $data]);
        view_require('_parts/footer');
    }
}

new Dashboard();
