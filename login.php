<?php
// session_start();
include "helper.php";
include 'Controller/Controller.php';
class LoginController extends Controller
{

    public function __construct()
    {
        $this->return_dashboard();
        $this->index();
    }
    public function index()
    {
        $cssFiles = ['login'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('login/login');
        view_require('_parts/footer');
        // $this->test();
    }
}

new LoginController();