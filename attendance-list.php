<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';

class AttendanceListController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Kathmandu');
        $this->check_login();
        $this->index();
    }

    public function index()
    {
        $db = new Database();
        $role = $_SESSION['role'];

        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/attendance-list');
        view_require('_parts/footer');
    }
}

new AttendanceListController();
