<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";

class StudentController extends Controller
{
    public function __construct()
    {
        if ($_GET['page'] == 'undefined') {
            exit;
        }
        if ($_GET['page'] === 'inputStudent') { // Ajax call
            $this->inputStudent();
        }elseif($_GET['page'] === 'deleteStudent'){
            $this->deleteStudent();
        }
    }

    public function inputStudent()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Handle file uploads
            $studentImage = $this->uploadFile('student_image', 'studentImage');
            $father_image = $this->uploadFile('father_image', 'parentImage');
            $mother_image = $this->uploadFile('mother_image', 'parentImage');
            $guardian_image = $this->uploadFile('guardian_image', 'parentImage');

            // Sanitize inputs
            $name = input_sanitize($_POST['name']);
            $name_in_nepali = input_sanitize($_POST['name_in_nepali']);
            $gender = input_sanitize($_POST['gender']);
            $dob_bs = input_sanitize($_POST['dob_bs']);
            $faculty_id = input_sanitize($_POST['faculty']);
            $batch_id = input_sanitize($_POST['batch']);
            $class_id = input_sanitize($_POST['class']);
            $section_id = input_sanitize($_POST['section']);
            $phone = input_sanitize($_POST['phone']);
            $nationality = input_sanitize($_POST['nationality']);
            $permanent_province = input_sanitize($_POST['permanent_province']);
            $permanent_district = input_sanitize($_POST['permanent_district']);
            $permanent_municipality = input_sanitize($_POST['permanent_municipality']);
            $permanent_address = input_sanitize($_POST['permanent_address']);
            $father_name = input_sanitize($_POST['father_name']);
            $f_occupation = input_sanitize($_POST['f_occupation']);
            $f_cell = input_sanitize($_POST['f_cell']);
            $f_email = input_sanitize($_POST['f_email']);
            $f_office = input_sanitize($_POST['f_office']);
            $mother_name = input_sanitize($_POST['mother_name']);
            $m_occupation = input_sanitize($_POST['m_occupation']);
            $m_cell = input_sanitize($_POST['m_cell']);
            $m_email = input_sanitize($_POST['m_email']);
            $m_office = input_sanitize($_POST['m_office']);
            $guardian_name = input_sanitize($_POST['guardian_name']);
            $g_relation = input_sanitize($_POST['g_relation']);
            $g_cell = input_sanitize($_POST['g_cell']);
            $religion = input_sanitize($_POST['religion']);
            $cast = input_sanitize($_POST['cast']);
            $citizenship = input_sanitize($_POST['citizenship']);
            $handicapped = input_sanitize($_POST['handicapped']);
            $password = 'student_password@123';
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);


            // Validate required fields
            $requiredFields = [
                'student_image' => $studentImage,
                'name' => $name,
                'gender' => $gender,
                'dob_bs' => $dob_bs,
                'faculty_id' => $faculty_id,
                'batch_id' => $batch_id,
                'class_id' => $class_id,
                'section_id' => $section_id,
                'phone' => $phone,
                'nationality' => $nationality,
            ];

            foreach ($requiredFields as $field => $value) {
                if (empty($value)) {
                    $_SESSION['errorMessage'] = ucfirst(str_replace('_', ' ', $field)) . ' is required!';
                    header("Location: ../student.php");
                    exit();
                }
            }

            $db = new Database();
            $insertData = [
                'student_image_path' => $studentImage,
                'name' => $name,
                'name_in_nepali' => $name_in_nepali,
                'gender' => $gender,
                'dob_bs' => $dob_bs,
                'faculty_id' => $faculty_id,
                'batch_id' => $batch_id,
                'class_id' => $class_id,
                'section_id' => $section_id,
                'phone' => $phone,
                'nationality' => $nationality,
                'permanent_province' => $permanent_province,
                'permanent_district' => $permanent_district,
                'permanent_municipality' => $permanent_municipality,
                'permanent_address' => $permanent_address,
                'father_name' => $father_name,
                'father_image_path' => $father_image,
                'f_occupation' => $f_occupation,
                'f_cell' => $f_cell,
                'f_email' => $f_email,
                'f_office' => $f_office,
                'mother_name' => $mother_name,
                'mother_image_path' => $mother_image,
                'm_occupation' => $m_occupation,
                'm_cell' => $m_cell,
                'm_email' => $m_email,
                'm_office' => $m_office,
                'guardian_name' => $guardian_name,
                'guardian_image_path' => $guardian_image,
                'g_relation' => $g_relation,
                'g_cell' => $g_cell,
                'religion' => $religion,
                'cast' => $cast,
                'citizenship' => $citizenship,
                'handicapped' => $handicapped,
                'password' => $hashedPassword,
            ];

            $insertResult = $db->create('student_details', $insertData);
            if ($insertResult) {
                $_SESSION['successMessage'] = 'Student data inserted successfully!';
                header("Location: ../student.php");
                exit();
            } else {
                $_SESSION['errorMessage'] = 'Failed to insert student data. Please try again!';
                header("Location: ../student.php");
                exit();
            }
        } else {
            $_SESSION['errorMessage'] = 'Invalid request method!';
            header("Location: ../student.php");
            exit();
        }
    }

    private function uploadFile($inputName, $folderName)
    {
        if (isset($_FILES[$inputName]) && $_FILES[$inputName]['error'] == 0) {
            $fileTmpPath = $_FILES[$inputName]['tmp_name'];
            $fileName = $_FILES[$inputName]['name'];
            $fileSize = $_FILES[$inputName]['size'];
            $fileType = $_FILES[$inputName]['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            // Generate unique filename
            $newFileName = uniqid() . '_' . time() . '.' . $fileExtension;

            // Directory where the file should be uploaded
            $uploadFileDir = "../View/assets/image/$folderName/";
            $dest_path = $uploadFileDir . $newFileName;

            if (move_uploaded_file($fileTmpPath, $dest_path)) {
                return $dest_path;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    public function deleteStudent()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and (!empty($_POST['id']))) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);
            $condition = "WHERE id = '" . $id . "'";
            $db->delete('student_details', $condition);
            $_SESSION['successMessage'] = 'Student Deleted Successfully!';
            header("Location: ../student-management.php");
            exit;
        } else {
            $_SESSION['errorMessage'] = 'Student Deletion Failed!';
            header("Location: ../student-management.php");
            exit;
        }
    }
}

new StudentController();
?>
