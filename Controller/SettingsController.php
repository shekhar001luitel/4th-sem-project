<?php
// session_start();

include "../helper.php";
include "Controller.php";
include "../Model/Database.php";

class SettingsController extends Controller
{
    public function __construct()
    {
        if ($_GET["page"] == "undefined") {
            exit();
        }
        if ($_GET["page"] === "update") {
            $this->setting_update();
        } elseif ($_GET["page"] === "delete") {
            $this->delete_user();
        } elseif ($_GET["page"] === "inputProgram") {
            // Ajex call
            $this->ajax_load();
        } elseif ($_GET["page"] === "programAdd") {
            // Ajex call
            $this->createProgram();
        } elseif ($_GET["page"] === "programId") {
            // Ajex call
            $this->createId();
        } elseif ($_GET["page"] === "viewProgram") {
            // Ajex call
            $this->viewProgram();
        } elseif ($_GET["page"] === "editProgram") {
            // Ajex call
            $this->editProgram();
        } elseif ($_GET["page"] === "programUpdate") {
            // Ajex call
            $this->updateProgram();
        } elseif ($_GET["page"] === "programDelete") {
            // Ajex call
            $this->deleteProgram();
        }elseif($_GET["page"] === "acceptAdmin"){
            $this->acceptAdmin();
        }elseif($_GET["page"] === "deleteAdmin"){
            $this->deleteAdmin();
        }
    }

    public function ajax_view()
    {
        echo '
        <div class="title">
            <h2>Add Program</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SettingsController.php?page=programAdd">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Name</label>
            </div>
            <input id="text" name="name" type="text">
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="level">Level</label>
            </div>
            <select name="level">
                <option value="">Select Level</option>
                <option value="intermediate">Intermediate</option>
                <option value="bachelor">Bachelors Level</option>
                <option value="master">Masters Level</option>
                <option value="mphil">MPhil</option>
                <option value="phd">Ph.D.</option>
            </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="affiliating-university">Affiliating University</label>
            </div>
            <select class="" name="affiliate_university" id="">
                <option value="">Select University</option>
                <option value="tu">Tribhuvan University</option>
                <option value="pokhara">Pokhara University</option>
                <option value="purbanchal">Purbanchal University</option>
            </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="affiliating-university">Enrollment Type</label>
            </div>
            <select class="" name="enrollment_type" id="">
                <option value="">Select System</option>
                <option value="annual">Annual System</option>
                <option value="semester">Semester System</option>
            </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="description">Description</label>
            </div>
            <input id="text" name="description">
        </div>
        <button class="btn-green" type="submit">Save</button>
    </form>
    </div>';
    }

    public function setting_update()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and !empty($_POST["email"])) {
            $email = input_sanitize($_POST["email"]);
            // Check Email Formate
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $_SESSION["errorMessage"] = "Email Field Required!";
                header("Location: ../setting.php");
                exit();
            }
            $updateData = [
                "email" => $email,
            ];
            if (!empty($_POST["password"])) {
                $password = input_sanitize($_POST["password"]);
                $password = password_hash($password, PASSWORD_DEFAULT);
                $updateData = [
                    "email" => $email,
                    "password" => $password,
                ];
            }
            $db = new Database();

            $id = $this->User_Detail();
            $id = $id["id"];
            $condition = "WHERE id = '" . $id . "'";
            $updates = $db->update("users", $updateData, $condition);
            // Message
            if ($updates) {
                $_SESSION["successMessage"] = "Update Successful!";
                header("Location: ../setting.php");
                exit();
            } else {
                $_SESSION["errorMessage"] = "Update Failed!";
                header("Location: ../setting.php");
                exit();
            }
        } else {
            $_SESSION["errorMessage"] = "Update Failed!";
            header("Location: ../setting.php");
            exit();
        }
    }
    
    public function delete_user()
    {
        $db = new Database();
        $id = $this->User_Detail();
        $id = $id["id"];
        $condition = "WHERE id = '" . $id . "'";
        $db->delete("users", $condition);
        $this->logout();
    }
    public function deleteProgram()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and !empty($_POST["id"])) {
            $db = new Database();
            $id = input_sanitize($_POST["id"]);
            $condition = "WHERE id = '" . $id . "'";
            $db->delete("faculty", $condition);
            $_SESSION["successMessage"] = "Program Deleted Successfully!";
            header("Location: ../setup.php");
            exit();
        } else {
            $_SESSION["errorMessage"] = "Program Deletion Failed!";
            header("Location: ../setup.php");
            exit();
        }
    }

    public function ajax_load()
    {
        echo $this->ajax_view();
    }

    // Create Program
    public function createProgram()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["name"]) && !empty($_POST["description"])) {
            $name = input_sanitize($_POST["name"]);
            $name = strtoupper($name);
            $description = input_sanitize($_POST["description"]);
            $level = input_sanitize($_POST["level"]);
            $affiliate_university = input_sanitize($_POST["affiliate_university"]);
            $enrollment_type = input_sanitize($_POST["enrollment_type"]);
            $db = new Database();
    
            // Check if the program name already exists
            $existingProgram = $db->read("faculty", "WHERE faculty_name = '" . $name . "'");
            if (!empty($existingProgram)) {
                $_SESSION["errorMessage"] = "Program already exists. Please use a different name.";
                header("Location: ../setup.php");
                exit();
            }
    
            $createData = [
                "faculty_name" => $name,
                "level" => $level,
                "affiliate_university" => $affiliate_university,
                "enrollment_type" => $enrollment_type,
                "description" => $description,
            ];
    
            $createResult = $db->create("faculty", $createData);
    
            if ($createResult) {
                $_SESSION["successMessage"] = "Program Created Successfully!";
            } else {
                $_SESSION["errorMessage"] = "Failed to create program. Please try again!";
            }
        } else {
            $_SESSION["errorMessage"] = "Fill all the required fields!";
        }
        header("Location: ../setup.php");
        exit();
    }
    
    public function updateProgram()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["name"]) && !empty($_POST["id"]) && !empty($_POST["description"])) {
            $id = input_sanitize($_POST["id"]);
            $name = input_sanitize($_POST["name"]);
            $name = strtoupper($name);
            $description = input_sanitize($_POST["description"]);
            $level = input_sanitize($_POST["level"]);
            $affiliate_university = input_sanitize($_POST["affiliate_university"]);
            $enrollment_type = input_sanitize($_POST["enrollment_type"]);
            $db = new Database();
    
            // Check if the program name already exists, excluding the current program being updated
            $existingProgram = $db->read("faculty", "WHERE faculty_name = '" . $name . "' AND id != '" . $id . "'");
            if (!empty($existingProgram)) {
                $_SESSION["errorMessage"] = "Program name already exists. Please use a different name.";
                header("Location: ../setup.php");
                exit();
            }
    
            $updateData = [
                "faculty_name" => $name,
                "level" => $level,
                "affiliate_university" => $affiliate_university,
                "enrollment_type" => $enrollment_type,
                "description" => $description,
            ];
    
            $condition = "WHERE id = '" . $id . "'";
            $updateResult = $db->update("faculty", $updateData, $condition);
    
            if ($updateResult) {
                $_SESSION["successMessage"] = "Program Updated Successfully!";
            } else {
                $_SESSION["errorMessage"] = "Failed to update program. Please try again!";
            }
        } else {
            $_SESSION["errorMessage"] = "Fill all the required fields!";
        }
        header("Location: ../setup.php");
        exit();
    }
    
    public function viewProgram()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["hash"])) {
            $db = new Database();
            $id = input_sanitize($_POST["hash"]);
            $condition = "WHERE id = " . $id;
            $readResult = $db->read("faculty", $condition);
            if (!empty($readResult)) {
                echo '<div class="title">
                    <h2>Program</h2>
                </div>
                <div style="display: flex;
                flex-direction: column;
                align-items: center;
                row-gap: 7px";>
                    <div style="margin-bottom: 15px; text-transform: capitalize;">
                    <strong>Name:</strong> ' . $readResult["faculty_name"] . '
                    </div>
                    <div style="margin-bottom: 15px;text-transform: capitalize;">
                        <strong>Level:</strong> ' . $readResult["level"] . '
                    </div>
                    <div style="margin-bottom: 15px;text-transform: uppercase;">
                        <strong style="text-transform: capitalize;">Affiliate University:</strong> ' . $readResult["affiliate_university"] . '
                    </div>
                    <div style="margin-bottom: 15px;text-transform: capitalize;">
                        <strong>Enrollment Type:</strong> ' . $readResult["enrollment_type"] . '
                    </div>
                    <div style="margin-bottom: 15px;text-transform: capitalize;">
                        <strong>Description:</strong> ' . $readResult["description"] . '
                    </div>
                </div>';
            } else {
                $_SESSION["errorMessage"] = "Program not found!";
                header("Location: ../setup.php");
                exit();
            }
        } else {
            $_SESSION["errorMessage"] = "Invalid request!";
            header("Location: ../setup.php");
            exit();
        }
    }
    
    public function editProgram()
    {
        $db = new Database();
        $id = input_sanitize($_POST["hash"]);
        $readResult = $db->fetchById("faculty", "id", $id);
        echo '
        <div class="title">
            <h2>' .
            $readResult["faculty_name"] .
            ' Program Edit</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SettingsController.php?page=programUpdate">
        <input type="hidden" name="id" value="' .
            $readResult["id"] .
            '">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Name</label>
            </div>
            <input id="text" name="name" value="' .
            $readResult["faculty_name"] .
            '" type="text">
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="level">Level</label>
            </div>
            <select name="level">
                <option value="" selected>Select Level</option>
                <option value="intermediate" '.($readResult["level"] == "intermediate" ? "selected" : "").'>Intermediate</option>
                <option value="bachelor" '.($readResult["level"] == "bachelor" ? "selected" : "").'>Bachelors Level</option>
                <option value="master" '.($readResult["level"] == "master" ? "selected" : "").'>Masters Level</option>
                <option value="mphil" '.($readResult["level"] == "mphil" ? "selected" : "").'>MPhil</option>
                <option value="phd" '.($readResult["level"] == "phd" ? "selected" : "").'>Ph.D.</option>
        </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="affiliating-university">Affiliating University</label>
            </div>
            <select class="" name="affiliate_university" id="">
                <option value="" selected>Select University</option>
                <option value="tu" '.($readResult["affiliate_university"] == "tu" ? "selected" : "").'>Tribhuvan University</option>
                <option value="pokhara" '.($readResult["affiliate_university"] == "pokhara" ? "selected" : "").'>Pokhara University</option>
                <option value="purbanchal" '.($readResult["affiliate_university"] == "purbanchal" ? "selected" : "").'>Purbanchal University</option>
            </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="affiliating-university">Enrollment Type</label>
            </div>
            <select class="" name="enrollment_type" id="">
                <option value="" selected >Select System</option>
                <option '.($readResult["enrollment_type"] == "annual" ? "selected" : "").' value="annual">Annual System</option>
                <option '.($readResult["enrollment_type"] == "semester" ? "selected" : "").' value="semester">Semester System</option>
            </select>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="description">Description</label>
            </div>
            <input id="text" value="' .
            $readResult["description"] .
            '" name="description">
        </div>
        <button class="btn-green" type="submit">Update</button>
        </form>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SettingsController.php?page=programDelete">
        <input name="id" type="hidden" value="' .
            $readResult["id"] .
            '">
        <button class="btn-red" type="submit">Delete</button>
        </form>
    </div>';
    }

    public function createId()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" and !empty($_POST["hash"])) {
            $id = input_sanitize($_POST["hash"]);
            if ($id != "undefined") {
                $this->bachDetails($id);
            }
        }
    }
    public function bachDetails($id)
    {
        $db = new Database();
        $sql = "SELECT batch.*, faculty.id, faculty.faculty_name FROM batch INNER JOIN faculty ON batch.faculty_id = faculty.id WHERE id = $id";
        $updates = $db->sql($sql);

        if (!empty($updates)) {
            // Extracting faculty name from the first result since it should be the same for all results
            $facultyName = $updates[0]["faculty_name"];

            echo '<div class="title">
                    <h2>Batch ' .
                $facultyName .
                '</h2>
                </div>
                <table>
                    <tr>
                        <th>Batch Name</th>
                        <th>Description</th>
                    </tr>';

            foreach ($updates as $data) {
                echo '
                    <tr>
                        <td>' .
                    $data["batch_name"] .
                    '</td>
                        <td>' .
                    $data["description"] .
                    '</td>
                    </tr>';
            }

            echo "</table>";
        } else {
            echo '<div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h3>
                </table>';
        }
    }

    public function acceptAdmin()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["admin_id"])) {
            $adminId = $_POST["admin_id"];
            $db = new Database();

            // Update admin role to 'admin'
            $updateData = [
                "role" => "admin"
            ];

            $condition = "WHERE id = '" . $adminId . "' AND role = 'non-admin'";
            $updateResult = $db->update("users", $updateData, $condition);

            if ($updateResult) {
                $_SESSION["successMessage"] = "Admin accepted successfully!";
            } else {
                $_SESSION["errorMessage"] = "Failed to accept admin!";
            }
        } else {
            $_SESSION["errorMessage"] = "Invalid request!";
        }
        header("Location: ../user-management.php");
        exit();
    }

    public function deleteAdmin()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["admin_id"])) {
            $adminId = $_POST["admin_id"];
            $db = new Database();

            // Delete admin
            $condition = "WHERE id = '" . $adminId . "'";
            $deleteResult = $db->delete("users", $condition);
            if ($deleteResult) {
                $_SESSION["successMessage"] = "Admin deleted successfully!";
            } else {
                $_SESSION["errorMessage"] = "Failed to delete admin!";
            }
        } else {
            $_SESSION["errorMessage"] = "Invalid request!";
        }
        header("Location: ../user-management.php");
        exit();
    }
}
new SettingsController();
