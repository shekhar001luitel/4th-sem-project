<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";

class StaffController extends Controller
{
    public function __construct()
    {
        if ($_GET['page'] == 'undefined') {
            exit;
        }
        if ($_GET['page'] === 'inputStaff') { // Ajax call
            $this->inputStaff();
        }elseif($_GET['page'] === 'deleteStaff'){
            $this->deleteStaff();
        }elseif($_GET['page'] === 'update'){
            $this->updateStaff();
        }
    }

    public function inputStaff()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Handle file uploads
            $staffImage = $this->uploadFile('staff_image', 'staffImage');
            $father_image = $this->uploadFile('father_image', 'parentImage');
            $mother_image = $this->uploadFile('mother_image', 'parentImage');

            // Sanitize inputs
            $name = input_sanitize($_POST['name']);
            $name_in_nepali = input_sanitize($_POST['name_in_nepali']);
            $gender = input_sanitize($_POST['gender']);
            $dob_bs = input_sanitize($_POST['dob_bs']);
            $phone = input_sanitize($_POST['phone']);
            $nationality = input_sanitize($_POST['nationality']);
            $permanent_province = input_sanitize($_POST['permanent_province']);
            $permanent_district = input_sanitize($_POST['permanent_district']);
            $permanent_municipality = input_sanitize($_POST['permanent_municipality']);
            $permanent_address = input_sanitize($_POST['permanent_address']);
            $father_name = input_sanitize($_POST['father_name']);
            $f_occupation = input_sanitize($_POST['f_occupation']);
            $f_cell = input_sanitize($_POST['f_cell']);
            $f_office = input_sanitize($_POST['f_office']);
            $mother_name = input_sanitize($_POST['mother_name']);
            $m_occupation = input_sanitize($_POST['m_occupation']);
            $m_office = input_sanitize($_POST['m_office']);
            $religion = input_sanitize($_POST['religion']);
            $cast = input_sanitize($_POST['cast']);
            $citizenship = input_sanitize($_POST['citizenship']);
            $handicapped = input_sanitize($_POST['handicapped']);
            $password = '123';
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);


            // Validate required fields
            $requiredFields = [
                'staff_image' => $staffImage,
                'name' => $name,
                'gender' => $gender,
                'dob_bs' => $dob_bs,
                'phone' => $phone,
                'nationality' => $nationality,
            ];

            foreach ($requiredFields as $field => $value) {
                if (empty($value)) {
                    $_SESSION['errorMessage'] = ucfirst(str_replace('_', ' ', $field)) . ' is required!';
                    header("Location: ../staff.php");
                    exit();
                }
            }

            $db = new Database();
            $insertData = [
                'staff_image_path' => $staffImage,
                'name' => $name,
                'name_in_nepali' => $name_in_nepali,
                'gender' => $gender,
                'dob_bs' => $dob_bs,
                'phone' => $phone,
                'nationality' => $nationality,
                'permanent_province' => $permanent_province,
                'permanent_district' => $permanent_district,
                'permanent_municipality' => $permanent_municipality,
                'permanent_address' => $permanent_address,
                'father_name' => $father_name,
                'father_image_path' => $father_image,
                'f_occupation' => $f_occupation,
                'f_cell' => $f_cell,
                'f_office' => $f_office,
                'mother_name' => $mother_name,
                'mother_image_path' => $mother_image,
                'm_occupation' => $m_occupation,
                'm_cell' => $m_cell,
                'm_office' => $m_office,
                'religion' => $religion,
                'cast' => $cast,
                'citizenship' => $citizenship,
                'handicapped' => $handicapped,
                'password' => $hashedPassword,
            ];

            $insertResult = $db->create('staff_details', $insertData);
            if ($insertResult) {
                $_SESSION['successMessage'] = 'Staff data inserted successfully!';
                header("Location: ../staff.php");
                exit();
            } else {
                $_SESSION['errorMessage'] = 'Failed to insert staff data. Please try again!';
                header("Location: ../staff.php");
                exit();
            }
        } else {
            $_SESSION['errorMessage'] = 'Invalid request method!';
            header("Location: ../staff.php");
            exit();
        }
    }

    private function uploadFile($inputName, $folderName)
    {
        if (isset($_FILES[$inputName]) && $_FILES[$inputName]['error'] == 0) {
            $fileTmpPath = $_FILES[$inputName]['tmp_name'];
            $fileName = $_FILES[$inputName]['name'];
            $fileSize = $_FILES[$inputName]['size'];
            $fileType = $_FILES[$inputName]['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            // Generate unique filename
            $newFileName = uniqid() . '_' . time() . '.' . $fileExtension;

            // Directory where the file should be uploaded
            $uploadFileDir = "../View/assets/image/$folderName/";
            $dest_path = $uploadFileDir . $newFileName;

            if (move_uploaded_file($fileTmpPath, $dest_path)) {
                return $dest_path;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    public function deleteStaff()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and (!empty($_POST['id']))) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);
            $condition = "WHERE id = '" . $id . "'";
            $db->delete('staff_details', $condition);
            $_SESSION['successMessage'] = 'Staff Deleted Successfully!';
            header("Location: ../teacher-management.php");
            exit;
        } else {
            $_SESSION['errorMessage'] = 'Staff Deletion Failed!';
            header("Location: ../teacher-management.php");
            exit;
        }
    }
    public function updateStaff()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && !empty($_POST["password"])) {
                $password = input_sanitize($_POST["password"]);
                $password = password_hash($password, PASSWORD_DEFAULT);
                $updateData = [
                    "password" => $password,
                ];
            $db = new Database();

            $id = $this->Staff_Details();
            $id = $id["id"];
            $condition = "WHERE id = '" . $id . "'";
            $updates = $db->update("staff_details", $updateData, $condition);
            // Message
            if ($updates) {
                $_SESSION["successMessage"] = "Update Successful!";
                header("Location: ../setting.php");
                exit();
            } else {
                $_SESSION["errorMessage"] = "Update Failed!";
                header("Location: ../setting.php");
                exit();
            }
        } else {
            $_SESSION["errorMessage"] = "Update Failed!";
            header("Location: ../setting.php");
            exit();
        }
    }
    
    public function delete_user()
    {
        $db = new Database();
        $id = $this->Staff_Details();
        $id = $id["id"];
        $condition = "WHERE id = '" . $id . "'";
        $db->delete("staff_details", $condition);
        $this->logout();
    }
}

new StaffController();
?>
