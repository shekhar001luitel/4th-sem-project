<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";
class BatchController extends Controller
{
    public function __construct()
    {

        // dd($_GET);
        if($_GET['page'] == 'undefined'){
            exit;
        }
        if ($_GET['page'] === 'inputBatch') { // Ajex call
            $this->bathId();
        } elseif ($_GET['page'] === 'addBatch') {
            $this->createBatch();
        } elseif ($_GET['page'] === 'tableBatch') {
            $this->batchTable();
        } elseif ($_GET['page'] === 'batchEdit') {
            $this->editBatch();
        } elseif ($_GET['page'] === 'batchDelete') {
            $this->deleteBatch();
        } elseif ($_GET['page'] === 'batchUpdate') {
            $this->updateBatch();
        }
    }

    public function bathId()
    {
        $db = new Database;
        $condition = "where id =" . $_POST['hash'];
        $data = $db->read('faculty', $condition);
        echo '
        <div class="title">
            <h2>' . $data['faculty_name'] . ' Batch</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/BatchController.php?page=addBatch">
        <div>
        <input id="text" value="' . $_POST['hash'] . '" name="program_id" type="hidden">
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Batch Name</label>
            </div>
            <input id="text" name="name" type="text">
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="description">Description</label>
            </div>
            <input id="text" name="description">
        </div>
        <button class="btn-green" type="submit">Save</button>
    </form>
    </div>';
    }
    public function createBatch()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['name'], $_POST['program_id'], $_POST['description'])) {
            $name = input_sanitize($_POST['name']);
            $description = input_sanitize($_POST['description']);
            $program_id = input_sanitize($_POST['program_id']);

            $db = new Database();
            $condition = "WHERE faculty_id='" . $program_id . "' AND batch_name='" . $name . "'";
            $readResult = $db->read('batch', $condition);

            if (!empty($readResult)) {
                $_SESSION['errorMessage'] = 'Batch Name Already Exists!';
                header("Location: ../batch.php");
                exit();
            }

            $createData = [
                'batch_name' => $name,
                'description' => $description,
                'faculty_id' => $program_id,
            ];

            $createResult = $db->create('batch', $createData);

            if (!empty($createResult)) {
                // Batch created successfully, proceed to create classes
                $classesCreated = $this->createClassess($program_id, $name);
                if ($classesCreated) {
                    $_SESSION['successMessage'] = 'Batch Created Successfully with Classes!';
                } else {
                    $_SESSION['successMessage'] = 'Batch Created Successfully!';
                }
                header("Location: ../batch.php");
                exit();
            } else {
                $_SESSION['errorMessage'] = 'Failed to Create Batch, Please Try Again!';
                header("Location: ../batch.php");
                exit();
            }
        } else {
            $_SESSION['errorMessage'] = 'Fill all required fields!';
            header("Location: ../setup.php");
            exit();
        }
    }

    public function createClassess($program_id, $batch_name)
{
    $db = new Database();
    
    $faculty = $db->read('faculty', "WHERE id=" . $program_id);
    $batch = $db->read('batch', "WHERE batch_name='" . $batch_name . "'");
    
    if (!empty($faculty) && !empty($batch)) {
        $batchId = $batch['batch_id'];
        $enrollmentType = $faculty['enrollment_type'];
        $level = $faculty['level'];
        
        $classMapping = [
            'bachelor' => [
                'annual' => 4,
                'semester' => 8,
            ],
            'master' => [
                'annual' => 4,
                'semester' => 4,
            ],
            'mphil' => [
                'annual' => 2,
                'semester' => 2,
            ],
            'intermediate' => [
                'annual' => 2,
                'semester' => 2,
            ],
            'phd' => [
                'annual' => 2,
                'semester' => 2,
            ],
        ];
        
        // Check if the level exists in the class mapping
        if (array_key_exists($level, $classMapping)) {
            // Check if the enrollment type exists for the given level
            if (array_key_exists($enrollmentType, $classMapping[$level])) {
                $classesToCreate = $classMapping[$level][$enrollmentType];
                // Create classes based on the determined number
                for ($i = 1; $i <= $classesToCreate; $i++) {
                    $status = ($i == 1) ? '1' : '0';
                    $classType = ($enrollmentType == 'semester') ? 'semester' : 'year';
                    $className = $faculty['faculty_name'] . '-' . $batch_name . '-' . $i . '-' . $classType;
                    $createClassData = [
                        'name' => $className,
                        'display_name' => $className,
                        'batch_id' => $batchId,
                        'status' => $status,
                    ];
                    $db->create('classes', $createClassData);
                }
                
                return true;
            }
        }
    }
    
    return false; 
}

    
    

    public function batchTable()
    {
        $data = $_POST;
        $db = new Database();
        $sql = "select batch.*, faculty.faculty_name from faculty inner join batch on batch.faculty_id = faculty.id where id =" . $data['hash'];
        $readResult = $db->sql($sql);
        $this->tableView($data, $readResult);
    }
    public function tableView($data, $readResult)
    {
        if ($readResult) {
            echo '
            <div class="title">
                <h2>' . $readResult[0]["faculty_name"] . ' Batch</h2>
            </div>
            <table>
                <tr>
                    <th>Batch Name</th>
                    <th>Description</th>
                    <th>Option</th>
                </tr>';
            foreach ($readResult as $key) :
                echo '<tr>
                    <td>' . $key['batch_name'] . '</td>
                    <td>' . $key['description'] . '</td>
                    <td><a href="#batchEdit#' . $key['batch_id'] . '#' . $key['batch_name'] . '" class="btn">View</a></td>
                </tr>';
            endforeach;
            echo '
            </table>
        </div>';
        } else {
            echo '
            <div class="content-2 create-batch">
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h1>
                </table>
            </div>
        </div>
        </div>';
        }
    }
    public function editBatch()
    {
        // dd($_POST);
        $data = $_POST;
        $db = new Database();
        $id =  input_sanitize($_POST['hash']);
        if($id == 'undefined'){
            exit;
        }
        $readResult = $db->fetchById('batch', 'batch_id', $id);
        echo '
        <div class="title">
            <h2>' . $data['program'] . ' Batch Edit</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/BatchController.php?page=batchUpdate">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Name</label>
            </div>
            <input name="batch_id" type="hidden" value="' . $readResult['batch_id'] . '">
            <input name="faculty_id" type="hidden" value="' . $readResult['faculty_id'] . '">
            <input id="text" name="name" value="' . $readResult['batch_name'] . '" type="text" readonly>
        </div>
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="description">Description</label>
            </div>
            <input id="text" value="' . $readResult['description'] . '" name="description">
        </div>
        <button class="btn-green" type="submit">Update</button>
        </form>
        <form style="margin:10px" class="login-form" method="post" action="Controller/BatchController.php?page=batchDelete">
        <input name="id" type="hidden" value="' . $readResult['batch_id'] . '">
        <button class="btn-red" type="submit">Delete</button>
        </form>
    </div>';
    }
    public function deleteBatch()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and (!empty($_POST['id']))) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);
            $condition = "WHERE batch_id = '" . $id . "'";
            $db->delete('batch', $condition);
            $_SESSION['successMessage'] = 'Batch Deleted Successfully!';
            header("Location: ../batch.php");
            exit;
        } else {
            $_SESSION['errorMessage'] = 'Batch Deletion Failed!';
            header("Location: ../batch.php");
            exit;
        }
    }
    public function updateBatch()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST" and (!empty($_POST['name']) and !empty($_POST['description']) and !empty($_POST['batch_id']) and !empty($_POST['faculty_id']))) {
            $name = input_sanitize($_POST['name']);
            $description = input_sanitize($_POST['description']);
            $id = input_sanitize($_POST['batch_id']);
            $faculty_id = input_sanitize($_POST['faculty_id']);
            
            $db = new Database();
            
            $condition = "WHERE batch_name = '" . $name . "'";
            $existingBatch = $db->read('batch', $condition);
            
            if (!empty($existingBatch) && $existingBatch['batch_id'] != $id) {
                $_SESSION['errorMessage'] = 'Batch Name Already Exists, Please Choose Another Name!';
                header("Location: ../batch.php");
                exit();
            }
            
            $updateData = [
                'batch_name' => $name,
                'description' => $description,
            ];
            
            if ($name === $readResult['batch_name']) {
                unset($updateData['batch_name']);
            }
            
            $condition = "WHERE batch_id = '" . $id . "' AND faculty_id = '" . $faculty_id . "'";
            
            $updateResult = $db->update('batch', $updateData, $condition);
            
            if ($updateResult) {
                $_SESSION['successMessage'] = 'Batch Updated Successfully!';
                header("Location: ../batch.php");
                exit();
            } else {
                $_SESSION['errorMessage'] = 'Failed to Update Batch. Please Try Again!';
                header("Location: ../batch.php");
                exit();
            }
        }
    }
    
}
new BatchController();
