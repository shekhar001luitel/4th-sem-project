<?php
// session_start();

include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";

class LoginController extends Controller
{
    public function __construct()
    {
        if ($_GET['page'] == 'undefined') {
            exit;
        }
        if ($_GET['page'] === 'register' && $_SESSION['role'] === 'admin') {
            $this->register();
        } elseif ($_GET['page'] === 'login') {
            $this->login();
        } elseif ($_POST['page'] === 'logout') {
            $this->logout();
        }
    }

    public function login()
    {
        $Database = new Database();

        if ($_SERVER["REQUEST_METHOD"] == "POST" and (!empty($_POST['email']) and !empty($_POST['password']))) {
            $email = input_sanitize($_POST["email"]);
            // if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //     $_SESSION['errorMessage'] = 'Valid Email!';
            //     header("Location: ../login.php");
            //     exit;
            // }

            $password = input_sanitize($_POST['password']);
            $userType = $_GET['type'];
            $tableName = $this->getTableName($userType);

            if (!$tableName) {
                $_SESSION['errorMessage'] = 'Invalid user type';
                header("Location: ../login.php");
                exit;
            }

            if($userType === 'admin'){
                $condition = "WHERE email = '" . $email . "'";
            }else{
                $condition = "WHERE id = '" . $email . "'";
            }
            $readResult = $Database->read($tableName, $condition);

            if (!empty($readResult)) {
                $hashedPasswordFromDatabase = $readResult['password'];
                if (password_verify($password, $hashedPasswordFromDatabase)) {
                    // Authentication successful
                    $_SESSION['user_id'] = $readResult['id'];
                    $_SESSION['role'] = $userType;

                    // Redirect based on user type
                    switch ($userType) {
                        case 'student':
                            $this->fetchStudentDetails($readResult['id']);
                            break;
                        case 'teacher':
                            $this->fetchTeacherDetails($readResult['id']);
                            break;
                        case 'admin':
                            header("Location: ../dashboard.php"); // Direct admin to dashboard
                            exit;
                        default:
                            // Handle unexpected types
                            break;
                    }
                } else {
                    // Incorrect Password
                    $_SESSION['errorMessage'] = 'Incorrect Password';
                    header("Location: ../login.php");
                    exit;
                }
            } else {
                // Incorrect Email
                $_SESSION['errorMessage'] = 'Incorrect Email';
                header("Location: ../login.php");
                exit;
            }
        } else {
            $_SESSION['errorMessage'] = 'Login Failed!';
            header("Location: ../login.php");
            exit;
        }
    }

    // Method to determine appropriate table name based on user type
    private function getTableName($userType)
    {
        switch ($userType) {
            case 'student':
                return 'student_details';
            case 'teacher':
                return 'staff_details';
            case 'admin':
                return 'users';
            default:
                return false;
        }
    }

    // Method to fetch student details
    private function fetchStudentDetails($userId)
    {
        $Database = new Database();
        $condition = "WHERE id = '" . $userId . "'";
        $studentDetails = $Database->read('student_details', $condition);

        if (!empty($studentDetails)) {
            // Redirect to student dashboard or profile page
            $_SESSION['successMessage'] = 'Login successful';
            header("Location: ../dashboard.php");
        } else {
            $_SESSION['errorMessage'] = 'Student details not found';
            header("Location: ../login.php");
        }
        exit;
    }

    // Method to fetch teacher details
    private function fetchTeacherDetails($userId)
    {
        $Database = new Database();
        $condition = "WHERE id = '" . $userId . "'";
        $teacherDetails = $Database->read('staff_details', $condition); // Assuming staff_details for teacher

        if (!empty($teacherDetails)) {
            // Redirect to teacher dashboard or profile page
            $_SESSION['successMessage'] = 'Login successful';
            header("Location: ../dashboard.php");
        } else {
            $_SESSION['errorMessage'] = 'Teacher details not found';
            header("Location: ../login.php");
        }
        exit;
    }

    public function register()
    {
        $Database = new Database();

        if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['email']) && !empty($_POST['password'])) {
            $email = input_sanitize($_POST["email"]);
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $_SESSION['errorMessage'] = 'Valid Email!';
                header("Location: ../login.php");
            }
            $password = input_sanitize($_POST['password']);

            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
            $condition = "WHERE email = '" . $email . "'";
            $readResult = $Database->read('users', $condition);

            if (empty($readResult)) {
                $createData = [
                    'email' => $email,
                    'password' => $hashedPassword,
                    'role' => 'non-admin',
                ];
                $createResult = $Database->create('users', $createData);
                if (!empty($createResult)) {
                    // Registration success
                    $_SESSION['successMessage'] = 'User registered successfully';
                    header("Location: ../login.php");
                } else {
                    $_SESSION['errorMessage'] = 'Registration failed. Please try again.';
                    header("Location: ../register.php");
                }
            } else {
                $_SESSION['errorMessage'] = 'Invalid Email.';

                header("Location: ../register.php");
            }
        } else {
            $_SESSION['errorMessage'] = 'Registration failed. Please try again.';

            header("Location: ../register.php");
        }
    }
}

new LoginController();
