<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";
class ClassessController extends Controller
{
    public function __construct()
    {

        // dd($_GET);
        if ($_GET['page'] == 'undefined') {
            exit;
        }

        if ($_GET['page'] === 'viewClass') { // Read or list
            $this->classTable();
        } elseif ($_GET['page'] === 'ClassessEdit') { // update 
            $this->editBatch();
        } elseif ($_GET['page'] === 'classStatus') { //UPDATE
            $this->classStatus();
        } elseif ($_GET['page'] === 'classUpdate') { //UPDATE
            $this->classUpdate();
        }
    }



    public function classTable()
    {
        $data = $_POST;
        $db = new Database();
        $sql = "select batch.*, faculty.faculty_name from faculty inner join batch on batch.faculty_id = faculty.id where id =" . $data['hash'];
        $condition = "where batch_id =" . $data['hash'];
        $readResult = $db->readAll('classes', $condition);
        $this->tableView($data, $readResult);
    }
    public function tableView($data, $readResult)
    {
        if ($readResult) {
            echo '
            <div class="title">
                <h2>Class</h2>
            </div>
            <table>
                <tr>
                    <th>Class Name</th>
                    <th>Option</th>
                </tr>';
            foreach ($readResult as $key) :
                echo '<tr style="';
                if ($key['status']) {
                    echo 'background-color: #b5b5b5;';
                }
                echo '">
                    <td>' . $key['display_name'] . '</td>
                    <td><a href="#classEdit#' . $key['id'] . '" class="btn">View</a></td>
                </tr>';
            endforeach;
            echo '
            </table>
        </div>';
        } else {
            echo '
            <div class="content-2 create-batch">
            <div class="recent-payments">
                <div class="title">
                    <h2>Batch</h2>
                </div>
                <table>
                    <h3 style="text-align: center; margin-top: 15%;">No Program Selected</h1>
                </table>
            </div>
        </div>
        </div>';
        }
    }
    public function editBatch()
    {
        $data = $_POST;
        $db = new Database();
        $id = input_sanitize($_POST['hash']);
        if ($id == 'undefined') {
            exit; // This might need to be handled differently based on your application logic
        }
        $readResult = $db->fetchById('classes', 'id', $id);

        echo '
    <div class="title">
        <h2>Class Edit (' . $readResult['name'] . ')</h2>
    </div>
    <form style="margin:10px" class="login-form" method="post" action="Controller/ClassessController.php?page=classUpdate">
        <input name="class_id" type="hidden" value="' . $readResult['id'] . '">
        <div>
            <div style="display: flex; justify-content: space-between;">
                <label for="name">Display Name</label>
            </div>
            <input id="name" name="name" value="' . $readResult['display_name'] . '" type="text">
        </div>
        <button class="btn-green" type="submit">Update</button>
    </form>
    <form style="margin:10px" class="login-form" method="post" action="Controller/ClassessController.php?page=classStatus">
        <input name="id" type="hidden" value="' . $readResult['id'] . '">
        <button class="btn-' . ($readResult['status'] ? 'green' : 'red') . '" type="submit">' . ($readResult['status'] ? 'ACTIVE' : 'INACTIVE') . '</button>
    </form>
    </div>';
    }

    public function classStatus()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && !empty($_POST['id'])) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);

            // Fetch the current status of the class
            $conditions = 'where id = '.$id;
            $currentStatus = $db->read('classes',$conditions);

            // Determine the new status based on the current status
            $newStatus = ($currentStatus['status'] == 1) ? 0 : 1;
            // Update the status in the database
            $updateData = ['status' => $newStatus];
            $condition = "where id = '" . $id . "'";
            $updateResult = $db->update('classes', $updateData, $condition);

            if ($updateResult) {
                $_SESSION['successMessage'] = 'Class Status Updated Successfully!';
            } else {
                $_SESSION['errorMessage'] = 'Failed to update class status.';
            }
        } else {
            $_SESSION['errorMessage'] = 'Invalid request to update class status.';
        }
        header("Location: ../classes.php");
        exit;
    }


    public function classUpdate()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" && !empty($_POST['name']) && !empty($_POST['class_id'])) {
            $name = input_sanitize($_POST['name']);
            $id = input_sanitize($_POST['class_id']);
            $db = new Database();

            // Check if the updated name already exists
            $existingCondition = "WHERE display_name = '" . $name . "' AND id != '" . $id . "'";
            $existingResult = $db->read('classes', $existingCondition);

            if (!empty($existingResult)) {
                $_SESSION['errorMessage'] = 'Class Name already exists. Please use a different name.';
                header("Location: ../batch.php");
                exit();
            }

            $updateData = [
                'display_name' => $name,
            ];

            $condition = "WHERE id = '" . $id . "'";
            $updateResult = $db->update('classes', $updateData, $condition);

            if ($updateResult) {
                $_SESSION['successMessage'] = 'Class Updated Successfully!';
            } else {
                $_SESSION['errorMessage'] = 'Failed to update class.';
            }
        } else {
            $_SESSION['errorMessage'] = 'Invalid data submitted for class update.';
        }
        header("Location: ../classes.php");
        exit();
    }
}
new ClassessController();
