<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";
class SubjectController extends Controller
{
    public function __construct()
    {

        // dd($_GET);
        if ($_GET['page'] == 'undefined') {
            exit;
        }

        if ($_GET['page'] == 'createSubject') { // Read or list
            $this->createSubject();
        } elseif ($_GET['page'] === 'subjectAdd') {
            $this->subjectAdd();
        }elseif ($_GET['page'] === 'editSubject') {
            $this->editSubject();
        }elseif ($_GET['page'] === 'updateSubject') {
            $this->updateSubject();
        }elseif ($_GET['page'] === 'deleteSubject') {
            $this->deleteSubject();
        }elseif ($_GET['page'] === 'subjectClass') {
            $this->subjectClass();
        }elseif ($_GET['page'] === 'classSectionSubjectRelation') {
            $this->classSectionSubjectRelation();
        }
    }



    public function createSubject()
    {
        echo '
        <div class="title">
            <h2>Add Subject</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SubjectController.php?page=subjectAdd">
            <div>
                <div style="display: flex; justify-content: space-between; ">
                    <label for="name">Name</label>
                </div>
                <input id="text" name="name" type="text">
            </div>
            <button class="btn-green" type="submit">Save</button>
        </form>
    </div>';
    }
    public function subjectAdd()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Check if the name field is set and not empty
            if (isset($_POST["name"]) && !empty($_POST["name"])) {
                // Get the name from the form
                $name = $_POST["name"];

                $db = new Database();
                $condition = "WHERE 'subject_name='" . $name . "'";
                $readResult = $db->read('subject', $condition);

                if (!empty($readResult)) {
                    $_SESSION['errorMessage'] = 'Subject Name Already Exists!';
                    header("Location: ../subject.php");
                    exit();
                }

                $createData = [
                    'subject_name' => $name,
                ];

                $createResult = $db->create('subject', $createData);

                if (!empty($createResult)) {
                    $_SESSION["successMessage"] = "Subject added successfully!";
                    header("Location: ../subject.php");
                    exit();
                } else {
                    // If name field is not set or empty, display error message
                    $_SESSION["errorMessage"] = "Name field is required!";
                    header("Location: ../subject.php");
                    exit();
                }
            }
        }
    }
    public function editSubject()
    {
        // dd($_POST);
        $data = $_POST;
        $db = new Database();
        $id =  input_sanitize($_POST['hash']);
        if($id == 'undefined'){
            exit;
        }
        $readResult = $db->fetchById('subject', 'subject_id', $id);
        echo '
        <div class="title">
            <h2>Subject Edit</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SubjectController.php?page=updateSubject">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Name</label>
            </div>
            <input name="subject_id" type="hidden" value="' . $readResult['subject_id'] . '">
            <input id="text" name="name" value="' . $readResult['subject_name'] . '" type="text">
        </div>
            <button class="btn-green" type="submit">Update</button>
        </form>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SubjectController.php?page=deleteSubject">
        <input name="id" type="hidden" value="' . $readResult['subject_id'] . '">
        <button class="btn-red" type="submit">Delete</button>
        </form>
    </div>';
    }
    public function updateSubject()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Check if the name field is set and not empty
            if (isset($_POST["name"]) && !empty($_POST["name"]) && isset($_POST["subject_id"]) && !empty($_POST["subject_id"])) {
                // Get the name and subject id from the form
                $name = $_POST["name"];
                $subject_id = $_POST["subject_id"];
    
                $db = new Database();
    
                // Check if the new name is already associated with another subject
                $condition = "WHERE subject_name='" . $name . "' AND subject_id != '" . $subject_id . "'";
                $readResult = $db->read('subject', $condition);
    
                if (!empty($readResult)) {
                    $_SESSION['errorMessage'] = 'Subject Name Already Exists!';
                    header("Location: ../subject.php");
                    exit();
                }
    
                // Update the subject name in the database
                $updateData = [
                    'subject_name' => $name,
                ];
                $updateCondition = "where subject_id='" . $subject_id . "'";
                $updateResult = $db->update('subject', $updateData, $updateCondition);
    
                if (!empty($updateResult)) {
                    $_SESSION["successMessage"] = "Subject updated successfully!";
                    header("Location: ../subject.php");
                    exit();
                } else {
                    // If update failed, display error message
                    $_SESSION["errorMessage"] = "Failed to update subject!";
                    header("Location: ../subject.php");
                    exit();
                }
            } else {
                // If name or subject_id field is not set or empty, display error message
                $_SESSION["errorMessage"] = "Name and subject_id fields are required!";
                header("Location: ../subject.php");
                exit();
            }
        }
    }
    
    public function deleteSubject()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and (!empty($_POST['id']))) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);
            $condition = "WHERE subject_id = '" . $id . "'";
            $db->delete('subject', $condition);
            $_SESSION['successMessage'] = 'Subject Deleted Successfully!';
            header("Location: ../subject.php");
            exit;
        } else {
            $_SESSION['errorMessage'] = 'Subject Deletion Failed!';
            header("Location: ../subject.php");
            exit;
        }
    }
    
    public function subjectClass(){
        $db = new Database();
    
        // Fetch all sections
        $sections_sql = "SELECT * FROM section";
        $sections = $db->sql($sections_sql);
    
        // Display classes for each section
        foreach ($sections as $section) {
            echo '
            <div class="title">
                <h2>Section ' . $section['section_name'] . '</h2>
            </div>
            <table>
                <tr>
                    <th>Class Name</th>
                    <th>Section</th>
                    <th>Option</th>
                </tr>';
    
            // Fetch classes associated with the current section
            $classes_sql = "SELECT c.id AS class_id, c.name AS class_name, c.display_name AS class_display_name, 
                                   b.batch_id, b.batch_name, b.description AS batch_description, 
                                   f.id AS faculty_id, f.faculty_name, f.description AS faculty_description, 
                                   f.level AS faculty_level, f.affiliate_university, f.enrollment_type
                            FROM classes c
                            JOIN batch b ON c.batch_id = b.batch_id
                            JOIN faculty f ON b.faculty_id = f.id
                            LEFT JOIN class_section cs ON c.id = cs.class_id
                            WHERE cs.section_id = " . $section['section_id'];
    
            $classes = $db->sql($classes_sql);
    
            // Display class information
            foreach ($classes as $class) {
                echo '<tr style="';
                if ($class['status']) {
                    echo 'background-color: #b5b5b5;';
                }
                echo '">
                    <td>' . $class['class_display_name'] . '</td>
                    <td>' . $section['section_name'] . '</td>';
    
                // Check if the subject is already associated with this section
                $subject_id = input_sanitize($_POST['hash']);
                $check_subject_sql = "SELECT * FROM section_subject WHERE section_id = " . $section['section_id'] . " AND subject_id = " . $subject_id;
                $check_subject_result = $db->sql($check_subject_sql);
    
                // Display button with appropriate styling and text based on subject association
                echo '
                    <td>';
                if (!empty($check_subject_result)) {
                    // If subject is already associated, display button as added with red color
                    echo '<button class="btn-red" disabled>Added</button>';
                } else {
                    // If subject is not associated, display button as green
                    echo '
                        <form style="margin:10px" class="login-form" method="post" action="Controller/SubjectController.php?page=classSectionSubjectRelation">
                            <input name="subject_id" type="hidden" value="' . $subject_id . '">
                            <input name="section_id" type="hidden" value="' . $section['section_id'] . '">
                            <button class="btn-green" type="submit">Add</button>
                        </form>';
                }
                echo '</td>
                ';
    
                echo '</tr>';
            }
    
            echo '</table>
            </div>';
        }
    }
    
    

    public function classSectionSubjectRelation(){
        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['section_id']) && isset($_POST['subject_id'])) {
            // Sanitize input
            $class_id = input_sanitize($_POST['class_id']);
            $section_id = input_sanitize($_POST['section_id']);
            $subject_id = input_sanitize($_POST['subject_id']);
    
            // Check if the class ID, section ID, and subject ID are valid numeric values
            if (!is_numeric($section_id) || !is_numeric($subject_id)) {
                // Redirect with error message if any ID is invalid
                $_SESSION['errorMessage'] = 'Invalid section ID, or subject ID';
                header("Location: ../subject.php");
                exit();
            }
    
            // Check if the class, section, and subject exist in the database
            $db = new Database();
            $section_check_sql = "SELECT * FROM section WHERE section_id = $section_id";
            $section_check_result = $db->sql($section_check_sql);
            if (empty($section_check_result)) {
                // Redirect with error message if section does not exist
                $_SESSION['errorMessage'] = 'Section does not exist';
                header("Location: ../subject.php");
                exit();
            }
    
            $subject_check_sql = "SELECT * FROM subject WHERE subject_id = $subject_id";
            $subject_check_result = $db->sql($subject_check_sql);
            if (empty($subject_check_result)) {
                // Redirect with error message if subject does not exist
                $_SESSION['errorMessage'] = 'Subject does not exist';
                header("Location: ../subject.php");
                exit();
            }
    
            // Associate section with class and subject by inserting into section_subject table
            $data = [
                'section_id' => $section_id,
                'subject_id' => $subject_id
            ];
            $createResult = $db->create('section_subject', $data);
    
            if ($createResult) {
                // Redirect with success message if association is successful
                $_SESSION['successMessage'] = 'Subject associated with section successfully';
                header("Location: /subject.php");
                exit();
            } else {
                // Redirect with error message if association fails
                $_SESSION['errorMessage'] = 'Failed to associate subject with section';
                header("Location: ../subject.php");
                exit();
            }
        } else {
            // Redirect with error message if class ID, section ID, or subject ID is not provided
            $_SESSION['errorMessage'] = 'Class ID, section ID, or subject ID not provided';
            header("Location: ../subject.php");
            exit();
        }
    }
     
    
}
new SubjectController();
