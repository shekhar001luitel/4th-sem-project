<?php
session_start();
class Controller
{
    public function __construct(){
        $this->initialize();
        // $this->check_user_role('admin');
        // date_default_timezone_set('Asia/Kathmandu');
    }

    public function logout()
    {
        session_destroy();
        header("Location: ../login.php");
    }
    public function check_user_role(...$allowed_roles)
    {
        $user_role = $this->user_role();

        if (!in_array($user_role, $allowed_roles)) {
            $this->logout();
        }
    }

    public function user_role()
    {
        $db = new Database();
        $user_id = $_SESSION['user_id'];
        $user_role = $db->checkRole($user_id);
        return $user_role;
    }

    public function return_dashboard()
    {
        $session = isset($_SESSION['user_id']);
        if ($session) {

            header('Location: dashboard.php');
        }   
    }

    public static $user_id;

    public static function initialize() {
        self::$user_id = $_SESSION['user_id'] ?? null;
    }
    public function check_login()
    {
        if (!$_SESSION['user_id']) {
            header('Location: login.php');
        }
    }
    public function User_Detail()
    {
        $db = new Database();
        $id = $_SESSION['user_id'];
        $condition = "WHERE id = '" . $id . "'";
        $data = $db->read('users', $condition);
        return $data;
    }
    public function Staff_Details()
    {
        $db = new Database();
        $id = $_SESSION['user_id'];
        $condition = "WHERE id = '" . $id . "'";
        $data = $db->read('staff_details', $condition);
        return $data;
    }
    public function Student_Detail()
    {
        $db = new Database();
        $id = $_SESSION['user_id'];
        $condition = "WHERE id = '" . $id . "'";
        $data = $db->read('student_details', $condition);
        return $data;
    }
    
}
// this is for role check 
