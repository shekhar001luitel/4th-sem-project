<?php
// session_start();

include "../helper.php";
include "Controller.php";
include "../Model/Database.php";

class StaffAttendanceController
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Kathmandu');

        if ($_GET['page'] === 'assignClass') {
            $this->assignClass();
        } elseif ($_GET['page'] === 'assignedClassList') {
            $this->assignedClassList();
        }
    }

    public function assignClass()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $staffId = $_POST['staff_id'];
            $classId = $_POST['class'];
            $sectionId = $_POST['section'];
            $attendanceDate = date('Y-m-d'); // Get current date

            // Validate input
            if (empty($staffId) || empty($classId) || empty($sectionId)) {
                $_SESSION['errorMessage'] = 'Please select a valid teacher and class!';
                header("Location: ../attendance-staff.php");
                exit();
            }

            $db = new Database();

            // Check if the staff member exists
            $staffExists = $db->get('staff_details', ['id' => $staffId]);
            if (!$staffExists) {
                $_SESSION['errorMessage'] = 'Selected teacher does not exist!';
                header("Location: ../attendance-staff.php");
                exit();
            }

            // Check if the class exists
            $classExists = $db->get('classes', ['id' => $classId]);
            if (!$classExists) {
                $_SESSION['errorMessage'] = 'Selected class does not exist!';
                header("Location: ../attendance-staff.php");
                exit();
            }

            // Check if the section exists
            $sectionExists = $db->get('section', ['section_id' => $sectionId]);
            if (!$sectionExists) {
                $_SESSION['errorMessage'] = 'Selected section does not exist!';
                header("Location: ../attendance-staff.php");
                exit();
            }

            // Check if the teacher has already been marked present today
            $existingRecord = $db->get('staff_attendance', [
                'staff_id' => $staffId,
                'attendance_date' => $attendanceDate
            ]);

            // If no existing record for today, set first_in to 1
            $firstIn = $existingRecord ? 0 : 1;

            // Insert staff attendance
            $insertData = [
                'staff_id' => $staffId,
                'class_id' => $classId,
                'section_id' => $sectionId,
                'attendance_date' => $attendanceDate,
                'first_in' => $firstIn
            ];

            $insertResult = $db->create('staff_attendance', $insertData);
            if ($insertResult) {
                $_SESSION['successMessage'] = 'Teacher assigned to class successfully!';
                header("Location: ../attendance-staff.php");
                exit();
            } else {
                $_SESSION['errorMessage'] = 'Failed to assign teacher to class. Please try again!';
                header("Location: ../attendance-staff.php");
                exit();
            }
        } else {
            $_SESSION['errorMessage'] = 'Invalid request method!';
            header("Location: ../attendance-staff.php");
            exit();
        }
    }

    public function assignedClassList()
    {
        $staffId = $_POST['staff_id'];

        // Validate input
        if (empty($staffId)) {
            echo json_encode(['error' => 'Invalid request! Staff ID is required.']);
            exit();
        }

        $db = new Database();

        $query = "
        SELECT 
            sa.id AS attendance_id,
            c.display_name AS class_name,
            s.section_name,
            sa.attendance_date,
            sa.first_in
        FROM 
            staff_attendance sa
            INNER JOIN classes c ON sa.class_id = c.id
            INNER JOIN section s ON sa.section_id = s.section_id
        WHERE 
            sa.staff_id = ".$staffId."
            AND DATE(sa.attendance_date) = '".date('Y-m-d')."'
        ORDER BY 
            sa.attendance_date DESC"
        ;

        // Parameter array for binding
        try {
        // Execute the SQL query with parameter binding
        $assignedClasses = $db->sql($query);

        // Generate the HTML for the table
        if (empty($assignedClasses)) {
            $tableHtml = '<h3 style="text-align: center; width: 100%;">No Class Assigned</h3>';
        } else {
            $tableHtml = '<table style="width: 100%;"><tr><th>Class</th><th>Section</th><th>Date</th><th>First In</th></tr>';
            foreach ($assignedClasses as $row) {
                $tableHtml .= '<tr>
                    <td>' . htmlspecialchars($row['class_name']) . '</td>
                    <td>' . htmlspecialchars($row['section_name']) . '</td>
                    <td>' . htmlspecialchars($row['attendance_date']) . '</td>
                    <td>' . ($row['first_in'] ? 'Yes' : 'No') . '</td>
                </tr>';
            }
            $tableHtml .= '</table>';
        }

        echo ($tableHtml);

        } catch (Exception $e) {
        echo json_encode(['error' => 'An error occurred while fetching the assigned classes.']);
        }
    }

}

new StaffAttendanceController();
?>
