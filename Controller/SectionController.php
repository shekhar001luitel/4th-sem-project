<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";
class SectionController extends Controller
{
    public function __construct()
    {

        // dd($_GET);
        if ($_GET['page'] == 'undefined') {
            exit;
        }

        if ($_GET['page'] == 'createSection') { // Read or list
            $this->createSection();
        } elseif ($_GET['page'] === 'SectionAdd') {
            $this->sectionAdd();
        }elseif ($_GET['page'] === 'editSection') {
            $this->editSection();
        }elseif ($_GET['page'] === 'updateSection') {
            $this->updateSection();
        }elseif ($_GET['page'] === 'deleteSection') {
            $this->deleteSection();
        }elseif ($_GET['page'] === 'sectionClass') {
            $this->sectionClass();
        }elseif ($_GET['page'] === 'classSectionRelation') {
            $this->classSectionRelation();
        }
    }



    public function createSection()
    {
        echo '
        <div class="title">
            <h2>Add Section</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SectionController.php?page=SectionAdd">
            <div>
                <div style="display: flex; justify-content: space-between; ">
                    <label for="name">Name</label>
                </div>
                <input id="text" name="name" type="text">
            </div>
            <button class="btn-green" type="submit">Save</button>
        </form>
    </div>';
    }
    public function sectionAdd()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Check if the name field is set and not empty
            if (isset($_POST["name"]) && !empty($_POST["name"])) {
                // Get the name from the form
                $name = $_POST["name"];

                $db = new Database();
                $condition = "WHERE 'section_name='" . $name . "'";
                $readResult = $db->read('sectioin', $condition);

                if (!empty($readResult)) {
                    $_SESSION['errorMessage'] = 'Section Name Already Exists!';
                    header("Location: ../section.php");
                    exit();
                }

                $createData = [
                    'section_name' => $name,
                ];

                $createResult = $db->create('section', $createData);

                if (!empty($createResult)) {
                    $_SESSION["successMessage"] = "Section added successfully!";
                    header("Location: ../section.php");
                    exit();
                } else {
                    // If name field is not set or empty, display error message
                    $_SESSION["errorMessage"] = "Name field is required!";
                    header("Location: ../section.php");
                    exit();
                }
            }
        }
    }
    public function editSection()
    {
        // dd($_POST);
        $data = $_POST;
        $db = new Database();
        $id =  input_sanitize($_POST['hash']);
        if($id == 'undefined'){
            exit;
        }
        $readResult = $db->fetchById('section', 'section_id', $id);
        echo '
        <div class="title">
            <h2>Section Edit</h2>
        </div>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SectionController.php?page=updateSection">
        <div>
            <div style="display: flex; justify-content: space-between; ">
                <label for="name">Name</label>
            </div>
            <input name="section_id" type="hidden" value="' . $readResult['section_id'] . '">
            <input id="text" name="name" value="' . $readResult['section_name'] . '" type="text">
        </div>
            <button class="btn-green" type="submit">Update</button>
        </form>
        <form style="margin:10px" class="login-form" method="post" action="Controller/SectionController.php?page=deleteSection">
        <input name="id" type="hidden" value="' . $readResult['section_id'] . '">
        <button class="btn-red" type="submit">Delete</button>
        </form>
    </div>';
    }
    public function updateSection()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Check if the name field is set and not empty
            if (isset($_POST["name"]) && !empty($_POST["name"]) && isset($_POST["section_id"]) && !empty($_POST["section_id"])) {
                // Get the name and section id from the form
                $name = $_POST["name"];
                $section_id = $_POST["section_id"];
    
                $db = new Database();
    
                // Check if the new name is already associated with another section
                $condition = "WHERE section_name='" . $name . "' AND section_id != '" . $section_id . "'";
                $readResult = $db->read('section', $condition);
    
                if (!empty($readResult)) {
                    $_SESSION['errorMessage'] = 'Section Name Already Exists!';
                    header("Location: ../section.php");
                    exit();
                }
    
                // Update the section name in the database
                $updateData = [
                    'section_name' => $name,
                ];
                $updateCondition = "where section_id='" . $section_id . "'";
                $updateResult = $db->update('section', $updateData, $updateCondition);
    
                if (!empty($updateResult)) {
                    $_SESSION["successMessage"] = "Section updated successfully!";
                    header("Location: ../section.php");
                    exit();
                } else {
                    // If update failed, display error message
                    $_SESSION["errorMessage"] = "Failed to update section!";
                    header("Location: ../section.php");
                    exit();
                }
            } else {
                // If name or section_id field is not set or empty, display error message
                $_SESSION["errorMessage"] = "Name and section_id fields are required!";
                header("Location: ../section.php");
                exit();
            }
        }
    }
    
    public function deleteSection()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST" and (!empty($_POST['id']))) {
            $db = new Database();
            $id = input_sanitize($_POST['id']);
            $condition = "WHERE section_id = '" . $id . "'";
            $db->delete('section', $condition);
            $_SESSION['successMessage'] = 'Section Deleted Successfully!';
            header("Location: ../section.php");
            exit;
        } else {
            $_SESSION['errorMessage'] = 'Section Deletion Failed!';
            header("Location: ../section.php");
            exit;
        }
    }
    public function sectionClass(){
        // dd($_POST);
        $data = $_POST;
        $db = new Database();
        $id =  input_sanitize($_POST['hash']);
        if($id == 'undefined'){
            exit;
        }
        $sql = "SELECT * FROM classes";
        $readResult = $db->sql($sql);
    
        echo '
        <div class="title">
            <h2>Class</h2>
        </div>
        <table>
            <tr>
                <th>Class Name</th>
                <th>Option</th>
            </tr>';
    
        foreach ($readResult as $key) {
            echo '<tr style="';
            if ($key['status']) {
                echo 'background-color: #b5b5b5;';
            }
            echo '">
                <td>' . $key['display_name'] . '</td>';
            
            // Check if the class has a section associated with it
            $sectionExistSql = "SELECT COUNT(*) AS count FROM class_section WHERE `class_id` = " . $key['id']. " AND `section_id` = " . $id."";
            $sectionExist = $db->sql($sectionExistSql);
            $sectionExist = $sectionExist[0]['count'];
    
            // Display button with appropriate styling and text
            if ($sectionExist > 0) {
                echo '
                <td>
                <form style="margin:10px" class="login-form" method="post" action="Controller/SectionController.php?page=classSectionRelation">
                    <input name="class_id" type="hidden" value="' . $key['id'] . '">
                    <input name="section_id" type="hidden" value="' . $id . '">
                    <button class="btn-red" type="submit">Added</button>
                </form>
                </td>
                ';
            } else {
                echo '
                <td>
                <form style="margin:10px" class="login-form" method="post" action="Controller/SectionController.php?page=classSectionRelation">
                    <input name="class_id" type="hidden" value="' . $key['id'] . '">
                    <input name="section_id" type="hidden" value="' . $id . '">
                    <button class="btn-green" type="submit">Add</button>
                </form>
                </td>
            ';
            }
            
            echo '</tr>';
        }
    
        echo '</table>
        </div>';
    }
    

    public function classSectionRelation(){
        if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['class_id']) && isset($_POST['section_id'])) {
            // Sanitize input
            $class_id = input_sanitize($_POST['class_id']);
            $section_id = input_sanitize($_POST['section_id']);
    
            // Check if the class ID and section ID are valid numeric values
            if (!is_numeric($class_id) || !is_numeric($section_id)) {
                // Redirect with error message if class ID or section ID is invalid
                $_SESSION['errorMessage'] = 'Invalid class ID or section ID';
                header("Location: ../section.php");
                exit();
            }
    
            // Check if the class exists in the database
            $db = new Database();
            $class_check_sql = "SELECT * FROM classes WHERE id = $class_id";
            $class_check_result = $db->sql($class_check_sql);
            if (empty($class_check_result)) {
                // Redirect with error message if class does not exist
                $_SESSION['errorMessage'] = 'Class does not exist';
                header("Location: ../section.php");
                exit();
            }
    
            // Check if the section exists in the database
            $section_check_sql = "SELECT * FROM section WHERE section_id = $section_id";
            $section_check_result = $db->sql($section_check_sql);
            if (empty($section_check_result)) {
                // Redirect with error message if section does not exist
                $_SESSION['errorMessage'] = 'Section does not exist';
                header("Location: ../section.php");
                exit();
            }
    
            // Associate section with class by inserting into class_section table
            $data = [
                'class_id' => $class_id,
                'section_id' => $section_id
            ];
            $createResult = $db->create('class_section', $data);
    
            if ($createResult) {
                // Redirect with success message if association is successful
                $_SESSION['successMessage'] = 'Section associated with class successfully';
                header("Location: /section.php");
                exit();
            } else {
                // Redirect with error message if association fails
                $db = new Database();
                $id = input_sanitize($_POST['id']);
                $condition = "WHERE class_id = '" . $class_id . "' and section_id = '" .$section_id."'";
                $db->delete('class_section', $condition);
                $_SESSION['errorMessage'] = 'Section unassociated with class successfully';
                header("Location: ../section.php");
                exit();
            }
        } else {
            // Redirect with error message if class ID or section ID is not provided
            $_SESSION['errorMessage'] = 'Class ID or section ID not provided';
            header("Location: ../section.php");
            exit();
        }
    }    
    
}
new SectionController();
