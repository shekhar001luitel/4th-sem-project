<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";
class StudentAttendanceController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Kathmandu');
        // dd($_GET);
        if ($_GET['page'] == 'undefined') {
            exit;
        }

        if ($_GET['page'] == 'assignedStudent') { // Read or list
            $this->assignedStudent();
        }elseif ($_GET['page'] === 'selectedClassStudent') {
            $this->selectedClassStudent();
        }elseif ($_GET['page'] === 'studentAttendenceData') {
            $this->studentAttendenceData();
        }
    }



    public function assignedStudent()
{
    $db = new Database();
    $subjects = $db->readAll('subject'); // Fetch subjects from the database
    echo '
    <div class="title" style="padding: 20px;">
        <h2>Add Section</h2>
    </div>
    <div style="padding: 20px;">
        <div style="display: flex; justify-content: space-between; margin-top: 20px;">
            <label for="subject">Subject</label>
        </div>
        <select id="subject" name="subject" onChange="showSection(this.value)">
            <option value="">Select Subject</option>';

            // Populate the select options with subjects
            foreach ($subjects as $subject) {
                echo '<option value="' . $subject['subject_id'] . '">' . $subject['subject_name'] . '</option>';
            }

    echo '
        </select>
    </div>';
}
    public function selectedClassStudent()
    {
        $data = $_POST;
        $db = new Database();
        $classId = input_sanitize($_POST['class']);
        $sectionId = input_sanitize($_POST['section']);
        
        if ($classId == 'undefined' || $sectionId == 'undefined') {
            exit;
        }

        $sql = "SELECT sd.id AS student_id, sd.name AS student_name, sd.student_image_path AS student_image, sd.name_in_nepali, sd.gender, sd.dob_bs, f.faculty_name, b.batch_name, c.name AS class_name, c.id AS class_id, s.section_name, s.section_id AS section_id, sd.phone, sd.nationality, sd.permanent_province, sd.permanent_district, sd.permanent_municipality, sd.permanent_address, sd.father_name, sd.father_image_path, sd.f_occupation, sd.f_cell, sd.f_email, sd.f_office, sd.mother_name, sd.mother_image_path, sd.m_occupation, sd.m_cell, sd.m_email, sd.m_office, sd.guardian_name, sd.guardian_image_path, sd.g_relation, sd.g_cell, sd.religion, sd.cast, sd.citizenship, sd.handicapped, sd.password 
                FROM student_details sd 
                LEFT JOIN faculty f ON sd.faculty_id = f.id 
                LEFT JOIN batch b ON sd.batch_id = b.batch_id 
                LEFT JOIN classes c ON sd.class_id = c.id 
                LEFT JOIN section s ON sd.section_id = s.section_id 
                WHERE sd.class_id = $classId AND sd.section_id = $sectionId";

        $readResult = $db->sql($sql);

        echo '
        <form style="margin: 10px" class="login-form" method="POST" action="Controller/studentAttendanceController.php?page=studentAttendenceData" id="attendanceForm" onSubmit="return validateForm()">
        <div>
        <div>
        <div class="title" style="flex-direction: column;">
            <h2>' . $readResult[0]['class_name'] . ' - ' . $readResult[0]['section_name'] . '</h2><br>
            <p style="color: #aaa; font-size: 12px; font-weight: 400; ">#Note : The Red Box Indicates Absent Students and Green Box Indicates Present Students</p>
        </div>
        <table style="width: 100%;">
            <tr>
                <th>Student Name</th>
                <th>Class</th>
                <th>Section</th>
                <th style="width: 100px;">Attendance</th>
            </tr>';

        foreach ($readResult as $key) {
            echo '<tr>
                <td>' . $key['student_name'] . '</td>
                <td>' . $key['class_name'] . '</td>
                <td>' . $key['section_name'] . '</td>
                <td>
                    <input name="students[' . $key["student_id"] . '][student_id]" type="hidden" value="' . $key["student_id"] . '">
                    <input name="students[' . $key["student_id"] . '][class_id]" type="hidden" value="' . $key["class_id"] . '">
                    <input name="students[' . $key["student_id"] . '][section_id]" type="hidden" value="' . $key["section_id"] . '">
                    <div style="display: flex; justify-content: space-around;">
                        <div style="background: green; border-radius: 100%; height: 16px; width: 16px; display: flex; align-items: center; justify-content: space-between; padding-top: 1px;">
                            <input type="radio" name="students[' . $key["student_id"] . '][attendance]" value="present" id="presentRadio' . $key["student_id"] . '">
                        </div>
                        <div style="background: red; border-radius: 100%; height: 16px; width: 16px; display: flex; align-items: center; justify-content: space-between; padding-top: 1px;">
                            <input type="radio" name="students[' . $key["student_id"] . '][attendance]" value="absent" id="absentRadio' . $key["student_id"] . '">
                        </div>
                    </div>
                </td>
            </tr>';
        }

        echo '</table>
        </div>
        <div style="text-align: center; margin-top: 10px; position: absolute; bottom: 20px; left: 20px;">
            <input type="hidden" name="subject_id" id="selectedSubjectId">
            <input type="hidden" name="staff_attendance_id" id="selectedStaffAttendanceId">
            <button type="submit" class="btn-green">Submit</button>
        </div>
        </div>
        </div>
        </form>
        ';
    }

    public function studentAttendenceData()
{
    if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['students']) && isset($_POST['subject_id']) && isset($_POST['staff_attendance_id'])) {
        $db = new Database();

        $students = $_POST['students'];
        $subject_id = input_sanitize($_POST['subject_id']);
        $staff_attendance_id = input_sanitize($_POST['staff_attendance_id']);
        $attendance_date = date("Y-m-d");

        // Check if subject_id and staff_attendance_id are valid
        if (empty($subject_id) || empty($staff_attendance_id)) {
            $_SESSION["errorMessage"] = "Invalid subject or staff attendance ID!";
            header("Location: ../attendance-student.php");
            exit();
        }

        // Insert student attendance records
        foreach ($students as $student) {
            $student_id = input_sanitize($student['student_id']);
            $class_id = input_sanitize($student['class_id']);
            $section_id = input_sanitize($student['section_id']);
            $attendance_status = input_sanitize($student['attendance']);

            $attendanceData = [
                'student_id' => $student_id,
                'class_id' => $class_id,
                'section_id' => $section_id,
                'subject_id' => $subject_id,
                'staff_id' => $_SESSION['user_id'],
                'attendance_status' => $attendance_status,
                'attendance_date' => $attendance_date,
            ];

            $inserted = $db->create('student_attendance', $attendanceData);

        }

        // Update staff_attendance table's status to 1
        $updateData = [
            'status' => 1
        ];
        $updateCondition = "WHERE id = '" . $staff_attendance_id . "'";
        $updated = $db->update('staff_attendance', $updateData, $updateCondition);

        if ($updated) {
            $_SESSION["successMessage"] = "Student attendance data has been recorded successfully!";
        } else {
            $_SESSION["errorMessage"] = "Failed to update staff attendance status!";
        }

        header("Location: ../attendance-student.php");
        exit();
    } else {
        $_SESSION["errorMessage"] = "Failed to record student attendance data!";
        header("Location: ../attendance-student.php");
        exit();
    }
}

    
    
}
new StudentAttendanceController();
