<?php
// session_start();
include "../helper.php";
include 'Controller.php';
include "../Model/Database.php";

class AttendanceListController extends Controller
{
    public function __construct()
    {
        if ($_GET['page'] === 'listStudent') {
            $this->listStudent();
        }
    }

    public function listStudent()
    {
        $date = $_POST['date'];
        $role = $_SESSION['role'];
        $db = new Database();
        $attendanceData = [];

        if ($role == 'admin') {
            $attendanceQuery = "
                SELECT 
                    c.name AS class_name, 
                    sec.section_name, 
                    s.subject_name,
                    COUNT(sa.student_id) AS total_students,
                    SUM(CASE WHEN sa.attendance_status = 'present' THEN 1 ELSE 0 END) AS present_students,
                    SUM(CASE WHEN sa.attendance_status = 'absent' THEN 1 ELSE 0 END) AS absent_students
                FROM student_attendance sa
                INNER JOIN classes c ON sa.class_id = c.id
                INNER JOIN section sec ON sa.section_id = sec.section_id
                INNER JOIN subject s ON sa.subject_id = s.subject_id
                WHERE sa.attendance_date = '".$date."'
                GROUP BY c.name, sec.section_name, s.subject_name;
            ";
            $attendanceData = $db->sql($attendanceQuery);
        } elseif ($role == 'teacher') {
            $teacherId = $_SESSION['user_id'];
            $attendanceQuery = "
                SELECT 
                    c.name AS class_name, 
                    sec.section_name, 
                    s.subject_name,
                    COUNT(sa.student_id) AS total_students,
                    SUM(CASE WHEN sa.attendance_status = 'present' THEN 1 ELSE 0 END) AS present_students,
                    SUM(CASE WHEN sa.attendance_status = 'absent' THEN 1 ELSE 0 END) AS absent_students
                FROM student_attendance sa
                INNER JOIN classes c ON sa.class_id = c.id
                INNER JOIN section sec ON sa.section_id = sec.section_id
                INNER JOIN subject s ON sa.subject_id = s.subject_id
                WHERE sa.staff_id = ".$teacherId." AND DATE(sa.attendance_date) = '".$date."'
                GROUP BY c.name, sec.section_name, s.subject_name;
            ";
            $attendanceData = $db->sql($attendanceQuery);
        } elseif ($role == 'student') {
            $studentId = $_SESSION['user_id'];
            $attendanceQuery = "
                SELECT 
                    sa.attendance_date, 
                    s.subject_name,
                    sa.attendance_status 
                FROM student_attendance sa
                INNER JOIN subject s ON sa.subject_id = s.subject_id
                WHERE sa.student_id = ".$studentId."
                ORDER BY sa.attendance_date DESC, s.subject_name;
            ";
            $attendanceData = $db->sql($attendanceQuery);
        }

        // Render the attendance data as HTML
        $html = $this->renderAttendanceData($attendanceData);
        echo $html;
    }

    private function renderAttendanceData($attendanceData)
    {
        if (empty($attendanceData)) {
            return '<p>No attendance data available.</p>';
        }

        $html = '<h2>Attendance Data</h2>';
        $html .= '<table border="1">';
        $html .= '<tr>
                    <th>Class Name</th>
                    <th>Section Name</th>
                    <th>Subject Name</th>
                    <th>Total Students</th>
                    <th>Present Students</th>
                    <th>Absent Students</th>
                  </tr>';

        foreach ($attendanceData as $attendance) {
            $html .= '<tr>';
            $html .= '<td>' . htmlspecialchars($attendance['class_name']) . '</td>';
            $html .= '<td>' . htmlspecialchars($attendance['section_name']) . '</td>';
            $html .= '<td>' . htmlspecialchars($attendance['subject_name']) . '</td>';
            $html .= '<td>' . htmlspecialchars($attendance['total_students']) . '</td>';
            $html .= '<td>' . htmlspecialchars($attendance['present_students']) . '</td>';
            $html .= '<td>' . htmlspecialchars($attendance['absent_students']) . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }
}

new AttendanceListController();
?>
