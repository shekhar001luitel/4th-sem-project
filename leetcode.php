<?php
function dd($value)
{
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
    die();
}
function twoSum()
{
    $nums = [2, 7, 11, 15];
    $target = 9;
    $sum = 0;
    foreach ($nums as $key => $value) {
        var_dump($key+1);
        for ($i = $key + 1; $i <= count($nums) - 1; $i++) {  // key= 5  count = 3
            if ($nums[$key] + $nums[$i] === $target) {
                $output = [$key, $i];
                return $output;
            }
        }
    };
    echo 'no match found';
}
$l1 = [2, 4, 3];
$l2 = [5, 6, 4];
// print_r (twoSum());


function array_rev_add($list)
{
    foreach ($list as $key => $value) :
        $test = pow(10, $key);
        $list[$key] = ($value * $test);
    endforeach;
    $list = array_reverse($list);
    $value_add = 0;
    foreach ($list as $key => $value) :
        $value_add = $value + $value_add;
    endforeach;
    return $value_add;
}
function addTwoNumbers($l1, $l2)
{

    $l = array_rev_add($l1) + array_rev_add($l2);
    $newl = array();
    foreach (str_split($l) as $key => $value) {
        $newl[$key] = $value;
    }
    return (array_reverse($newl));
}
// print_r(addTwoNumbers($l1, $l2));

function isPalindrome($x) {
    if ($x < 0) {
        return false;
    }

    $original = $x;
    $reversed = 0;

    while ($x > 0) {
        $digit = $x % 10;
        $reversed = $reversed * 10 + $digit;
        $x = (int)($x / 10);
    }

    return $original === $reversed;
}
// print_r(isPalindrome(1));

function isProfane($input) {
    $badWords = array('badword1', 'badword2', 'fuck', 'anotherbadword');

    // Convert the input to lowercase for case-insensitive matching
    $lowercaseInput = strtolower($input);

    foreach ($badWords as $badWord) {
        // Check for exact match
        if (strpos($lowercaseInput, $badWord) !== false) {
            // return true;
        }

        // Check for variations like f*ck, fuuck, etc.
        $pattern = '/\b(' . str_replace(['*', ''], ['\w?', ''], $badWord) . '|' . $badWord . ')\b/i';

        if (preg_match($pattern, $lowercaseInput)) {
            var_dump($pattern);
            // return true;
        }
    }
    // No inappropriate words found
    return false;
}

// // Example usage
// $userInput = 'This is a fuck example.';
// if (isProfane($userInput)) {
//     echo 'Inappropriate word found!';
// } else {
//     echo 'No inappropriate words.';
// }

