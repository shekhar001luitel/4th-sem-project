<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class TeacherManagement extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $readResult = $db->readAll('staff_details');
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/teacherManagement',['data' =>$readResult]);
        view_require('_parts/footer');
    }
}
 new TeacherManagement();