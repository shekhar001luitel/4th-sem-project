<?php
include "helper.php";
include 'Controller/Controller.php';
include 'Model/Database.php';
class Subject extends Controller
{

    public function __construct()
    {
        $this->check_login();
        $this->index();
    }
    public function index()
    {
        $db = new Database();
        $readResult = $db->readAll('subject');
        $cssFiles = ['dashboard'];
        view_require('_parts/header', ['css' => $cssFiles]);
        view_require('_parts/sidebar');
        view_require('dashboard/subject',['data' =>$readResult]);
        view_require('_parts/footer');
    }
}
new Subject();