<?php
// landing page
require 'helper.php';
function Check_login()
{
    if ($_SESSION['user_id']) {
        header('Location: dashboard.php');
    } else {
        header('Location: login.php');
    }
}

Check_login();
