CREATE TABLE
  `users` (
    `id` int (11) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `role` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`)
  )
CREATE TABLE
  `faculty` (
    `id` int (11) NOT NULL AUTO_INCREMENT,
    `faculty_name` varchar(255) NOT NULL,
    `description` varchar(255) NOT NULL,
    `level` varchar(255) NOT NULL,
    `affiliate_university` varchar(255) NOT NULL,
    `enrollment_type` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `faculty_name` (`faculty_name`)
  )
CREATE TABLE
  `batch` (
    `batch_id` int (11) NOT NULL AUTO_INCREMENT,
    `faculty_id` int (11) NOT NULL,
    `batch_name` varchar(255) NOT NULL,
    `description` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`batch_id`),
    KEY `faculty_id` (`faculty_id`),
    CONSTRAINT `batch_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE
  )
CREATE TABLE
  `classes` (
    `id` int (11) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    `display_name` varchar(255) DEFAULT NULL,
    `batch_id` int (11) DEFAULT NULL,
    `status` int (11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`),
    KEY `batch_id` (`batch_id`),
    CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`batch_id`) ON DELETE CASCADE
  )
CREATE TABLE
  `section` (
    `section_id` int (11) NOT NULL AUTO_INCREMENT,
    `section_name` varchar(255) NOT NULL,
    `class_id` int (11) DEFAULT NULL,
    PRIMARY KEY (`section_id`),
    KEY `class_id` (`class_id`),
    CONSTRAINT `section_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE
  )
CREATE TABLE
  `class_section` (
    `class_id` int (11) NOT NULL,
    `section_id` int (11) NOT NULL,
    PRIMARY KEY (`class_id`, `section_id`),
    KEY `class_section_ibfk_2` (`section_id`),
    CONSTRAINT `class_section_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE,
    CONSTRAINT `class_section_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`) ON DELETE CASCADE
  )
CREATE TABLE
  `subject` (
    `subject_id` int (11) NOT NULL AUTO_INCREMENT,
    `subject_name` varchar(255) NOT NULL,
    PRIMARY KEY (`subject_id`)
  )
CREATE TABLE
  `section_subject` (
    `section_id` int (11) NOT NULL,
    `subject_id` int (11) NOT NULL,
    PRIMARY KEY (`section_id`, `subject_id`),
    KEY `subject_id` (`subject_id`),
    CONSTRAINT `section_subject_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`) ON DELETE CASCADE,
    CONSTRAINT `section_subject_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`subject_id`) ON DELETE CASCADE
  )
CREATE TABLE student_details (
    id INT AUTO_INCREMENT PRIMARY KEY,
    student_image_path VARCHAR(255),
    name VARCHAR(255) NOT NULL,
    name_in_nepali VARCHAR(255),
    gender VARCHAR(10),
    dob_bs DATE,
    faculty_id INT,
    batch_id INT,
    class_id INT,
    section_id INT,
    phone VARCHAR(15),
    nationality VARCHAR(50),
    permanent_province VARCHAR(50),
    permanent_district VARCHAR(50),
    permanent_municipality VARCHAR(50),
    permanent_address VARCHAR(255),
    father_name VARCHAR(255),
    father_image_path VARCHAR(255),
    f_occupation VARCHAR(255),
    f_cell VARCHAR(15),
    f_email VARCHAR(255),
    f_office VARCHAR(255),
    mother_name VARCHAR(255),
    mother_image_path VARCHAR(255),
    m_occupation VARCHAR(255),
    m_cell VARCHAR(15),
    m_email VARCHAR(255),
    m_office VARCHAR(255),
    guardian_name VARCHAR(255),
    guardian_image_path VARCHAR(255),
    g_relation VARCHAR(50),
    g_cell VARCHAR(15),
    religion VARCHAR(50),
    cast VARCHAR(50),
    citizenship VARCHAR(50),
    handicapped BOOLEAN,
    password varchar(255) NOT NULL
);

CREATE TABLE staff_details (
    id INT AUTO_INCREMENT PRIMARY KEY,
    staff_image_path VARCHAR(255),
    name VARCHAR(255) NOT NULL,
    name_in_nepali VARCHAR(255),
    gender VARCHAR(10),
    dob_bs DATE,
    phone VARCHAR(15),
    nationality VARCHAR(50),
    permanent_province VARCHAR(50),
    permanent_district VARCHAR(50),
    permanent_municipality VARCHAR(50),
    permanent_address VARCHAR(255),
    father_name VARCHAR(255),
    father_image_path VARCHAR(255),
    f_occupation VARCHAR(255),
    f_cell VARCHAR(15),
    f_office VARCHAR(255),
    mother_name VARCHAR(255),
    mother_image_path VARCHAR(255),
    m_occupation VARCHAR(255),
    m_cell VARCHAR(15),
    m_office VARCHAR(255),
    religion VARCHAR(50),
    cast VARCHAR(50),
    citizenship VARCHAR(50),
    handicapped BOOLEAN,
    password varchar(255) NOT NULL
);

CREATE TABLE staff_attendance (
    id INT AUTO_INCREMENT PRIMARY KEY,
    staff_id INT NOT NULL,
    class_id INT NOT NULL,
    section_id INT NOT NULL,
    attendance_date DATE NOT NULL DEFAULT CURRENT_DATE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    first_in int DEFAULT 0,
    status int DEFAULT 0,
    FOREIGN KEY (staff_id) REFERENCES staff_details(id) ON DELETE CASCADE,
    FOREIGN KEY (class_id) REFERENCES classes(id) ON DELETE CASCADE
    FOREIGN KEY (section_id) REFERENCES section(section_id) ON DELETE CASCADE
);

CREATE TABLE student_attendance (
    id INT AUTO_INCREMENT PRIMARY KEY,
    student_id INT NOT NULL,
    class_id INT NOT NULL,
    section_id INT NOT NULL,
    subject_id INT NOT NULL,
    staff_id INT NOT NULL,
    attendance_status ENUM('present', 'absent') NOT NULL,
    attendance_date DATE NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (student_id) REFERENCES student_details(id) ON DELETE CASCADE,
    FOREIGN KEY (class_id) REFERENCES classes(id) ON DELETE CASCADE,
    FOREIGN KEY (section_id) REFERENCES section(section_id) ON DELETE CASCADE,
    FOREIGN KEY (subject_id) REFERENCES subject(subject_id) ON DELETE CASCADE,
    FOREIGN KEY (staff_id) REFERENCES staff_details(id) ON DELETE CASCADE
);
