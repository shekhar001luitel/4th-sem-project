<?php

class Database
{
    private $host = "localhost";
    private $database = "sem";
    private $username = "root";
    private $password = "";
    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host={$this->host};dbname={$this->database}", $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function create($tableName, $data)
    {
        try {
            $columns = implode(", ", array_keys($data));
            $values = ":" . implode(", :", array_keys($data));
            $sql = "INSERT INTO $tableName ($columns) VALUES ($values)";
            $stmt = $this->connection->prepare($sql);

            foreach ($data as $key => $value) {
                $stmt->bindValue(":$key", $value);
            }

            $stmt->execute();
            // echo "Record created successfully";
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return [];
        }
    }

    public function read($tableName, $condition = '')
    {
        try {
            $sql = "SELECT * FROM $tableName $condition";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return [];
        }
    }
    public function fetchById($tableName,$row ,$id, $read = false)
    {
        // fetchById(faculty, 1,  true)
        $tableName = ''.$tableName.'';
        $id =  input_sanitize($id);
        $condition = "WHERE ". $row ."=" . $id;
        if ($read === true) {
            return $this->readAll($tableName, $condition);
        } else {
           return $this->read($tableName, $condition);
        }
    }

    public function readAll($tableName, $condition = '')
    {
        try {
            $sql = "SELECT * FROM $tableName $condition";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            // dd($sql);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return [];
        }
    }

    public function update($tableName, $data, $condition)
    {

        try {
            $setClause = implode("=?, ", array_keys($data)) . "=?";
            $sql = "UPDATE $tableName SET $setClause $condition";
            $stmt = $this->connection->prepare($sql);
            $i = 1;
            foreach ($data as $value) {
                $stmt->bindValue($i++, $value);
            }
            $stmt->execute();
            return True;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    // public function delete($tableName, $condition)
    // {
    //     try {
    //         $sql = "DELETE FROM $tableName $condition";
    //         $stmt = $this->connection->prepare($sql);
    //         $stmt->execute();
    //         echo "Record deleted successfully";
    //     } catch (PDOException $e) {
    //         echo "Error: " . $e->getMessage();
    //     }
    // }
    public function delete($tableName, $condition)
{
    try {
        $this->connection->beginTransaction();
        
        $sql = "DELETE FROM $tableName $condition";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        
        $this->connection->commit();
        echo "Record(s) deleted successfully";
        return true;
    } catch (PDOException $e) {
        $this->connection->rollBack();
        echo "Error: " . $e->getMessage();
    }
}

    public function sql($query ,$oneRow = false){
        try {
            $sql = "$query";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            // dd($sql);
            if($oneRow === true){
                return $stmt->fetch(PDO::FETCH_ASSOC);
            }else{
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return [];
        }
    }
    public function joinTable($table1,$table2 ,$id){
        try {
            $sql = "select $table1.*, $table1.* from $table1 inner join $table2 on $table1.id = $table2.id where id = $id";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute();
            // dd($sql);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return [];
        }

    }

    public function get($tableName, $conditions)
    {
        try {
            $whereClause = '';
            $params = [];
            foreach ($conditions as $key => $value) {
                $whereClause .= "$key = :$key AND ";
                $params[":$key"] = $value;
            }
            $whereClause = rtrim($whereClause, " AND ");
            $sql = "SELECT * FROM $tableName WHERE $whereClause";
            $stmt = $this->connection->prepare($sql);
            $stmt->execute($params);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
        }
    }

    public function checkRole($user_id){
        $condition = "select * from user where id = ";
        $role = $this->fetchById('users','id', $user_id );
        return $role['role'];
    }
}
